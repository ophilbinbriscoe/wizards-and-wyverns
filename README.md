# README #

This is the repository for our COMP 345 team project - a lightweight D20 RPG clone.

### What is this repository for? ###

* Tracking our changes.
* Backing up our work.

### How do I get set up? ###

* Install Visual Studio 2015 Community for C/C++.
* Install SourceTree.
* Open SourceTree, and press Clone/New. Follow the directions.
* Pull the latest changes.
* Open the Wizards and Wyverns.sln file using Visual Studio.
* Press Local Windows Debugger to compile and run in debug mode.