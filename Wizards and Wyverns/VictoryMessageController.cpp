#include "VictoryMessageController.h"
#include "SaveDiscardDialogController.h"
#include "Database.h"

VictoryMessageController::VictoryMessageController( Game * game ) : MenuController(), _game( game )
{
	_menu->Pack( sfg::Label::Create( "You have finished the campaign!" ) );

	auto button = sfg::Button::Create( "OK" );

	button->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &VictoryMessageController::HandleClickOK, this ) );

	_menu->Pack( button );

	Finalize();
}

VictoryMessageController::~VictoryMessageController()
{}

void VictoryMessageController::HandleClickOK()
{
	auto player = _game->GetHero();

	for ( auto existing : Data().characters.GetContents() )
	{
		if ( existing->IsPlayable() && existing->GetName() == player->GetName() )
		{
			Push( new SaveDiscardDialogController( player, existing ) );
			
			return;
		}
	}
	
	Collapse();
}

void VictoryMessageController::HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke )
{
	if ( ke.code == sf::Keyboard::Space )
	{
		HandleClickOK();
	}
}

void VictoryMessageController::SubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Subscribe( this, &VictoryMessageController::HandleKeyPressed );
}

void VictoryMessageController::UnsubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Unsubscribe( this, &VictoryMessageController::HandleKeyPressed );
}


