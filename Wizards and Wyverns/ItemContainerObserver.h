#pragma once

#include "ItemContainer.h"
#include "Container.h"
#include "ItemObserver.h"
#include "View.h"

namespace view
{
	class ItemContainerObserver : public View, public Observer<ItemContainer>
	{
		typedef tuple<ItemObserver*, shared_ptr<sfg::Label>, shared_ptr<sfg::Box>> ItemView;

	public:
		ItemContainerObserver( shared_ptr<sfg::Box> _container );
		virtual ~ItemContainerObserver();

	private:
		virtual void StartObserving();
		virtual void StopObserving();

		void HandleItemAdd( Item const * item );
		void HandleItemRemove( Item const * item );
		void HandleItemQuantityModified( Item const * item );

		void AddItemView( Item const * item );
		void RemoveItemView( Item const * item );
		void UpdateItemQuantity( Item const * item );

		shared_ptr<sfg::Box> _itemBox;

		map<Item const *, ItemView> _itemViews;
	};
}
