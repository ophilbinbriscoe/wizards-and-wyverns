#include "ItemContainer.h"

ItemContainer::ItemContainer()
{}

ItemContainer::ItemContainer( const ItemContainer & container ) : _items( container._items )
{}

ItemContainer::~ItemContainer()
{}

bool ItemContainer::AddItem( Item const * item, const unsigned int& quantity )
{
	if ( _items.find( item ) == _items.end() )
	{
		_items.insert( pair<Item const *, unsigned int>( item, quantity ) );
		OnAddItem( item );
		return true;
	}
	else
	{
		_items.at( item ) += quantity;
		OnItemQuantityModified( item );
		return true;
	}
}

bool ItemContainer::RemoveItem( Item const * item, const unsigned int& quantity )
{
	if ( _items.find( item ) == _items.end() )
	{
		return false;
	}

	unsigned int remove = min( quantity, _items.at( item ) );

	_items.at( item ) -= remove;

	if ( _items.at( item ) == 0 )
	{
		_items.erase( item );
		OnRemoveItem( item );
		
	}
	return true;
}

void ItemContainer::SetItemQuantity( Item const * item, const unsigned int & quantity )
{
	if ( _items.find( item ) == _items.end() )
	{
		AddItem( item, quantity );
	}
	else
	{
		_items.at( item ) = quantity;
		OnItemQuantityModified( item );
	}
}

unsigned int ItemContainer::GetItemQuantity( Item const * item ) const
{
	if ( _items.find( item ) == _items.end() )
	{
		return 0;
	}

	return _items.at( item );
}

const map<Item const *, unsigned int>& ItemContainer::GetItems() const
{
	return _items;
}

void ItemContainer::operator=( const ItemContainer & container )
{
	Empty();

	for ( auto pair : container._items )
	{
		AddItem( pair.first, pair.second );
	}
}

void ItemContainer::Empty()
{
	for ( auto pair : _items )
	{
		OnRemoveItem( pair.first );
	}

	_items.clear();
}

void ItemContainer::EmptyInto( ItemContainer & container )
{
	for ( auto pair : _items )
	{
		container.AddItem( pair.first, pair.second );
	}

	Empty();
}
