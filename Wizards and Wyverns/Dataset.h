#pragma once

#include "Container.h"

template <typename type>
class Dataset : public Container<type>
{
public:
	typedef bool( *Predicate )(type *);

	Dataset();
	~Dataset();

	type Random();
	vector<type *> Filter( Predicate predicate );

protected:
	virtual bool AllowDuplicates() const { return false; }
};

template<typename type>
inline Dataset<type>::Dataset()
{}

template<typename type>
inline Dataset<type>::~Dataset()
{}

template<typename type>
inline type Dataset<type>::Random()
{
	return _vector[rand() % _vector.size()];
}

template<typename type>
inline vector<type*> Dataset<type>::Filter( Predicate predicate )
{
	vector<type*> filtered;

	for ( auto element : _vector )
	{
		if ( Predicate( element ) )
		{
			filtered.push_back( element );
		}
	}

	return filtered;
}
