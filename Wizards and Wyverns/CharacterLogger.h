#pragma once

#include "Logger.h"
#include "Character.h"
#include "Observer.h"

class CharacterLogger : public Logger, public Observer<Character>
{
public:
	CharacterLogger();
	virtual ~CharacterLogger();

protected:
	void HandleLootedChest( Character const * const actor, Chest const * const target );
	void HandleLootedCorpse( Character const * const actor, Character const * const target );
	void HandleAttack( Character const * const actor, Character const * const target, const bool & hit, const int& attackRoll, const int& attackBonus, const int& damageRoll );
	void HandleDeath( Character const * const character );

	virtual void StartObserving();
	virtual void StopObserving();
};

