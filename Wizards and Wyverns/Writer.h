#pragma once

#include <stdio.h>

#include <fstream>

class Writer
{
public:
	Writer( std::fstream& file, const char * filepath );
	virtual ~Writer();

protected:
	std::fstream& _file;
};

