#include "SharedCanvas.h"

shared_ptr<sfg::Canvas> GetSharedCanvas()
{
	static shared_ptr<sfg::Canvas> canvas = sfg::Canvas::Create();

	return canvas;
}
