#pragma once

#include <tuple>

#include "Map.h"
#include "Campaign.h"
#include "Dataset.h"

/// Allows a Map template to be read from a file.
class CampaignReader
{
public:
	CampaignReader( const char* filename, Dataset<Campaign *>& dataset, Dataset<Map *>& maps );
	~CampaignReader();
};

