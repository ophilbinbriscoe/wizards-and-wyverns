#include "DiceLogger.h"

DiceLogger::DiceLogger()
{}

DiceLogger::~DiceLogger()
{}

void DiceLogger::StartObserving()
{
	_target->OnRoll.Subscribe(this, &DiceLogger::OnRollEventHandler);
}

void DiceLogger::StopObserving()
{
	_target->OnRoll.Unsubscribe(this, &DiceLogger::OnRollEventHandler);
}

void DiceLogger::OnRollEventHandler(const string & value, const unsigned int & result)
{
	if ( enabled )
	{
		cout << "(dice) roll " << value << ", result: " << result << endl;
	}
}
