#include "SaveDiscardDialogController.h"

#include "Database.h"

SaveDiscardDialogController::SaveDiscardDialogController( Character const * character, Character * existing ) : MenuController(), _character( character ), _existing( existing )
{
	_menu->Pack( sfg::Label::Create( string( "A saved character named '" ) + character->GetName() + string( "' already exists." ) ) );

	auto buttons = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 10.0f );

	auto buttonDiscard = sfg::Button::Create( "Discard" );

	buttonDiscard->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &SaveDiscardDialogController::HandleClickDiscard, this ) );

	buttons->Pack( buttonDiscard );

	auto buttonOverwrite = sfg::Button::Create( "Overwrite" );

	buttonOverwrite->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &SaveDiscardDialogController::HandleClickOverwrite, this ) );

	buttons->Pack( buttonOverwrite );

	_menu->Pack( buttons );

	Finalize();
}

SaveDiscardDialogController::~SaveDiscardDialogController()
{}

void SaveDiscardDialogController::HandleClickDiscard()
{
	Collapse();
}

void SaveDiscardDialogController::HandleClickOverwrite()
{
	*_existing = Character( *_character );

	Collapse();
}