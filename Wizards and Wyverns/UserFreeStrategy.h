#pragma once

#include "Strategy.h"
#include "Position.h"

template <typename type>
class UserFreeStrategy : public FreeStrategy<type>
{
public:
	UserFreeStrategy();
	~UserFreeStrategy();

	virtual void RequestDecision( Character const * const character, Map const * const map );

	void Decide( Character const * const character, const type & decision );
};

template <typename type>
UserFreeStrategy<type>::UserFreeStrategy()
{}

template <typename type>
UserFreeStrategy<type>::~UserFreeStrategy()
{}

template<typename type>
inline void UserFreeStrategy<type>::RequestDecision( Character const * const character, Map const * const map )
{}

template <typename type>
void UserFreeStrategy<type>::Decide( Character const * const character, const type & decision )
{
	_decision = decision;
	OnDecide( this, character );
}