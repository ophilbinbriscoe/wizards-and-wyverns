#include "ArmorClassObserver.h"

#include "StatFormatter.h"

view::ArmorClassObserver::ArmorClassObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	_label = sfg::Label::Create( "" );
	
	_label->SetAlignment( { 0.0f, 0.5f } );
	
	_container->Pack( _label, false, false );
}

view::ArmorClassObserver::~ArmorClassObserver()
{}

void view::ArmorClassObserver::StartObserving()
{
	_target->GetStat( Stats().ArmorClass ).OnModified.Subscribe( this, &ArmorClassObserver::HandleArmorClassModified );
	
	_label->Show( true );

	SetLabel();
}

void view::ArmorClassObserver::StopObserving()
{	
	_target->GetStat( Stats().ArmorClass ).OnModified.Unsubscribe( this, &ArmorClassObserver::HandleArmorClassModified );
	
	_label->Show( false );
	
	_label->SetText( "" );
}
void view::ArmorClassObserver::HandleArmorClassModified( DerivedStat const * const stat )
{
	SetLabel();
}

void view::ArmorClassObserver::SetLabel()
{	
	_label->SetText( Stats().ArmorClass->name + string( ": " ) + to_string( (int) _target->GetStat( Stats().ArmorClass ) ) );
}