#include "MapWriter.h"

#include <vector>
#include <stdio.h>
#include <string>
#include <cstring>

#include <algorithm>

#include "CharacterProxy.h"
#include "Chest.h"

using namespace std;

#pragma warning(disable:4996)

MapWriter::MapWriter( fstream& file, const char* filename, Dataset<Character *> characters ) : Writer( file, filename )
{
	for ( unsigned int i = 0; i < characters.GetCount(); i++ )
	{
		_order.insert( pair<Character const *, unsigned int>( characters.GetContents()[i], i ) );
	}
}

void MapWriter::Write( Map const * map, ItemContainerWriter const * writer )
{
	// write the header, width, and height
	_file << "map: " << map->GetWidth() << ' ' << map->GetHeight() << ' ';

	// write the number of objects
	_file << map->GetObjects().size() << ' ';
		
	// write the name
	_file << map->GetName().data() << '\n';

	for ( auto pair : map->GetObjects() )
	{
		auto character = dynamic_cast<CharacterProxy const *>(pair.first);

		if ( character )
		{
			_file << "char: " << pair.second.x << ' ' << pair.second.y << ' ' << _order.at( character->GetReal() ) << '\n';
			continue;
		}

		auto chest = dynamic_cast<Chest const *>(pair.first);

		if ( chest )
		{
			_file << "chest: " << pair.second.x << ' ' << pair.second.y << ' ';

			writer->Write( _file, chest );

			_file << '\n';
		}
	}

	// write the tiles
	for ( int y = 0; y < map->GetHeight(); y++ )
	{
		for ( int x = 0; x < map->GetWidth(); x++ )
		{
			_file << map->GetTileAt( Position( x, y ) )->Enumerator() << ' ';
		}

		_file << '\n';
	}
}

MapWriter::~MapWriter()
{}
