#pragma once

#include "Strategy.h"
#include "Character.h"

class StrategyProvider
{
public:
	StrategyProvider();
	virtual ~StrategyProvider();

	virtual BarrierStrategy<Position> * GetMovementStrategy( Character const * character ) const = 0;
	virtual BarrierStrategy<MapObject const *> * GetInteractionStrategy( Character const * character ) const = 0;
	virtual FreeStrategy<Item const *> * GetEquipStrategy( Character const * character ) const = 0;
	virtual FreeStrategy<SlotType const *> * GetUnequipStrategy( Character const * character ) const = 0;
	virtual FreeStrategy<StatType const *> * GetLevelStrategy( Character const * character ) const = 0;
};

