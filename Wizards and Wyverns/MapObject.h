#pragma once

#include "Position.h"
#include "Event.h"
#include "Resource.h"
#include "Interactable.h"
#include "Inspectable.h"

using namespace std;

class MapObject : public Resource, public Interactable, public Inspectable
{
public:
	MapObject( const string& name = "" );
	virtual ~MapObject();

	virtual bool IsFixed() const = 0;

	virtual void Accept( AbstractInteractor * interactor );
	virtual void Accept( AbstractInspector * inspector ) const;
};

