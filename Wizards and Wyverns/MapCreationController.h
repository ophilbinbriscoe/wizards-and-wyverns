#pragma once

#include "CreationController.h"
#include "MapObserver.h"
#include "ItemContainerObserver.h"
#include "CharacterProxy.h"

static const char TOOL_TILE = 1;
static const char TOOL_NPC = 2;
static const char TOOL_CHEST = 3;
static const char TOOL_REMOVE = 4;
static const char TOOL_MOVE = 5;
static const char TOOL_INSPECT = 6;

class MapCreationController : public CreationController<Map>, public AbstractInspector
{
public:
	MapCreationController( Map * hardcopy, const bool & add );
	virtual ~MapCreationController();

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );

protected:
	virtual Map * MakeWorkingCopy( Map * hardcopy );

	virtual void Inspect( Chest const * chest );
	virtual void Inspect( CharacterProxy const * proxy );

	virtual void HandleSpawn( Chest * chest );
	virtual void HandleSpawn( CharacterProxy * proxy );

	void HandleTileClicked( const Position & position );
	void HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke );

	void ShowToolOptions( shared_ptr<sfg::Window> options );
	void HideToolOptions();

	void ShowInspector( shared_ptr<sfg::Window> inspector );
	void HideInspectors();

	void HandleClickAddItem();
	void HandleClickClearItems();
	void HandleSetAlignment();

	void ClearObject();
	void InspectObject();

	char _tool;

	view::MapObserver * _mapObserver;
	view::ItemContainerObserver * _itemContainerObserver;

	Position _position;

	vector<Character *> _npcs;

	shared_ptr<sfg::ComboBox> _tilesCombo;
	shared_ptr<sfg::ComboBox> _npcCombo;
	shared_ptr<sfg::ComboBox> _itemCombo;
	shared_ptr<sfg::ComboBox> _alignmentCombo;

	shared_ptr<sfg::Window> _toolsWindow;

	shared_ptr<sfg::Window> _tileOptionWindow;
	shared_ptr<sfg::Window> _npcOptionWindow;

	shared_ptr<sfg::Window> _chestInspectorWindow;
	shared_ptr<sfg::Window> _npcInspectorWindow;

	map<Chest const *, Chest *> _chests;
	map<CharacterProxy const *, CharacterProxy *> _proxies;
};

