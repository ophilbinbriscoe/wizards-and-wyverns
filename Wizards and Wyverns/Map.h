#pragma once

#include <queue>
#include <vector>

#include "Grid.h"
#include "Container.h"
#include "MapObject.h"
#include "Observer.h"
#include "TileType.h"

using namespace std;

class Map : public Resource, protected Grid<TileType const *>
{
public:
	Map( Map const & map );
	Map( const int& width, const int& height, const string& name = "" );
	~Map();

	virtual bool Validate() const;

	int GetWidth() const { return _width; }
	int GetHeight() const { return _height; }

	void SetTileAt( const Position& position, TileType const * const type );
	TileType const * GetTileAt( const Position& position ) const;

	Event<const Position&> OnTileModified;
	Event<MapObject const *> OnAdd;
	Event<MapObject const *> OnRemove;
	Event<MapObject const *> OnSetPosition;

	void AddObjectAt( MapObject const * object, const Position& position );
	void SetPosition( MapObject const * object, const Position& position );
	void RemoveObject( MapObject const * object );

	const Position& GetPosition( MapObject const * object ) const;
	bool Contains( MapObject const * object ) const;
	bool Contains( const Position& position ) const;
	const map<MapObject const *, Position>& GetObjects() const;
	bool IsObjectAt( const Position& position ) const;
	MapObject const * GetObjectAt( const Position& position ) const;
	bool IsOccupied( const Position& position ) const;
	bool IsAdjacent( MapObject const * object, const Position& position ) const;

	int GetEntranceCount() const;
	Position GetEntrance( int index ) const;

	void operator = ( const Map & map );

protected:
	virtual bool AllowDuplicates() const { return false; }

private:
	bool ExpandTowards( const Position& position, queue<Position>* queue, bool* visited ) const;

	map<MapObject const *, Position> _objects;
};

inline ostream& operator<< ( ostream& stream, const Map& map )
{
	for ( int y = 0; y < map.GetHeight(); y++ )
	{
		for ( int x = 0; x < map.GetWidth(); x++ )
		{
			stream << map.GetTileAt( { x, y } );

			if ( y < map.GetHeight() - 1 )
			{
				stream << ' ';
			}
		}

		stream << endl;
	}

	return stream;
}



