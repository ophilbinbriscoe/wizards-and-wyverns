#pragma once

#include "Character.h"
#include "Map.h"

/// Abstract base class for a Strategy.
template <typename type>
class Strategy
{
public:
	Strategy();
	virtual ~Strategy();

	virtual void RequestDecision( Character const * const character, Map const * const map ) = 0;

	const type& GetDecision() const { return _decision; }

protected:
	type _decision;
};

template <typename type>
Strategy<type>::Strategy()
{}

template <typename type>
Strategy<type>::~Strategy()
{}

/// Abstract base class for a BarrierStrategy, for decisions that must be made at specific times and in order.
template <typename type>
class BarrierStrategy : public Strategy<type>
{
public:
	BarrierStrategy();
	virtual ~BarrierStrategy();

	const bool& IsReady() const { return _ready; }
	const bool& WillYield() const { return _yield; }

protected:
	bool _ready;
	bool _yield;
};

template <typename type>
BarrierStrategy<type>::BarrierStrategy()
{}

template <typename type>
BarrierStrategy<type>::~BarrierStrategy()
{}

/// Abstract base class for a FreeStrategy, for decisions that can be made at arbitrary times and out of order.
template <typename type>
class FreeStrategy : public Strategy<type>
{
public:
	FreeStrategy();
	virtual ~FreeStrategy();

	Event<FreeStrategy const *, Character const *> OnDecide;
};

template <typename type>
FreeStrategy<type>::FreeStrategy()
{}

template <typename type>
FreeStrategy<type>::~FreeStrategy()
{}