#include "CopyAdjuster.h"

#include "CharacterBuilder.h"
#include "CharacterProxy.h"

CopyAdjuster::CopyAdjuster()
{}

CopyAdjuster::~CopyAdjuster()
{}

void CopyAdjuster::Adjust( Chest const * chest, const Position & position, Map * map )
{
	// create a copy of the original Chest
	Chest * clone = new Chest( chest );

	// add the copy to the adjusted Map, at the same position as the original
	map->AddObjectAt( clone, position );

	// notify listeners of a newly spawned Chest
	OnSpawnChest( clone );
}

void CopyAdjuster::Adjust( CharacterProxy const * proxy, const Position & position, Map * map )
{
	// create a copy of the original proxy
	CharacterProxy * clone = new CharacterProxy( proxy->GetReal() );
	
	// copy the override settings from the original proxy
	clone->SetOverride( proxy->GetOverride() );
	
	// add the copy to the adjusted Map, at the same position as the original
	map->AddObjectAt( clone, position);

	// notify listeners of a newly spawned Character
	OnSpawnProxy( clone );
}
