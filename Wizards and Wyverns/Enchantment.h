#pragma once

#include "StatType.h"

class Enchantment
{
private:

public:
	Enchantment( StatType const * const type, const unsigned int& bonus );
	~Enchantment();

	const unsigned int bonus;
	StatType const * const type;
};

typedef vector<Enchantment> Enchantments;
