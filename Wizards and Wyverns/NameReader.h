#pragma once

#include "Dataset.h"
#include "Item.h"

class NameReader
{
public:
	NameReader( const char* filename, Dataset<Item *>& dataset );
	~NameReader();
};

