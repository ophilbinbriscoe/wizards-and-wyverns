#pragma once

#include "MenuController.h"
#include "MessageController.h"
#include "Dataset.h"

template <class type>
class CreationController : public MenuController
{
public:
	CreationController( type * hardcopy, Dataset<type *> & dataset, const bool & add );
	virtual ~CreationController();

protected:	
	void PackButtons();

	virtual type * MakeWorkingCopy( type * hardcopy ) = 0;

	void Prepare();

	type * _target;

private:
	void HandleClickSave();
	void HandleClickQuit();

	Dataset<type *> & _dataset;
	bool _add;
	
	void Save();
	
	type * const _hardcopy;
};

template <class type>
CreationController<type>::CreationController( type * hardcopy, Dataset<type *> & dataset, const bool & add ) : _hardcopy( hardcopy ), _dataset( dataset ), _add( add )
{}

template <class type>
CreationController<type>::~CreationController()
{
	// clean up temporary local resource
	delete _target;

	// if the permanent resource was never added to the dataset
	if ( _add )
	{
		// clean it up
		delete _hardcopy;
	}
}

template <class type>
void CreationController<type>::PackButtons()
{
	auto buttonQuit = sfg::Button::Create( "Quit" );
	auto buttonSave = sfg::Button::Create( "Save" );

	auto buttons = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 10.0f );

	buttonQuit->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CreationController<type>::HandleClickQuit, this ) );
	buttonSave->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CreationController<type>::HandleClickSave, this ) );

	buttons->Pack( buttonQuit );
	buttons->Pack( buttonSave );

	_menu->Pack( buttons );
}

template<class type>
inline void CreationController<type>::Prepare()
{
	_target = MakeWorkingCopy( _hardcopy );
}

template <class type>
void CreationController<type>::HandleClickSave()
{
	if ( _target->Validate() )
	{
		// save the resource
		Save();
		
		// push a new GUI frame, indicating to the user that the Map is valid and was saved
		Push( new MessageController( "Validation successful. Resource saved.", "Back" ) );
	}
	else
	{
		// push a new GUI frame, indicating to the user that the Map is not valid and was not saved
		Push( new MessageController( "Validation failed. Resource not saved.", "Back" ) );
	}
}

template <class type>
void CreationController<type>::HandleClickQuit()
{
	Pop( this );
}

template <class type>
void CreationController<type>::Save()
{
	*_hardcopy = *_target;

	if ( _add )
	{
		_dataset.Add( _hardcopy );

		_add = false;
	}
}

