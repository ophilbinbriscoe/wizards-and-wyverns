#include "WornItemsObserver.h"

namespace view
{
	WornItemsObserver::WornItemsObserver( shared_ptr<sfg::Box> container) : View( container )
	{
		CreateObserver( Slots().Head );
		CreateObserver( Slots().Body );
		CreateObserver( Slots().WeaponHand );
		CreateObserver( Slots().ShieldHand );
		CreateObserver( Slots().Waist );
		CreateObserver( Slots().Feet );
		CreateObserver( Slots().RingFinger );
	}

	WornItemsObserver::~WornItemsObserver()
	{}

	void WornItemsObserver::CreateObserver( SlotType const * type )
	{
		auto observer = new SlotObserver( _container, false );

		observer->OnClickRemove.Subscribe( this, &WornItemsObserver::HandleClickUnequip );
		observer->OnClickEquip.Subscribe( this, &WornItemsObserver::HandleClickEquip );

		_observers.insert( pair<SlotType const *, SlotObserver*>( type, observer ) );
	}

	void WornItemsObserver::StartObserving()
	{
		for ( auto pair : _observers )
		{
			pair.second->SetInteractive( _target->IsPlayable() );
			pair.second->Observe( &(_target->GetSlot( pair.first )) );
		}
	}

	void WornItemsObserver::StopObserving()
	{
		for ( auto pair : _observers )
		{
			pair.second->Observe( nullptr );
		}
	}

	void WornItemsObserver::HandleClickUnequip( SlotType const * type )
	{
		OnClickUnequip( type );
	}

	void WornItemsObserver::HandleClickEquip( SlotType const * type )
	{
		OnClickEquip( type );
	}
}