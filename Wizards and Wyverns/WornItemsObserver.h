#pragma once

#include "View.h"
#include "Observer.h"
#include "Character.h"
#include "SlotObserver.h"

namespace view
{
	class WornItemsObserver : public View, public Observer<Character>
	{
	public:
		WornItemsObserver( shared_ptr<sfg::Box> container );
		~WornItemsObserver();

		Event<SlotType const *> OnClickUnequip;
		Event<SlotType const *> OnClickEquip;

	protected:
		void CreateObserver( SlotType const * type );

		virtual void StartObserving();
		virtual void StopObserving();

		void HandleClickUnequip( SlotType const * type );
		void HandleClickEquip( SlotType const * type );

		map<SlotType const *, SlotObserver*> _observers;
	};
}

