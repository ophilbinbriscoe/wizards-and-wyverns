#pragma once

#include "Event.h"

template <typename type>
class Value
{
public:
	Value();
	Value( const Value& value );
	Value( const int& value );
	~Value();

	Event<type const * const> OnModified;

	virtual const int Min() const { return INT32_MIN; }
	virtual const int Max() const { return INT32_MAX; }

	operator int() const { return _value; }
	
	void operator = ( const Value& value );
	void operator = ( const int& value );
	void operator+= ( const int& value );
	void operator-= ( const int& value );

protected:
	int _value;
};

template<typename type>
inline Value<type>::Value() : _value( Min() )
{}

template<typename type>
inline Value<type>::Value( const Value & value ) : Value( (int) value )
{}

template <typename type>
inline Value<type>::Value( const int& value ) : _value( max( min( value, Max() ), Min() ) )
{}

template <typename type>
inline Value<type>::~Value()
{}

template <typename type>
inline void Value<type>::operator=( const Value & value )
{
	operator=( value._value );
}

template <typename type>
inline void Value<type>::operator= ( const int& value )
{
	// clamp the value to the legal range
	int clamped = max( min( value, Max() ), Min() );

	// check that the new value is actually different
	if ( clamped != _value )
	{
		// store the new value
		_value = clamped;

		// notify listeners that this stat has a new value
		OnModified( static_cast<type *>( this ) );
	}
}

template <typename type>
inline void Value<type>::operator+= ( const int& value )
{
	this->operator=( _value + value );
}

template <typename type>
inline void Value<type>::operator-= ( const int& value )
{
	this->operator=( _value - value );
}