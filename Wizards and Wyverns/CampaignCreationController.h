#pragma once

#include "Campaign.h"
#include "MenuController.h"

class CampaignCreationController : public MenuController
{
public:
	CampaignCreationController();
	~CampaignCreationController();

protected:
	void Reset();

	void HandleClickCreate();
	void HandleClickAppend();

	void HandleClickSave();
	void HandleClickQuit();
	void HandleClickClear();

	shared_ptr<sfg::Entry> _entryName;
	shared_ptr<sfg::ComboBox> _comboMaps;

	shared_ptr<sfg::Button> _buttonQuit;
	shared_ptr<sfg::Button> _buttonSave;
	shared_ptr<sfg::Button> _buttonBack;

	shared_ptr<sfg::Button> _buttonCreate;
	shared_ptr<sfg::Button> _buttonAdd;

	shared_ptr<sfg::Label> _labelName;
	shared_ptr<sfg::Box> _boxMaps;

	Campaign * _campaign;
	CampaignNode * _last;
};

