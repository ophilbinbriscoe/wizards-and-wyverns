#pragma once

#include <fstream>

#include "ItemContainer.h"
#include "Dataset.h"

class ItemContainerWriter
{
public:
	ItemContainerWriter( const Dataset<Item*> & items );
	virtual ~ItemContainerWriter();

	void Write( fstream& file, ItemContainer const * container ) const;

	unsigned int ItemOrder( Item const * item ) const;

private:
	map<Item const *, unsigned int> _order;
};

