#pragma once

#include "Strategy.h"

template <typename type>
class YieldStrategy : public BarrierStrategy<type>
{
public:
	YieldStrategy();
	~YieldStrategy();

	virtual void RequestDecision( Character const * const character, Map const * const map );	
};

template<typename type>
inline YieldStrategy<type>::YieldStrategy()
{}

template<typename type>
inline YieldStrategy<type>::~YieldStrategy()
{}

template<typename type>
void YieldStrategy<type>::RequestDecision( Character const * const character, Map const * const map )
{
	_yield = true;
	_ready = true;
}


