#pragma once

#include <tuple>
#include "Map.h"
#include "Character.h"

#include "Window.h"

namespace view
{
	class GridView : public Grid<sf::RectangleShape*>, public Window
	{
		static constexpr int DEFAULT_CELL_SIZE = 32;

	public:
		GridView( const int& width, const int& height, const int& cellSize = DEFAULT_CELL_SIZE );
		~GridView();

		Event<const Position&,sf::Mouse::Button> OnClickCell;

		int GetCellSize() const { return _cellSize; }

	private:
		int _cellSize;

		void HandleMouseButtonPress( Window * window, sf::Event::MouseButtonEvent mbe );
	};
}
