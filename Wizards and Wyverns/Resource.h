#pragma once

#include <string>

#include "Event.h"

using namespace std;

class Resource
{
public:
	Resource( const Resource& resource );
	Resource( const string& name = "" );
	~Resource();

	void SetName( const string& name );
	const string & GetName() const { return _name; }

	Event<Resource const *> OnRename;

	virtual bool Validate() const { return _name.length() > 0; }

private:
	string _name;
};

