//! @file 
//! @brief Implementation file for the ItemContainerTestSuite class
//!

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>

#include "ItemContainer.h"

using namespace CppUnit;

//! Test Class for the ItemContainer class
class ItemContainerTestSuite : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(ItemContainerTestSuite);
	CPPUNIT_TEST(ItemContainerAdd);
	CPPUNIT_TEST(ItemContainerAddExisting);
	CPPUNIT_TEST(ItemContainerRemoveExisting);
	CPPUNIT_TEST(ItemContainerRemoveNotExisting);
	CPPUNIT_TEST(ItemContainerRemoveQuantity);
	CPPUNIT_TEST(ItemContainerEqualOperator);
	CPPUNIT_TEST(ItemContainerGetItems);
	CPPUNIT_TEST_SUITE_END();
protected:
	void ItemContainerAdd();
	void ItemContainerAddExisting();
	void ItemContainerRemoveExisting();
	void ItemContainerRemoveNotExisting();
	void ItemContainerRemoveQuantity();
	void ItemContainerEqualOperator();
	void ItemContainerGetItems();
};

//! cppunit test cases registration
CPPUNIT_TEST_SUITE_REGISTRATION(ItemContainerTestSuite);

//! Tests creating an Item Container and adding a single item to it
//! Test Case: adding an item to the container should work
//! Tested item: ItemContainer::AddItem()
void ItemContainerTestSuite::ItemContainerAdd()
{
	ItemContainer * backpack = new ItemContainer();
	Equipment * eq1 = new Equipment("eq1", Slots().Body);
	CPPUNIT_ASSERT(backpack->AddItem(eq1) == 1);
}
//! Tests creating an Item Container and adding an item twice
//! Test Case: adding an item to the container twice should work
//! Tested item: ItemContainer::AddItem() && ItemContainer::GetItemQuantity()
void ItemContainerTestSuite::ItemContainerAddExisting()
{
	ItemContainer * backpack = new ItemContainer();
	Equipment * eq1 = new Equipment("eq1", Slots().Body);
	backpack->AddItem(eq1);
	backpack->AddItem(eq1);
	CPPUNIT_ASSERT(backpack->GetItemQuantity(eq1) == 2);
}

//! Tests creating an Item Container, adding an item and removing it
//! Test Case: removing an item from the container should work
//! Tested item: ItemContainer::RemoveItem()
void ItemContainerTestSuite::ItemContainerRemoveExisting()
{
	ItemContainer * backpack = new ItemContainer();
	Equipment * eq1 = new Equipment("eq1", Slots().Body);
	backpack->AddItem(eq1);
	CPPUNIT_ASSERT(backpack->RemoveItem(eq1));
}
//! Tests creating an Item Container and removing an item that is not in the container
//! Test Case: removing an item that is not in the container should not work
//! Tested item: ItemContainer::RemoveItem()
void ItemContainerTestSuite::ItemContainerRemoveNotExisting()
{
	ItemContainer * backpack = new ItemContainer();
	Equipment * eq1 = new Equipment("eq1", Slots().Body);
	Equipment * eq2 = new Equipment("eq2", Slots().Body);
	backpack->AddItem(eq1);
	CPPUNIT_ASSERT(!(backpack->RemoveItem(eq2)));
}
//! Tests creating an Item Container and adding an item twice then removes it once
//! Test Case: removing an item that is in the container more than once should only change it quantity
//! Tested item: ItemContainer::RemoveItem() && ItemContainer::GetItemQuantity()
void ItemContainerTestSuite::ItemContainerRemoveQuantity()
{
	ItemContainer * backpack = new ItemContainer();
	Equipment * eq1 = new Equipment("eq1", Slots().Body);
	backpack->AddItem(eq1);
	backpack->AddItem(eq1);
	backpack->RemoveItem(eq1);
	CPPUNIT_ASSERT(backpack->GetItemQuantity(eq1) == 1);
}

//! Tests creating an Item Container and making a copy
//! Test Case: Creating an item container, adding an item to it and making a copy of the container
//! should allow us to remove the added item from the copy.
//! Tested item: ItemContainer::Operator=() ItemContainer::AddItem()
void ItemContainerTestSuite::ItemContainerEqualOperator()
{
	ItemContainer * backpack = new ItemContainer();
	Equipment * eq1 = new Equipment("eq1", Slots().Body);
	backpack->AddItem(eq1);
	ItemContainer * backpack2 = backpack;
	CPPUNIT_ASSERT(backpack2->RemoveItem(eq1));
}
//! Tests creating an Item Container, adding an item to it and getting its list of items
//! Test Case: Getting the list of items from an Item Container should return its items
//! Tested item: ItemContainer::GetItemsRemoveItem() && ItemContainer::AddItem()
void ItemContainerTestSuite::ItemContainerGetItems()
{
	ItemContainer * backpack = new ItemContainer();
	Equipment * eq1 = new Equipment("eq1", Slots().Body);
	backpack->AddItem(eq1);
	backpack->AddItem(eq1);
	CPPUNIT_ASSERT(backpack->GetItems().at(eq1) == 2);
}