#pragma once

#include "Character.h"
#include "Observer.h"
#include "View.h"

namespace view
{
	class AbilitiesObserver : public View, public Observer<Character>
	{
	public:
		AbilitiesObserver( shared_ptr<sfg::Box> container );
		virtual ~AbilitiesObserver();

	private:
		void CreateAbilityLabels( StatType const * type, shared_ptr<sfg::Box> boxLabels, shared_ptr<sfg::Box> boxScores, shared_ptr<sfg::Box> boxBonuses, shared_ptr<sfg::Box> boxModifiers );
		
		void CreateScoreLabel( StatType const * type, shared_ptr<sfg::Box> box );
		void CreateBonusLabel( StatType const * type, shared_ptr<sfg::Box> box );
		void CreateModifierLabel( StatType const * type, shared_ptr<sfg::Box> box );
		
		virtual void StartObserving();
		virtual void StopObserving();
		
		void HandleScoreModified( Score const * const score );
		void HandleBonusModified( Bonus const * const bonus );
		void HandleModifierModified( Modifier const * const modifier );

		void SetScoreLabel( Score const * const score );
		void SetBonusLabel( Bonus const * const bonus );
		void SetModifierLabel( Modifier const * const modifier );
		
		shared_ptr<sfg::Box> _boxLabels;
		map<StatType const *, shared_ptr<sfg::Label>> _scoreLabels;
		map<StatType const *, shared_ptr<sfg::Label>> _bonusLabels;
		map<StatType const *, shared_ptr<sfg::Label>> _modifierLabels;
	};
}