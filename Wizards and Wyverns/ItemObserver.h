#pragma once

#include "Equipment.h"
#include "Observer.h"
#include "View.h"

class ItemObserver : public View, public Observer<Item>, protected AbstractUser
{
public:
	ItemObserver( shared_ptr<sfg::Box> container );
	~ItemObserver();

protected:
	virtual void Use( Item const * item );
	virtual void Use( Equipment const * equipment );

	void HandleRename( Resource const * resource );
	
	virtual void StartObserving();
	virtual void StopObserving();
	
	shared_ptr<sfg::Box> _box;
	shared_ptr<sfg::Label> _label;
	shared_ptr<sfg::Label> _enchantments;
};

