#pragma once

#include <string>
#include <vector>

#include "Item.h"
#include "Enchantment.h"
#include "SlotType.h"

class Equipment : public Item
{
public:
	Equipment( const string& name, SlotType const * const slot );
	Equipment( const string& name, SlotType const * const slot, const Enchantments& enchantments );
	virtual ~Equipment();

	const vector<Enchantment>& GetEnchantments() const;

	SlotType const * const slot;

	unsigned int IncreaseTo( StatType const * const type ) const;

	virtual void Accept( AbstractUser * user ) const;

private:
	bool Enchant( const Enchantment& enchantment );

	vector<Enchantment> _enchantments;
};
