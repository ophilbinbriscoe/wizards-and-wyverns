#include "ItemReader.h"

#pragma warning(disable:4996)

ItemReader::ItemReader( const char* filename ) : Reader( filename )
{}

ItemReader::~ItemReader()
{}

bool ItemReader::Read()
{
	Enchantments enchantments;

	char header1[8];
	char header2[8];
	char name[1024];

	int concept = 0, n = 0;

	int res = fscanf( _file, "%s ", header1 );

	if ( res == EOF )
	{
		// close file stream
		fclose( _file );

		// clean pointer
		_file = nullptr;

		// notify user of EOF
		return false;
	}

	// check if the equipment is gear
	if ( strcmp( header1, "item:" ) == 0 )
	{
		fgets( name, 1024, _file );
		name[strcspn( name, "\r\n" )] = 0;

		// cache the deserialized Item
		_result = new Item( name );

		return true;
	}
	else
	{
		int res = fscanf( _file, "%i %i ", &concept, &n );
	}

	// read all Enchantment pairs
	for ( int i = 0; i < n; i++ )
	{
		int stat = 0, bonus = 0;

		// scan the enchanted StatType and bonus
		fscanf( _file, "%i %i ", &stat, &bonus );

		// add the Enchantment to the vector
		enchantments.push_back( Enchantment( DomainConcept::Get<StatType>( stat ), bonus ) );
	}

	fgets( name, 1024, _file );
	name[strcspn( name, "\r\n" )] = 0;

	// check if the equipment is gear
	if ( strcmp( header1, "gear:" ) == 0 )
	{
		// cache the deserialized Equipment
		_result = new Equipment( name, DomainConcept::Get<SlotType>( concept ), enchantments );
	}

	// check if the equipment is a Weapon
	else if ( strcmp( header1, "weap:" ) == 0 )
	{
		// cache the deserialized Weapon
		_result = new Weapon( name, DomainConcept::Get<WeaponType>( concept ), enchantments );
	}

	return true;
}
