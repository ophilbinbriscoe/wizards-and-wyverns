#include "CampaignSelectionController.h"

#include "GameController.h"
#include "Database.h"

CampaignSelectionController::CampaignSelectionController( const Dataset<Campaign *>& dataset, Character * const player ) : SelectionMenuController( dataset ), _player( player )
{
	PackOptionButtons( "Campaigns", "No campaigns found." );
	Finalize();
}

CampaignSelectionController::~CampaignSelectionController()
{}

void CampaignSelectionController::HandleSelect( Campaign * campaign )
{
	Push( new GameController( campaign, _player ) );
}