#pragma once

#include "Game.h"
#include "Controller.h"

#include "StrategyProvider.h"
#include "UserBarrierStrategy.h"
#include "UserFreeStrategy.h"

#include "MapObserver.h"
#include "CharacterObserver.h"
#include "WornItemsObserver.h"
#include "ItemContainerObserver.h"

#include "GameLogger.h"
#include "DiceLogger.h"
#include "MapLogger.h"
#include "CharacterLogger.h"

static const string PROMPT_MOVEMENT_ACTION = "L/R/U/D arrow to move";
static const string PROMPT_INTERACT_ACTION = "Q/W/E/D/C/X/Z/A to interact";
static const string PROMPT_ADVANCE_ACTION = "ENTER to advance game";
static const string PROMPT_YIELD = "SPACE to yield phase";
static const string MESSAGE_NO_MOVEMENT = "no movement possible";
static const string MESSAGE_NO_INTERACT = "no available interactions";
static const string MESSAGE_AUTO = "auto-advancing game";

class GameController : public Controller, public StrategyProvider, public AbstractInspector
{
public:
	static const float MIN_PHASE_DURATION;
	static const float MAX_PHASE_DURATION;

	GameController( Campaign const * const campaign, Character const * const hero );
	virtual ~GameController();

	virtual BarrierStrategy<Position> * GetMovementStrategy( Character const * character ) const;
	virtual BarrierStrategy<MapObject const *> * GetInteractionStrategy( Character const * character ) const;
	virtual FreeStrategy<Item const *> * GetEquipStrategy( Character const * character ) const;
	virtual FreeStrategy<SlotType const *> * GetUnequipStrategy( Character const * character ) const;
	virtual FreeStrategy<StatType const *> * GetLevelStrategy( Character const * character ) const;

	void EnableAuto();
	void DisableAuto();
	const bool & IsAutoEnabled() const;

	void SetPhaseDuration( const float & seconds );
	const float & GetPhaseDuration();

	virtual void Update( const float & time );

	GameLogger * GetGameLogger() const;
	DiceLogger * GetDiceLogger() const;
	MapLogger * GetMapLogger() const;
	CharacterLogger * GetCharacterLogger() const;

protected:
	view::Window const * _top;

	void HandleKeyPress( view::Window* window, sf::Event::KeyEvent keyEvent );
	void HandleTileClicked( const Position& position );

	void Inspect( MapObject const * object );
	void Inspect( Character const * chest );
	void Inspect( Chest const * character );

	void HandleBeginTurn( Character const * const character );
	void HandleEndTurn( Character const * const character );

	void HandleBeginMovement( Character const * const character );
	void HandleEndMovement( Character const * const character );

	void HandleBeginInteraction( Character const * const character );
	void HandleEndInteraction( Character const * const character );

	void HandleMapFinished( Map const * const map );
	void HandleMapStarted( Map const * const map );

	void HandleObservedCharacterDeath( Character const * const character );

	void HandleToggleLog( Logger * logger, shared_ptr<sfg::CheckButton> button );

	void HandleDie();
	void HandleEnd();
	void HandleLevelUp( Character const * character );
	void HandleClickUnequip( SlotType const * type );
	void HandleClickEquip( SlotType const * type );
	void HandleEquipMenuCollapse( Controller * controller );
	void HandleLevelMenuCollapse( Controller * controller );

	bool CheckMovementAvailability();
	bool CheckInteractAvailability();
	void UpdateOffset();

	template<class type>
	void ClearObserver( type** observer );

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );

	void Decouple();

	bool _auto;
	bool _hold;

	float _delay;
	float _time;

	Game* _game;

	bool _break;
	bool _reset;
	
	bool _movement;
	bool _interaction;

	Character const * _inspected;

	shared_ptr<sfg::Box> _container;

	shared_ptr<sfg::Notebook> _notebookL;
	shared_ptr<sfg::Notebook> _notebookR;
	shared_ptr<sfg::Box> _notebookRBox;
	shared_ptr<sfg::Label> _notebookRLabel;

	shared_ptr<sfg::Button> _buttonSave;
	shared_ptr<sfg::Button> _buttonLoad;
	shared_ptr<sfg::Button> _buttonQuit;

	shared_ptr<sfg::Box> _boxMap;

	shared_ptr<sfg::Label> _labelPromptAction;
	shared_ptr<sfg::Label> _labelPromptYield;

	view::CharacterObserver* _playerObserver;
	view::WornItemsObserver* _playerWornObserver;
	view::ItemContainerObserver* _playerBackpackObserver;

	view::CharacterObserver* _npcObserver;
	view::WornItemsObserver* _npcWornObserver;
	view::ItemContainerObserver* _npcBackpackObserver;
	view::ItemContainerObserver* _chestObserver;

	view::MapObserver* _mapObserver;

	GameLogger * _gameLogger;
	DiceLogger * _diceLogger;
	MapLogger * _mapLogger;
	CharacterLogger * _characterLogger;

	UserBarrierStrategy<Position>* _movementStrategy;
	UserBarrierStrategy<MapObject const *>* _interactionStrategy;
	UserFreeStrategy<Item const *>* _equipStrategy;
	UserFreeStrategy<SlotType const *>* _unequipStrategy;
	UserFreeStrategy<StatType const *>* _levelStrategy;
};

template<class type>
inline void GameController::ClearObserver( type ** observer )
{
	if ( *observer )
	{
		(*observer)->GetContainer()->Show( false );
		(*observer)->Observe( nullptr );
		(*observer)->Observe( nullptr );
		delete (*observer);
		*observer = nullptr;
	}
}
