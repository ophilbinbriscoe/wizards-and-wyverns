#include "PauseMenuController.h"

#include "Database.h"

PauseMenuController::PauseMenuController( Game const * const game, GameController * const controller ) : MenuController(), _game( game ), _controller( controller )
{
	auto frame = sfg::Frame::Create( "Logging" );

	frame->SetAlignment( { 0.5f, 0.0f } );

	auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	_gameToggle = sfg::CheckButton::Create( "Game" );
	_diceToggle = sfg::CheckButton::Create( "Dice" );
	_mapToggle = sfg::CheckButton::Create( "Map" );
	_characterToggle = sfg::CheckButton::Create( "Character" );

	_gameToggle->GetSignal( sfg::CheckButton::OnToggle ).Connect( bind( &PauseMenuController::HandleToggleGameLogger, this ) );
	_diceToggle->GetSignal( sfg::CheckButton::OnToggle ).Connect( bind( &PauseMenuController::HandleToggleDiceLogger, this ) );
	_mapToggle->GetSignal( sfg::CheckButton::OnToggle ).Connect( bind( &PauseMenuController::HandleToggleMapLogger, this ) );
	_characterToggle->GetSignal( sfg::CheckButton::OnToggle ).Connect( bind( &PauseMenuController::HandleToggleCharacterLogger, this ) );

	_gameToggle->SetActive( _controller->GetGameLogger()->enabled );
	_diceToggle->SetActive( _controller->GetDiceLogger()->enabled );
	_mapToggle->SetActive( _controller->GetMapLogger()->enabled );
	_characterToggle->SetActive( _controller->GetCharacterLogger()->enabled );

	box->Pack( _gameToggle );
	box->Pack( _diceToggle );
	box->Pack( _mapToggle );
	box->Pack( _characterToggle );

	frame->Add( box );

	_menu->Pack( frame );

	_autoCombo = sfg::ComboBox::Create();

	_autoCombo->AppendItem( "Manual" );
	_autoCombo->AppendItem( "Instant" );
	_autoCombo->AppendItem( "Slow" );
	_autoCombo->AppendItem( "Fast" );

	_autoCombo->GetSignal( sfg::ComboBox::OnSelect ).Connect( bind( &PauseMenuController::HandleSelectAuto, this ) );

	if ( _controller->IsAutoEnabled() )
	{
		if ( _controller->GetPhaseDuration() == GameController::MIN_PHASE_DURATION )
		{
			_autoCombo->SelectItem( 1 );
		}
		else if ( _controller->GetPhaseDuration() == GameController::MAX_PHASE_DURATION )
		{
			_autoCombo->SelectItem( 3 );
		}
		else
		{
			_autoCombo->SelectItem( 2 );
		}
	}
	else
	{
		_autoCombo->SelectItem( 0 );
	}

	_menu->Pack( sfg::Label::Create( "Advancement" ) );
	_menu->Pack( _autoCombo );


	auto buttonSave = sfg::Button::Create( "Save" );
	auto buttonLoad = sfg::Button::Create( "Load" );
	auto buttonQuit = sfg::Button::Create( "Quit" );
	auto buttonBack = sfg::Button::Create( "Back" );

	buttonSave->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &PauseMenuController::HandleClickSave, this ) );
	buttonLoad->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &PauseMenuController::HandleClickLoad, this ) );
	buttonQuit->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &PauseMenuController::HandleClickQuit, this ) );
	buttonBack->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &PauseMenuController::HandleClickBack, this ) );

	//_menu->Pack( buttonSave );
	//_menu->Pack( buttonLoad );
	_menu->Pack( buttonQuit );
	_menu->Pack( buttonBack );

	Finalize();

	_autoCombo->Show( _controller->IsAutoEnabled() );
}

PauseMenuController::~PauseMenuController()
{}

void PauseMenuController::HandleToggleGameLogger()
{
	_controller->GetGameLogger()->enabled = _gameToggle->IsActive();
}

void PauseMenuController::HandleToggleDiceLogger()
{
	_controller->GetDiceLogger()->enabled = _diceToggle->IsActive();
}

void PauseMenuController::HandleToggleMapLogger()
{
	_controller->GetMapLogger()->enabled = _mapToggle->IsActive();
}

void PauseMenuController::HandleToggleCharacterLogger()
{
	_controller->GetCharacterLogger()->enabled = _characterToggle->IsActive();
}

void PauseMenuController::HandleSelectAuto()
{
	switch ( _autoCombo->GetSelectedItem() )
	{
		case 0:
		_controller->DisableAuto();
		break;

		case 1:
		_controller->EnableAuto();
		_controller->SetPhaseDuration( GameController::MIN_PHASE_DURATION );
		break;

		case 2:
		_controller->EnableAuto();
		_controller->SetPhaseDuration( GameController::MAX_PHASE_DURATION );
		break;

		case 3:
		_controller->EnableAuto();
		_controller->SetPhaseDuration( (GameController::MIN_PHASE_DURATION + GameController::MAX_PHASE_DURATION) / 2.0f );
		break;
	}
}

void PauseMenuController::HandleClickSave()
{
	Pop( this );
	Push( new MessageController( "Not implemented.", "OK" ) );
}

void PauseMenuController::HandleClickLoad()
{
	Pop( this );
	Push( new MessageController( "Not implemented.", "OK" ) );
}

void PauseMenuController::HandleClickQuit()
{
	Collapse();
}

void PauseMenuController::HandleClickBack()
{
	Pop( this );
}

void PauseMenuController::HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke )
{
	if ( ke.code == sf::Keyboard::Escape )
	{
		HandleClickBack();
	}
}

void PauseMenuController::SubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Subscribe( this, &PauseMenuController::HandleKeyPressed );
}

void PauseMenuController::UnsubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Unsubscribe( this, &PauseMenuController::HandleKeyPressed );
}



