#pragma once

#include "SelectionMenuController.h"
#include "Character.h"

class HeroSelectionController : public SelectionMenuController<Character>
{
public:
	HeroSelectionController( const Dataset<Character *>& dataset );
	virtual ~HeroSelectionController();

protected:
	virtual void HandleSelect( Character * character );
};

