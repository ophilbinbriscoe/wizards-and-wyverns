#pragma once

#include "Strategy.h"

class AggressorStrategy : public BarrierStrategy<MapObject const *>
{
public:
	AggressorStrategy( Character const * target );
	virtual ~AggressorStrategy();

	virtual void RequestDecision( Character const * character, Map const * map );

private:
	Character const * _target;
};

