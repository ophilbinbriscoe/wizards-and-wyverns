#include "FollowStrategy.h"

FollowStrategy::FollowStrategy( Character const * target ) : _target( target )
{}

FollowStrategy::~FollowStrategy()
{}

void FollowStrategy::RequestDecision( Character const * character, Map const * map )
{
	_yield = false;

	// check if Character is adjacent to target
	if ( map->IsAdjacent( character, map->GetPosition( _target ) ) )
	{
		_yield = true;
	}
	else
	{
		Position delta = map->GetPosition( _target ) - map->GetPosition( character );

		if ( abs( delta.x ) >= abs( delta.y ) )
		{
			delta.y = 0;

			if ( delta.x > 0 )
			{
				delta.x = 1;
			}
			else
			{
				delta.x = -1;
			}
		}
		else
		{
			delta.x = 0;

			if ( delta.y > 0 )
			{
				delta.y = 1;
			}
			else
			{
				delta.y = -1;
			}
		}

		Position position = map->GetPosition( character ) + delta;

		if ( map->Contains( position ) && !map->IsOccupied( position ) )
		{
			_decision = position;
		}
		else
		{
			_yield = true;
		}
	}

	_ready = true;
}
