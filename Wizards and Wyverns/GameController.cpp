#include "GameController.h"

#include "GameOverDialogController.h"
#include "VictoryMessageController.h"
#include "PauseMenuController.h"
#include "LevelUpController.h"
#include "EquipMenuController.h"
#include "LevelBasedAdjuster.h"

#include "AbstractStrategyProvider.h"
#include "NPCStrategyProvider.h"
#include "FollowStrategy.h"
#include "AggressorStrategy.h"
#include "YieldStrategy.h"
#include "DummyStrategy.h"
#include "MaximizeBonusStrategy.h"

#include "CharacterBuilder.h"

const float GameController::MIN_PHASE_DURATION = 0.0f;
const float GameController::MAX_PHASE_DURATION = 1.0f;

GameController::GameController( Campaign const * const campaign, Character const * const hero )
{
	// create a CharacterBuilder to clone the hero, so that the database version of the Character is not modified during play
	CharacterBuilder builder( true, hero->GetLevel(), hero->GetClass(), hero->GetAlignment(), hero->GetName() );

	// copy hit point max and damage taken
	builder.CopyHitPointsFrom( hero );

	// copy ability scores
	builder.CopyScoresFrom( hero );

	// copy backpack
	builder.CopyBackpackFrom( hero );

	// copy worn items
	builder.CopyWornFrom( hero );

	// create the clone
	Character * clone = builder.Create();

	// create the provider for NPC strategies
	NPCStrategyProvider * npcStrategyProvider = new NPCStrategyProvider(

		new FollowStrategy( clone ),				// always move towards the target Character
		new AggressorStrategy( clone ),				// aggressor attacks target Character when adjacent
		new YieldStrategy<MapObject const *>(),	// friendly strategy always yields interaction phase
		new MaximizeBonusStrategy(),				// always wear the best equipment
		new DummyStrategy<SlotType const *>(),		// never unequip anything
		new DummyStrategy<StatType const *>()		// never increase an ability score
		);

	// relies on external input to make movement decisions
	_movementStrategy = new UserBarrierStrategy<Position>();

	// relies on external input to make interaction decisions
	_interactionStrategy = new UserBarrierStrategy<MapObject const *>();

	// forwards external equip requests
	_equipStrategy = new UserFreeStrategy<Item const *>();

	// forwards external unequip requests
	_unequipStrategy = new UserFreeStrategy<SlotType const *>();

	// relies on external input to make ability score increase decisions at level-up time
	_levelStrategy = new UserFreeStrategy<StatType const *>();

	// create the Game
	_game = new Game(
		
		campaign,													// the Campaign
		clone,														// the user's Character (hero)
		new AbstractStrategyProvider( this, npcStrategyProvider ),	// selects the appropriate provider for a player or NPC Character
		new LevelBasedAdjuster( clone ) );							// adjusts a Map based on the level of the target Character

	// listen for turn events to ensure that the CharacterLogger is always logging for the active Character
	_game->OnBeginTurn.Subscribe( this, &GameController::HandleBeginTurn );
	_game->OnEndTurn.Subscribe( this, &GameController::HandleEndTurn );

	// listen for movement phase events
	_game->OnBeginMovement.Subscribe( this, &GameController::HandleBeginMovement );
	_game->OnEndMovement.Subscribe( this, &GameController::HandleEndMovement );

	// listen for interaction phase events
	_game->OnBeginInteraction.Subscribe( this, &GameController::HandleBeginInteraction );
	_game->OnEndInteraction.Subscribe( this, &GameController::HandleEndInteraction );

	// listen for Map transition events
	_game->OnFinishMap.Subscribe( this, &GameController::HandleMapFinished );
	_game->OnStartMap.Subscribe( this, &GameController::HandleMapStarted );

	// listen for win / lose events
	_game->OnGameOver.Subscribe( this, &GameController::HandleDie );
	_game->OnFinishCampaign.Subscribe( this, &GameController::HandleEnd );

	// listen for hero's level-up events
	_game->GetHero()->OnLevelUp.Subscribe( this, &GameController::HandleLevelUp );

	// create loggers

	_gameLogger = new GameLogger();
	_diceLogger = new DiceLogger();
	_mapLogger = new MapLogger();
	_characterLogger = new CharacterLogger();

	// connect Game and Dice loggers

	_gameLogger->Observe( _game );
	_diceLogger->Observe( Dice::Singleton );

	_container = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 10.0f );

	auto boxPlayer = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 10.0f );
	auto boxWorn = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );
	auto boxBackpack = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	_playerObserver = new view::CharacterObserver( boxPlayer );
	_playerWornObserver = new view::WornItemsObserver( boxWorn );
	_playerBackpackObserver = new view::ItemContainerObserver( boxBackpack );

	_playerObserver->Observe( _game->GetHero() );
	_playerWornObserver->Observe( _game->GetHero() );
	_playerBackpackObserver->Observe( _game->GetHero()->GetBackpack() );

	_playerWornObserver->OnClickUnequip.Subscribe( this, &GameController::HandleClickUnequip );
	_playerWornObserver->OnClickEquip.Subscribe( this, &GameController::HandleClickEquip );

	_notebookL = sfg::Notebook::Create();

	_notebookL->SetScrollable( true );

	_notebookL->AppendPage( boxPlayer, sfg::Label::Create( _game->GetHero()->GetName() ) );
	_notebookL->AppendPage( boxWorn, sfg::Label::Create( "Worn" ) );
	_notebookL->AppendPage( boxBackpack, sfg::Label::Create( "Backpack" ) );

	_boxMap = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	_mapObserver = new view::MapObserver( _boxMap );
	_mapObserver->Observe( _game->GetMap() );
	_mapObserver->OnTileClicked.Subscribe( this, &GameController::HandleTileClicked );

	_auto = true;

	_labelPromptAction = sfg::Label::Create( MESSAGE_AUTO );
	_labelPromptYield = sfg::Label::Create( "" );

	auto boxCenter = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 0.0f );

	auto boxCenterAlignment = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 0.0f );

	auto boxCenterNested = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	auto alignmentCenterHorizontal = sfg::Alignment::Create();

	alignmentCenterHorizontal->SetScale( { 0.0f, 0.0f } );
	alignmentCenterHorizontal->SetAlignment( { 0.5f,1.0f } );
	alignmentCenterHorizontal->Add( boxCenterAlignment );

	auto alignmentCenterVertical = sfg::Alignment::Create();

	alignmentCenterVertical->SetScale( { 0.0f, 0.0f } );
	alignmentCenterVertical->SetAlignment( { 0.5f,1.0f } );
	alignmentCenterVertical->Add( boxCenterNested );

	boxCenterNested->Pack( _boxMap, true, true );
	boxCenterNested->Pack( _labelPromptAction, true, true );
	boxCenterNested->Pack( _labelPromptYield, true, true );

	boxCenterAlignment->Pack( alignmentCenterVertical, true, true );

	_notebookR = sfg::Notebook::Create();

	_notebookR->AppendPage( sfg::Box::Create(), sfg::Label::Create( "          " ) );
	
	_notebookL->SetRequisition( { 300.0f, 500.0f } );
	 boxCenter->SetRequisition( { 570.0f, 500.0f } );
	_notebookR->SetRequisition( { 300.0f, 500.0f } );

	boxCenter->Pack( alignmentCenterHorizontal, true, true );

	_container->Pack( _notebookL );
	_container->Pack( boxCenter );
	_container->Pack( _notebookR );

	_window->Add( _container );

	_notebookR->RemoveAll();

	// set delay
	_delay = (MIN_PHASE_DURATION / MAX_PHASE_DURATION) + 0.5f;

	// stay execution
	_hold = true;

	// start the Game
	_game->Start();
}

GameController::~GameController()
{
	// decouple
	Decouple();

	// clean up Observers
	delete _playerObserver;
	delete _playerWornObserver;
	delete _playerBackpackObserver;
	delete _mapObserver;

	// clean up Game
	delete _game;
}

BarrierStrategy<Position>* GameController::GetMovementStrategy( Character const * character ) const
{
	return _movementStrategy;
}

BarrierStrategy<MapObject const*>* GameController::GetInteractionStrategy( Character const * character ) const
{
	return _interactionStrategy;
}

FreeStrategy<Item const*>* GameController::GetEquipStrategy( Character const * character ) const
{
	return _equipStrategy;
}

FreeStrategy<SlotType const*>* GameController::GetUnequipStrategy( Character const * character ) const
{
	return _unequipStrategy;
}

FreeStrategy<StatType const*>* GameController::GetLevelStrategy( Character const * character ) const
{
	return _levelStrategy;
}

void GameController::EnableAuto()
{
	_auto = true;

	if ( !_movement && !_interaction && _break )
	{
		_labelPromptAction->SetText( _auto ? MESSAGE_AUTO : PROMPT_ADVANCE_ACTION );
	}
}

void GameController::DisableAuto()
{
	_auto = false;

	if ( !_movement && !_interaction && _break )
	{
		_labelPromptAction->SetText( _auto ? MESSAGE_AUTO : PROMPT_ADVANCE_ACTION );
	}
}

const bool & GameController::IsAutoEnabled() const
{
	return _auto;
}

void GameController::SetPhaseDuration( const float & seconds )
{
	_delay = max( min( seconds, MAX_PHASE_DURATION ), MIN_PHASE_DURATION );
}

const float & GameController::GetPhaseDuration()
{
	return _delay;
}

void GameController::Update( const float & time )
{
	if ( _game->HasStarted() )
	{
		if ( _auto && !_movement && !_interaction && !_hold )
		{
			_time += time / 20.0f;

			if ( _time >= _delay )
			{
				_break = false;
			}
		}

		if ( !_break )
		{
			while ( !_break )
			{
				_game->Advance();
			}

			_time = 0.0f;
		}
	}
}

GameLogger * GameController::GetGameLogger() const
{
	return _gameLogger;
}

DiceLogger * GameController::GetDiceLogger() const
{
	return _diceLogger;
}

MapLogger * GameController::GetMapLogger() const
{
	return _mapLogger;
}

CharacterLogger * GameController::GetCharacterLogger() const
{
	return _characterLogger;
}

void GameController::HandleKeyPress( view::Window * window, sf::Event::KeyEvent keyEvent )
{
	Character const * hero = _game->GetHero();
	Map const * map = _game->GetMap();

	Position movement = Position::ZERO;
	Position interact = Position::ZERO;

	bool yield = false;

	switch ( keyEvent.code )
	{
		case sf::Keyboard::Escape:
		Push( new PauseMenuController( _game, this ) );
		return;

		case sf::Keyboard::Return:
		if ( _movement || _interaction )
		{
			// do nothing
		}
		else
		{
			// advance game
			_break = false;
		}
		return;

		case sf::Keyboard::Space:
		yield = true;
		break;
	}

	if ( _movement )
	{
		switch ( keyEvent.code )
		{
			case sf::Keyboard::Right:
			movement = Position::RIGHT;
			break;

			case sf::Keyboard::Left:
			movement = Position::LEFT;
			break;

			case sf::Keyboard::Up:
			movement = Position::UP;
			break;

			case sf::Keyboard::Down:
			movement = Position::DOWN;
			break;
		}
		
		if ( yield )
		{
			_movementStrategy->Yield();

			while ( !_interaction )
			{
				_game->Advance();
			}
		}
		else if ( movement != Position::ZERO )
		{
			Position position = map->GetPosition( hero ) + movement;

			if ( map->Contains( position ) && !map->IsOccupied( position ) )
			{
				_movementStrategy->Decide( position );

				while ( !_interaction && !_reset )
				{
					_game->Advance();
				}

				if ( !_game->HasEnded() )
				{
					UpdateOffset();
				}

				_reset = false;
			}
		}

		return;
	}

	if ( _interaction )
	{
		switch ( keyEvent.code )
		{
			case sf::Keyboard::W:
			interact = Position::UP;
			break;

			case sf::Keyboard::A:
			interact = Position::LEFT;
			break;

			case sf::Keyboard::D:
			interact = Position::RIGHT;
			break;

			case sf::Keyboard::X:
			interact = Position::DOWN;
			break;

			case sf::Keyboard::Q:
			interact = Position::LEFT + Position::UP;
			break;

			case sf::Keyboard::E:
			interact = Position::RIGHT + Position::UP;
			break;

			case sf::Keyboard::C:
			interact = Position::RIGHT + Position::DOWN;
			break;

			case sf::Keyboard::Z:
			interact = Position::LEFT + Position::DOWN;
			break;
		}

		if ( yield )
		{
			_interactionStrategy->Yield();

			_game->Advance();
			_game->Advance();

			_break = _interaction;
		}
		else if ( interact != Position::ZERO )
		{
			Position position = map->GetPosition( hero ) + interact;

			if ( map->Contains( position ) && map->IsObjectAt( position ) )
			{
				_interactionStrategy->Decide( map->GetObjectAt( position ) );

				_game->Advance();
				_game->Advance();

				_break = _interaction;
			}
		}
		return;
	}
}

void GameController::HandleTileClicked( const Position & position )
{
	Map const * map = _game->GetMap();
	
	cout << "click " << position << ": " << map->GetTileAt( position ) << endl;

	if ( _inspected )
	{
		_inspected->OnDie.Unsubscribe( this, &GameController::HandleObservedCharacterDeath );
		_inspected = nullptr;
	}

	if ( map->Contains( position ) && map->IsObjectAt( position ) )
	{
		map->GetObjectAt( position )->Accept( this );

		return;
	}

	ClearObserver( &_npcObserver );
	ClearObserver( &_npcWornObserver );
	ClearObserver( &_npcBackpackObserver );
	ClearObserver( &_chestObserver );

	_notebookR->RemoveAll();
}

void GameController::Inspect( MapObject const * object )
{
	ClearObserver( &_npcObserver );
	ClearObserver( &_chestObserver );

	_notebookR->RemoveAll();
}

void GameController::Inspect( Character const * character )
{
	if ( character == _game->GetHero() )
	{
		_notebookL->SetCurrentPage( 0 );

		return;
	}

	auto boxCharacter = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 10.0f );
	auto boxWornItems = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 10.0f );

	ClearObserver( &_chestObserver );
	ClearObserver( &_npcObserver );
	ClearObserver( &_npcWornObserver );
	ClearObserver( &_npcBackpackObserver );

	_npcObserver = new view::CharacterObserver( boxCharacter );
	_npcObserver->Observe( character );

	_npcWornObserver = new view::WornItemsObserver( boxWornItems );
	_npcWornObserver->Observe( character );

	for ( int i = 0; i < _notebookR->GetPageCount(); i++ )
	{
		_notebookR->GetNthPage( i )->Show( false );
	}

	_notebookR->RemoveAll();
	_notebookR->AppendPage( boxCharacter, sfg::Label::Create( character->GetName() ) );
	_notebookR->AppendPage( boxWornItems, sfg::Label::Create( "Worn" ) );

	if ( character->GetStat( Stats().HitPoints ) == 0 )
	{
		auto boxBackpack = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 10.0f );

		_npcBackpackObserver = new view::ItemContainerObserver( boxBackpack );
		_npcBackpackObserver->Observe( character->GetBackpack() );

		_notebookR->AppendPage( boxBackpack, sfg::Label::Create( "Backpack" ) );
	}

	character->OnDie.Subscribe( this, &GameController::HandleObservedCharacterDeath );

	_inspected = character;

	_notebookR->RefreshAll();
}

void GameController::Inspect( Chest const* chest )
{
	auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	ClearObserver( &_npcObserver );
	ClearObserver( &_npcWornObserver );
	ClearObserver( &_npcBackpackObserver );

	if ( _chestObserver == nullptr )
	{
		_chestObserver = new view::ItemContainerObserver( box );
	}
	else
	{
		_chestObserver->Observe( nullptr );
		_chestObserver->SetContainer( box );
	}

	_chestObserver->Observe( chest );

	for ( int i = 0; i < _notebookR->GetPageCount(); i++ )
	{
		_notebookR->GetNthPage( i )->Show( false );
	}

	_notebookR->RemoveAll();
	_notebookR->AppendPage( box, sfg::Label::Create( chest->GetName() ) );
	_notebookR->RefreshAll();
}

void GameController::HandleBeginTurn( Character const * const character )
{
	// CharacterLogger always logs the active Character's attacks
	_characterLogger->Observe( character );

	// check if the user's Character just ended their turn
	if ( character == _game->GetHero() )
	{
		_break = false;
	}
	else
	{
		_break = true;
	}
}

void GameController::HandleEndTurn( Character const * const character )
{}

void GameController::HandleBeginMovement( Character const * const character )
{
	// check if the user's Character just started their movement phase
	if ( character == _game->GetHero() )
	{
		_break = true;
		_movement = true;

		if ( CheckMovementAvailability() )
		{
			_labelPromptAction->SetText( PROMPT_MOVEMENT_ACTION );
		}
		else
		{
			_labelPromptAction->SetText( MESSAGE_NO_MOVEMENT );
		}

		_labelPromptYield->SetText( PROMPT_YIELD );
	}
}

void GameController::HandleEndMovement( Character const * const character )
{
	_time = 0.0f;

	// check if the user's Character just finished their movement phase
	if ( character == _game->GetHero() )
	{
		_break = false;
		_movement = false;
	}
}

void GameController::HandleBeginInteraction( Character const * const character )
{
	// check if the user's Character just started their interaction phase
	if ( character == _game->GetHero() )
	{
		_break = true;
		_interaction = true;

		if ( CheckInteractAvailability() )
		{
			_labelPromptAction->SetText( PROMPT_INTERACT_ACTION );
		}
		else
		{
			_labelPromptAction->SetText( MESSAGE_NO_INTERACT );
		}

		_labelPromptYield->SetText( PROMPT_YIELD );
	}
}

void GameController::HandleEndInteraction( Character const * const character )
{
	_time = 0.0f;

	// check if the user's Character just finished their interaction phase
	if ( character == _game->GetHero() )
	{
		_break = false;
		_interaction = false;

		_labelPromptAction->SetText( _auto ? MESSAGE_AUTO : PROMPT_ADVANCE_ACTION );
		_labelPromptYield->SetText( "" );
	}
}

void GameController::HandleMapFinished( Map const * map )
{
	// decouple the MapObserver
	_mapObserver->Observe( nullptr );

	// decouple the MapLogger
	_mapLogger->Observe( nullptr );

	// stay execution
	_hold = true;

	// stay execution
	_break = true;

	// reset
	_reset = true;

	_interaction = false;

	_movement = false;

	if ( _inspected )
	{
		_inspected->OnDie.Unsubscribe( this, &GameController::HandleObservedCharacterDeath );
		_inspected = nullptr;
	}

	ClearObserver( &_npcObserver );
	ClearObserver( &_npcWornObserver );
	ClearObserver( &_npcBackpackObserver );
	ClearObserver( &_chestObserver );

	if ( _notebookR->GetPageCount() > 0 )
	{
		_notebookR->GetNthPage( _notebookR->GetCurrentPage() )->Show( false );
	}

	_notebookR->RemoveAll();
}

void GameController::HandleMapStarted( Map const * map )
{
	// center the view around the user's Character
	UpdateOffset();

	// show the Map
	_mapObserver->Observe( map );

	// begin logging for the Map
	_mapLogger->Observe( map );

	// resume
	_hold = false;

	_labelPromptAction->SetText( _auto ? MESSAGE_AUTO : PROMPT_ADVANCE_ACTION );
	_labelPromptYield->SetText( "" );
}

void GameController::HandleObservedCharacterDeath( Character const * const character )
{
	auto boxBackpack = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 10.0f );

	_npcBackpackObserver = new view::ItemContainerObserver( boxBackpack );
	_npcBackpackObserver->Observe( character->GetBackpack() );

	_notebookR->AppendPage( boxBackpack, sfg::Label::Create( "Backpack" ) );

	character->OnDie.Unsubscribe( this, &GameController::HandleObservedCharacterDeath );
}

void GameController::HandleToggleLog( Logger * logger, shared_ptr<sfg::CheckButton> button )
{
	logger->enabled = button->IsActive();
}

void GameController::HandleDie()
{
	// show the game-over dialog
	Push( new GameOverDialogController() );
}

void GameController::HandleEnd()
{}

void GameController::HandleLevelUp( Character const * character )
{
	auto menu = new LevelUpController( character, _levelStrategy );

	// show the level-up dialog
	Push( menu );

	menu->Pop.Subscribe( this, &GameController::HandleLevelMenuCollapse );
}

void GameController::HandleClickUnequip( SlotType const * type )
{
	// forward the unequip request
	_unequipStrategy->Decide( _game->GetHero(), type );
}

void GameController::HandleClickEquip( SlotType const * type )
{
	EquipMenuController * menu = new EquipMenuController( _game->GetHero()->GetBackpack(), type );

	menu->Pop.Subscribe( this, &GameController::HandleEquipMenuCollapse );

	// show equipment selection menu
	Push( menu );
}

void GameController::HandleEquipMenuCollapse( Controller * controller )
{
	auto menu = static_cast<EquipMenuController *>(controller);

	if ( menu->WasCancelled() )
	{
		// do nothing
	}

	else
	{
		// forward the equip request
		_equipStrategy->Decide( _game->GetHero(), menu->GetSelection() );
	}
}

void GameController::HandleLevelMenuCollapse( Controller * controller )
{
	if ( _game->HasEnded() )
	{
		Push( new VictoryMessageController( _game ) );
	}
}

bool GameController::CheckMovementAvailability()
{
	Map const * map = _game->GetMap();
	Position player = map->GetPosition( _game->GetHero() );

	int possible = 4;

	if ( !map->Contains( player + Position::RIGHT ) || map->IsOccupied( player + Position::RIGHT ) )
	{
		possible--;
	}

	if ( !map->Contains( player + Position::LEFT ) || map->IsOccupied( player + Position::LEFT ) )
	{
		possible--;
	}

	if ( !map->Contains( player + Position::UP ) || map->IsOccupied( player + Position::UP ) )
	{
		possible--;
	}

	if ( !map->Contains( player + Position::DOWN ) || map->IsOccupied( player + Position::DOWN ) )
	{
		possible--;
	}

	return possible > 0;
}

bool GameController::CheckInteractAvailability()
{
	Map const * map = _game->GetMap();
	Position player = map->GetPosition( _game->GetHero() );

	if ( map->Contains( player + Position::RIGHT ) && map->IsObjectAt( player + Position::RIGHT ) )
	{
		return true;
	}

	if ( map->Contains( player + Position::LEFT ) && map->IsObjectAt( player + Position::LEFT ) )
	{
		return true;
	}

	if ( map->Contains( player + Position::UP ) && map->IsObjectAt( player + Position::UP ) )
	{
		return true;
	}

	if ( map->Contains( player + Position::DOWN ) && map->IsObjectAt( player + Position::DOWN ) )
	{
		return true;
	}

	if ( map->Contains( player + Position::UP + Position::RIGHT ) && map->IsObjectAt( player + Position::UP + Position::RIGHT ) )
	{
		return true;
	}

	if ( map->Contains( player + Position::DOWN + Position::LEFT ) && map->IsObjectAt( player + Position::DOWN + Position::LEFT ) )
	{
		return true;
	}

	if ( map->Contains( player + Position::UP + Position::LEFT ) && map->IsObjectAt( player + Position::UP + Position::LEFT ) )
	{
		return true;
	}

	if ( map->Contains( player + Position::DOWN + Position::RIGHT ) && map->IsObjectAt( player + Position::DOWN + Position::RIGHT ) )
	{
		return true;
	}

	return false;
}

void GameController::UpdateOffset()
{
	int width = _game->GetMap()->GetWidth();
	int height = _game->GetMap()->GetHeight();

	int padding = 4;
	int view = 10;

	if ( width > view || height > view )
	{
		Position player = _game->GetMap()->GetPosition( _game->GetHero() );

		Position offset = _mapObserver->GetOffset();

		if ( _mapObserver->MapToView( player ).x < padding )
		{
			offset.x = player.x - padding;
		}
		else if ( _mapObserver->MapToView( player ).x > view - 1 - padding )
		{
			offset.x = player.x - (view - 1) + padding;
		}

		if ( _mapObserver->MapToView( player ).y < padding )
		{
			offset.y = player.y- padding;
		}
		else if ( _mapObserver->MapToView( player ).y > view - 1 - padding )
		{
			offset.y = player.y - (view - 1) + padding;
		}

		offset.x = max( 0, min( width - 10, offset.x ) );
		offset.y = max( 0, min( width - 10, offset.y ) );

		_mapObserver->SetOffset( offset );
	}
	else
	{
		_mapObserver->SetOffset( Position::ZERO );
	}
}

void GameController::SubscribeInputHandlers( view::Window * window )
{
	_mapObserver->SetWindow( window );

	// listen for key press events
	window->OnKeyPressed.Subscribe( this, &GameController::HandleKeyPress );
}

void GameController::UnsubscribeInputHandlers( view::Window * window )
{
	// stop listening for key press events
	window->OnKeyPressed.Unsubscribe( this, &GameController::HandleKeyPress );
}

void GameController::Decouple()
{
	_game->OnFinishMap.Unsubscribe( this, &GameController::HandleMapFinished );
	_game->OnStartMap.Unsubscribe( this, &GameController::HandleMapStarted );
	_game->OnBeginTurn.Unsubscribe( this, &GameController::HandleBeginTurn );
	_game->OnEndTurn.Unsubscribe( this, &GameController::HandleEndTurn );
	_game->OnBeginMovement.Unsubscribe( this, &GameController::HandleBeginMovement );
	_game->OnEndMovement.Unsubscribe( this, &GameController::HandleEndMovement );
	_game->OnBeginInteraction.Unsubscribe( this, &GameController::HandleBeginInteraction );
	_game->OnEndInteraction.Unsubscribe( this, &GameController::HandleEndInteraction );

	if ( _inspected )
	{
		_inspected->OnDie.Unsubscribe( this, &GameController::HandleObservedCharacterDeath );
		_inspected = nullptr;
	}

	_mapObserver->Observe( nullptr );
	
	_playerObserver->Observe( nullptr );
	_playerBackpackObserver->Observe( nullptr );

	ClearObserver( &_npcObserver );
	ClearObserver( &_npcWornObserver );
	ClearObserver( &_npcBackpackObserver );
	ClearObserver( &_chestObserver );
}