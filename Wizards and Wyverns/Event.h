#pragma once

#include <vector>
#include <algorithm>
#include <functional>

using namespace std;

/// Generic base-class for EventHandlers.
/// A base-class is necessary since different EventHandlerMember instances need to point to different types of objects.
template <class ...arg> class AbstractEventHandler
{
public:
	AbstractEventHandler();
	~AbstractEventHandler();

	/// Virtual method for passing a set of event arguments.
	virtual void Handle( arg ... arguments ) = 0;
};

/// Specified EventHandler sub-class holding pointers to an instance and a member function of that instance.
template <class type, class ...arg> class EventHandler : public AbstractEventHandler<arg...>
{
public:
	// typedef for a member function pointer belonging to a specific class
	typedef void(type::*MemberPointer)(arg ... arguments);

	/// Create a new EventHandlerMember, pointing to a specific instance and member function of that instance.
	EventHandler( type* object, MemberPointer member );
	~EventHandler();

	/// The implementation for this method invokes the member function pointer on the target object using the provided arguments.
	virtual void Handle( arg... arguments );
	
	bool PointsTo( type* object, MemberPointer member );

private:
	type* _object;
	MemberPointer _member;
};

/// Provides an interface for implementing event-based behaviour.
/// Notify() is protected (exposed only in the Event sub-class), for use-cases where event broadcasting must be strongly encapsulated.
template <class ...arg> class EventInterface
{
public:
	/// Defines a pointer to a function equipped to handle an event from this provider.
	typedef void( *FunctionPointer )(arg ... arguments);

	EventInterface();
	virtual ~EventInterface();

	// Subscribe / Unsubscribe a pointer to a global function or a lambda expression.
	void Subscribe( FunctionPointer function );
	void Unsubscribe( FunctionPointer function );

	template<typename type> void Subscribe( type* object, void (type::*member)(arg ... arguments) );
	template<typename type> void Unsubscribe( type* object, void (type::*member)(arg ... arguments) );

	void Notify( arg... arguments );

protected:
	size_t _position;

	vector<FunctionPointer> _pointers;
	vector<AbstractEventHandler<arg...>*> _handlers;
};

/// Exposes the Notify method through an overloaded () operator.
template <class ...arg> class Event
{
	typedef void( *FunctionPointer )(arg ... arguments);
public:
	Event();
	virtual ~Event();

	void Subscribe( FunctionPointer function ) const;
	void Unsubscribe( FunctionPointer function ) const;

	template<typename type> void Subscribe( type* object, void (type::*member)(arg ... arguments) ) const;
	template<typename type> void Unsubscribe( type* object, void (type::*member)(arg ... arguments) ) const;

	void operator () ( arg ... arguments ) const;

private:
	EventInterface<arg...> * _interface;
};

// EventInterface implementation

template<class ...arg>
EventInterface<arg...>::EventInterface()
{}

template<class ...arg>
EventInterface<arg...>::~EventInterface()
{}

template<class ...arg>
inline void EventInterface<arg...>::Notify( arg... arguments )
{
	// iterate over all of the non-member function pointers
	for ( _position = 0; _position < _pointers.size(); _position++ )
	{
		_pointers[_position]( arguments ... );
	}

	// iterate over all of the member function pointers
	for ( _position = 0; _position < _handlers.size(); _position++)
	{
		_handlers[_position]->Handle( arguments ... );
	}
}

template<class ...arg>
inline void EventInterface<arg...>::Subscribe( FunctionPointer handler )
{
	_pointers.push_back( handler );
}

template<class ...arg>
inline void EventInterface<arg...>::Unsubscribe( FunctionPointer handler )
{
	auto iremove = remove( _pointers.begin(), _pointers.end(), handler );

	_position = std::min<size_t>( _position, iremove - _handlers.begin() );
	
	_pointers.erase( iremove, _pointers.end() );
}

/// Unsubscribe an EventHandler representing a member function pointer of a specific object instance.
template<class ...arg>
template<typename type>
inline void EventInterface<arg...>::Unsubscribe( type* object, void (type::*member)(arg ... arguments) )
{
	auto iremove = remove_if
	(
		_handlers.begin(),
		_handlers.end(),
		[=] ( AbstractEventHandler<arg...>* aeh ) -> bool
		{
			EventHandler<type, arg...>* eh = dynamic_cast<EventHandler<type, arg...>*>(aeh);

			if ( eh )
			{
				if ( eh->PointsTo( object, member ) )
				{
					delete eh;
					return true;
				}
			}

			return false;
		}
	);

	_position = std::min<size_t>( _position, iremove - _handlers.begin() );

	_handlers.erase( iremove, _handlers.end() );
}

/// Subscribe to receive events via a member function pointer of a specific object instance.
/// The EventHandler returned by this method acts as a key to be used when Unsubscribing.
template<class ...arg>
template<typename type>
inline void EventInterface<arg...>::Subscribe( type * object, void(type::* handler)(arg...arguments) )
{
	// create and store an event handler to hold the pointer to the object, and to its member function
	_handlers.push_back( new EventHandler<type, arg...>( object, handler ) );
}

// Event implementation

template<class ...arg>
Event<arg...>::Event()
{
	_interface = new EventInterface<arg...>();
}

template<class ...arg>
Event<arg...>::~Event()
{}

template<class ...arg>
inline void Event<arg...>::Subscribe( FunctionPointer function ) const
{
	_interface->Subscribe( function );
}

template<class ...arg>
inline void Event<arg...>::Unsubscribe( FunctionPointer function ) const
{
	_interface->Unsubscribe( function );
}

template<class ...arg>
inline void Event<arg...>::operator()( arg ...arguments ) const
{
	_interface->Notify( arguments ... );
}

// EventHandler implementation

template<class ...arg>
inline AbstractEventHandler<arg...>::AbstractEventHandler()
{}

template<class ...arg>
inline AbstractEventHandler<arg...>::~AbstractEventHandler()
{}

// EventHandlerMember implementation

template<class type, class ...arg>
inline EventHandler<type, arg...>::EventHandler( type * object, MemberPointer function ) : _object( object ), _member( function )
{}

template<class type, class ...arg>
inline EventHandler<type, arg...>::~EventHandler()
{}

template<class type, class ...arg>
inline void EventHandler<type, arg...>::Handle( arg ... arguments )
{
	// invoke the appropriate member function on the target object instance
	(_object->*_member)(arguments ...);
}

template<class type, class ...arg>
inline bool EventHandler<type, arg...>::PointsTo( type * object, MemberPointer member )
{
	return object == _object && member == _member;
}

template<class ...arg>
template<typename type>
inline void Event<arg...>::Subscribe( type * object, void(type::* member)(arg...arguments) ) const
{
	_interface->Subscribe( object, member );
}

template<class ...arg>
template<typename type>
inline void Event<arg...>::Unsubscribe( type * object, void(type::* member)(arg...arguments) ) const
{
	_interface->Unsubscribe( object, member );
}
