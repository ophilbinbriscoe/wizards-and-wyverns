#include "Chest.h"

Chest::Chest() : MapObject ("Chest")
{}

Chest::Chest( Chest const * const chest ) : MapObject( chest->GetName() )
{
	ItemContainer::operator=( *chest );
}

Chest::~Chest()
{}

void Chest::Accept( AbstractInteractor * interactor )
{
	interactor->Interact( this );
}

void Chest::Accept( AbstractInspector * inspector ) const
{
	inspector->Inspect( this );
}
