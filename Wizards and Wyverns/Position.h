#pragma once

#include <iostream>

using namespace std;

/// Struct modelling a 2D integer coordinate.
/** Overloads assignment, comparison, and +/- arithmetic operators. */
struct Position
{
public:
	/// Directional constant.
	static const Position RIGHT, LEFT, UP, DOWN, ZERO;

	Position();
	Position( const int& x, const int& y );
	Position( const Position & position );
	~Position();

	void operator= ( const Position& position );
	void operator+= ( const Position& position );
	void operator-= ( const Position& position );

	int x;
	int y;
};

inline ostream& operator<< ( ostream& stream, const Position& position )
{
	return stream << '(' << position.x << ", " << position.y << ')';
}

inline void Position::operator=( const Position & position )
{
	x = position.x;
	y = position.y;
}

inline bool operator==( const Position & lhs, const Position & rhs )
{
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline bool operator!=( const Position & lhs, const Position & rhs )
{
	return !operator==( lhs, rhs );
}

inline Position operator+( const Position & lhs, const Position & rhs )
{
	return Position( lhs.x + rhs.x, lhs.y + rhs.y );
}

inline Position operator-( const Position & lhs, const Position & rhs )
{
	return Position( lhs.x - rhs.x, lhs.y - rhs.y );
}

inline void Position::operator+=( const Position & position )
{
	x += position.x;
	y += position.y;
}

inline void Position::operator-=( const Position & position )
{
	x -= position.x;
	y -= position.y;
}

