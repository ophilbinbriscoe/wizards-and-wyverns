#include "NameWriter.h"

#pragma warning(disable:4996)

NameWriter::NameWriter( fstream& file, const char * filepath ) : Writer( file, filepath )
{}

NameWriter::~NameWriter()
{}

void NameWriter::Write( fstream& file, const Resource * item )
{
	file <<  item->GetName().data() << '\n';
}
