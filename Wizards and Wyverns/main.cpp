#include <iostream>

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace std;

#include "Database.h"
#include "ApplicationController.h"

//#define TEST

/// Application entry point
int main()
{
	// set the seed for the pseudo-random number genetator
	srand((unsigned int)time(NULL));

#ifdef TEST

	// run your tests here!

	CPPUNIT_NS::TextUi::TestRunner runner;   //the runner
											 // Get the top level suite from the registry
	CPPUNIT_NS::Test* suite =
		CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest();
	// Adds the test to the list of test to run
	runner.addTest(suite);
	// Run the test.
	bool wasSucessful = runner.run();

#else

	// make sure that StatType flyweights are initialized first
	Stats();

	// followed by SlotType flyweights
	Slots();

	// read serialized data
	Data().ReadAll();

	// create the ApplicationController (main loop)
	ApplicationController* controller = new ApplicationController();

	// serialize data
	Data().SaveAll();
#endif

	// wait for user input before terminating
	cin.ignore();

	return 0;
};