#include "MaximizeBonusStrategy.h"



MaximizeBonusStrategy::MaximizeBonusStrategy()
{}


MaximizeBonusStrategy::~MaximizeBonusStrategy()
{}

void MaximizeBonusStrategy::RequestDecision( Character const * const character, Map const * const map )
{
	MaximizeBonusFor( character, Slots().Head );
	MaximizeBonusFor( character, Slots().Body );
	MaximizeBonusFor( character, Slots().WeaponHand );
	MaximizeBonusFor( character, Slots().ShieldHand );
	MaximizeBonusFor( character, Slots().Waist );
	MaximizeBonusFor( character, Slots().Feet );
	MaximizeBonusFor( character, Slots().RingFinger );
}

void MaximizeBonusStrategy::MaximizeBonusFor( Character const * character, SlotType const * type )
{
	_max = 0;
	_type = type;

	// query the currently equipped Equipment
	auto equipped = character->GetSlot( type ).GetEquipped();

	// if something is actually equipped
	if ( equipped )
	{
		// set the initial maximum to the currently equipped maximum
		CheckIfMaxBonus( equipped );
	}

	_decision = nullptr;

	// iterate over the Equipment in the Characters' backpack
	for ( auto pair : character->GetBackpack()->GetItems() )
	{
		pair.first->Accept( this );
	}

	// if a better piece of Equipment was found
	if ( _decision )
	{
		// request that it be equipped
		OnDecide( this, character );
	}
}

bool MaximizeBonusStrategy::CheckIfMaxBonus( Equipment const * equipment )
{
	unsigned int accumulator = 0;

	// iterate over the Equipment's Enchantments
	for ( auto enchantment : equipment->GetEnchantments() )
	{
		// sum Enchantment bonuses
		accumulator += enchantment.bonus;
	}

	// check if the Equipment's Enchantment bonuses add up to more than the current maximum
	if ( accumulator > _max )
	{
		// update the current maximum
		_max = accumulator;

		// indicate that a new maximum has been found
		return true;
	}

	// indicate that no new maximum has been found
	return false;
}

void MaximizeBonusStrategy::Use( Item const * item )
{
	// do nothing
}

void MaximizeBonusStrategy::Use( Equipment const * equipment )
{
	// if the Equipment can be equipped to the slot under consideration
	if ( equipment->slot == _type )
	{
		// check if it provides a greater cumulative Enchantment bonus than the currently equipped Equipment
		if ( CheckIfMaxBonus( equipment ) )
		{
			_decision = equipment;
		}
	}
	
}
