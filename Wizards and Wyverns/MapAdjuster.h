#pragma once

#include "CharacterProxy.h"
#include "Chest.h"
#include "Map.h"

class MapAdjuster : private AbstractInspector
{
public:
	MapAdjuster();
	virtual ~MapAdjuster();

	Event<CharacterProxy * const> OnSpawnProxy;
	Event<Character * const> OnSpawnCharacter;
	Event<Chest * const> OnSpawnChest;

	Map * Adjust( Map const * map );

protected:
	virtual void Adjust( CharacterProxy const * proxy, const Position & position, Map * adjusted ) = 0;
	virtual void Adjust( Chest const * chest, const Position & position, Map * adjusted ) = 0;

private:
	Map * _adjusted;
	Position _position;

	virtual void Inspect( CharacterProxy const * proxy );
	virtual void Inspect( Chest const * chest );
};

