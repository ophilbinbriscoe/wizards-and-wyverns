#include "Command.h"


Command::Command()
{}


Command::~Command()
{}

UndoableCommand::UndoableCommand()
{}

UndoableCommand::~UndoableCommand()
{}

CommandManager::CommandManager()
{}

CommandManager::~CommandManager()
{}

void CommandManager::Execute( Command& command )
{
	ClearStack( _redoStack );
	command.Execute();
}

void CommandManager::Execute( std::shared_ptr<UndoableCommand> command )
{
	ClearStack( _redoStack );
	command->Execute();
	_undoStack.push( command );
}

void CommandManager::Undo()
{
	_undoStack.top()->Undo();
	_redoStack.push( _undoStack.top() );
	_undoStack.pop();
}

void CommandManager::Redo()
{
	_redoStack.top()->Execute();
	_undoStack.push( _redoStack.top() );
	_redoStack.pop();
}

int CommandManager::UndoStackSize() const
{
	return _undoStack.size();
}

int CommandManager::RedoStackSize() const
{
	return _redoStack.size();
}

void CommandManager::ClearStack( std::stack<std::shared_ptr<UndoableCommand>> & stack )
{
	while ( !stack.empty() )
	{
		delete &(stack.top());
		stack.pop();
	}
}
