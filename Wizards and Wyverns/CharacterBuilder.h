#pragma once

#include "Character.h"

class CharacterBuilder
{
public:
	CharacterBuilder( const bool & playable, const int & level, Class const * const type, Alignment const * const alignment, const string& name, const bool & roll = false );
	~CharacterBuilder();

	void SetName( const string& name );

	void SetHitPoints( const int& maximum, const int& damage );
	void RollHitPoints();
	void CopyHitPointsFrom( Character const * const character );

	void SetScore( StatType const * type, const unsigned int& score );
	void RollScore( StatType const * type );
	void RollScores();
	void CopyScoresFrom( Character const * const character );

	void AddWorn( Equipment const * equipment );
	void CopyWornFrom( Character const * const character );
	void CopyBackpackFrom( Character const * const character );
	ItemContainer & AccessBackpack();

	Character * Create();

private:
	bool _playable;
	int _level;
	Class const * _type;
	Alignment const * _alignment;
	string _name;

	map<StatType const * const, Score> _scores;

	int _max;
	int _damage;

	map<SlotType const * const, Slot> _worn;
	ItemContainer _backpack;
};

