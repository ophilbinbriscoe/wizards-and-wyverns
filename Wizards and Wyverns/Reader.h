#pragma once

#include <tuple>
#include <stdio.h>
#include <cstring>

#pragma warning(disable:4996)

/// Template base class for a Reader
template <class type>
class Reader
{
public:
	Reader( const char* filename );
	~Reader();

	/// Read the next value
	virtual bool Read() = 0;

	/// Query the result
	type GetResult() const;

protected:
	type _result;
	FILE* _file;
};

template <class type>
Reader<type>::Reader( const char* filename )
{
	// open file stream
	_file = fopen( filename, "r" );
}

template <class type>
Reader<type>::~Reader()
{
	// check if file stream is still open (EOF not reached)
	if ( _file )
	{
		// close file stream
		fclose( _file );
	}
}

template <class type>
type Reader<type>::GetResult() const
{
	return _result;
}

