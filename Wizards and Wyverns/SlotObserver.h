#pragma once

#include "View.h"
#include "Observer.h"
#include "Slot.h"
#include "ItemObserver.h"

namespace view
{
	class SlotObserver : public View, public Observer<Slot>
	{
	public:
		SlotObserver( shared_ptr<sfg::Box> container, const bool & interactive = false );
		~SlotObserver();

		Event<SlotType const *> OnClickRemove;
		Event<SlotType const *> OnClickEquip;

		void SetInteractive( const bool& interactive );
		const bool & IsInteractive() const { return _interactive; }

	protected:
		bool _interactive;

		void StartObserving();
		void StopObserving();

		void HandleEquip( Equipment const * const equipment );
		void HandleUnequip( Equipment const * const equipment );
		void HandleClick();

		void SetButton();
		void SetButtonLabel();

		shared_ptr<sfg::Box> _subcontainer;
		shared_ptr<sfg::Frame> _frame;
		shared_ptr<sfg::Button> _button;
		ItemObserver* _observer;
	};
}

