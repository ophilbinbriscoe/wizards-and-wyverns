#pragma once

#include "View.h"

/// This class is essentially a workaround for an SFGUI bug.
/// The bug seems to causes the target texture from the first canvas used anywhere in the application to be drawn on all subsequently created canvases.

shared_ptr<sfg::Canvas> GetSharedCanvas();

