//! @file 
//! @brief Implementation file for the WritersTestSuite class
//!

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>

#include "Writer.h"
#include "CampaignWriter.h"
#include "MapWriter.h"
#include "CharacterWriter.h"
#include "ItemWriter.h"

#include "Campaign.h"
#include "Map.h"
#include "Character.h"
#include "CharacterBuilder.h"
#include "Item.h"

using namespace CppUnit;

//! Test Class for the Writer class
class WritersTestSuite : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(WritersTestSuite);
	CPPUNIT_TEST(CampaignWrite);
	CPPUNIT_TEST(MapWrite);
	CPPUNIT_TEST(CharacterWrite);
	CPPUNIT_TEST(ItemWrite);
	CPPUNIT_TEST_SUITE_END();
protected:
	void CampaignWrite();
	void MapWrite();
	void CharacterWrite();
	void ItemWrite();
};

//! cppunit test cases registration
CPPUNIT_TEST_SUITE_REGISTRATION(WritersTestSuite);

//! Tests writing a campaign
//! Test Case: A written campaign should show up in an existing file and its name should be found inside it
//! Tested item: CampaignWriter::Write()
void WritersTestSuite::CampaignWrite()
{
	string info;
	Map * map = new Map(5, 5, "TestMap");
	CampaignNode * cn = new CampaignNode(map);
	Campaign * cm = new Campaign("TestCampaign", cn);
	Dataset<Map*> mapdata;
	fstream file;
	mapdata.Add(map);
	CampaignWriter cw(file, "tests/campaignTest.dat", mapdata);
	
	cw.Write(cm);
	file.close();
	
	ifstream in;
	in.open("tests/campaignTest.dat");
	if (!in.is_open())
		CPPUNIT_ASSERT(false);
	getline(in, info);
	in.close();
	CPPUNIT_ASSERT(info.find(cm->GetName()));
}
//! Tests writing a Map
//! Test Case: A written Map should show up in an existing file and its name should be found inside it
//! Tested item: MapWriter::Write()
void WritersTestSuite::MapWrite()
{
	string info;
	Map * map = new Map(5, 5, "TestMap");
	Character * testchar;
	CharacterBuilder builder(true, 6, Class::Fighter, Alignment::Good, "Test", true);
	testchar = builder.Create();
	fstream file;
	Dataset<Character *> chardata;
	chardata.Add(testchar);
	Dataset<Item *> itemdata;
	Item * item = new Item("TestItem");
	ItemContainerWriter * icw = new ItemContainerWriter(itemdata);
	MapWriter mw(file, "tests/mapsTest.dat", chardata);
	
	mw.Write(map, icw);
	file.close();

	ifstream in;
	in.open("tests/mapsTest.dat");
	if (!in.is_open())
		CPPUNIT_ASSERT(false);
	getline(in, info);
	in.close();
	CPPUNIT_ASSERT(info.find(map->GetName()));
}
//! Tests writing a Character
//! Test Case: A written Character should show up in an existing file and its name should be found inside it
//! Tested item: CharacterWriter::Write()
void WritersTestSuite::CharacterWrite()
{
	string info;
	fstream file;
	Dataset<Item *> itemdata;
	Item * item = new Item("TestItem");
	itemdata.Add(item);
	Character * testchar;
	CharacterBuilder builder(true, 6, Class::Fighter, Alignment::Good, "TestChar", true);
	testchar = builder.Create();
	ItemContainerWriter * icw = new ItemContainerWriter(itemdata);
	CharacterWriter pw(file, "tests/charactersTest.dat");
	
	pw.Write("char", testchar, icw);
	file.close();

	ifstream in;
	in.open("tests/charactersTest.dat");
	if (!in.is_open())
		CPPUNIT_ASSERT(false);
	getline(in, info);
	in.close();
	CPPUNIT_ASSERT(info.find(testchar->GetName()));
}

//! Tests writing an Item
//! Test Case: A written Item should show up in an existing file and its name should be found inside it
//! Tested item: ItemWriter::Write()
void WritersTestSuite::ItemWrite()
{
	string info;
	fstream file;
	ItemWriter er(file, "tests/itemsTest.dat");
	Item * item = new Item("TestItem");
	
	er.Write(item);
	file.close();

	ifstream in;
	in.open("tests/itemsTest.dat");
	if (!in.is_open())
		CPPUNIT_ASSERT(false);
	getline(in, info);
	in.close();
	CPPUNIT_ASSERT(info.find(item->GetName()));
}

