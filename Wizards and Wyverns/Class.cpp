#include "Class.h"

void CalculateFighterBaseAttackBonus( vector<int>& vector, const int & level )
{
	vector.resize( 1 + (int) ((level - 1) / 5) );

	vector[0] = level;

	for ( int i = 5; i < level; i += 5 )
	{
		vector[i / 5] = level - i;
	}
};

Class const * const Class::Fighter = new Class( "Fighter", 0, "d10", &CalculateFighterBaseAttackBonus );

void Class::CalculateBaseAttackBonus( vector<int>& vector, const int & level ) const
{
	_baseAttackBonusCalculator( vector, level );
}

Class::Class( const string& name, const int& enumerator, const string& hitDie, FunctionPointer baseAttackBonusCalculator ) : DomainConcept( name, "", enumerator ), hitDie( hitDie ), _baseAttackBonusCalculator( baseAttackBonusCalculator )
{}

Class::~Class()
{}