#include "SlotType.h"

SlotType::SlotType( const string& name, const StatTypes& supported, const unsigned int enumerator ) : DomainConcept( name, "", enumerator ), _supported( supported )
{}

bool SlotType::SupportsEnchantmentOf( StatType const * const statType ) const
{
	return find( _supported.begin(), _supported.end(), statType ) != _supported.end();
}

SlotType::~SlotType()
{}

SlotType::SlotTypeData::SlotTypeData() :
Head(		new SlotType( "Head",		StatTypes{ Stats().ArmorClass, Stats().Wisdom, Stats().Intelligence }, 21 ) ),
Body(		new SlotType( "Body",		StatTypes{ Stats().ArmorClass }, 22 ) ),
WeaponHand( new SlotType( "Main Hand",	StatTypes{ Stats().AttackBonus, Stats().DamageBonus }, 23 ) ),
ShieldHand( new SlotType( "Off Hand",	StatTypes{ Stats().ArmorClass }, 24 ) ),
Waist(		new SlotType( "Waist",		StatTypes{ Stats().Constitution, Stats().Strength }, 25 ) ),
Feet(		new SlotType( "Feet",		StatTypes{ Stats().ArmorClass, Stats().Dexterity }, 26 ) ),
RingFinger( new SlotType( "Ring Finger",StatTypes{ Stats().ArmorClass, Stats().Strength, Stats().Constitution, Stats().Wisdom, Stats().Charisma }, 27 ) )
{}

SlotType::SlotTypeData::~SlotTypeData()
{}

const SlotType::SlotTypeData & Slots()
{
	static SlotType::SlotTypeData* data = new SlotType::SlotTypeData();
	return *data;
}
