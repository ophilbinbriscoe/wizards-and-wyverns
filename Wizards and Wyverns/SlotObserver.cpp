#include "SlotObserver.h"

namespace view
{
	SlotObserver::SlotObserver( shared_ptr<sfg::Box> container, const bool & interactive ) : View( container ), _interactive( interactive )
	{
		_frame = sfg::Frame::Create( "" );

		auto box = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 5.0f );

		auto right = sfg::Alignment::Create();

		right->SetScale( { 0.0f, 1.0f } );
		right->SetAlignment( { 1.0f, 0.0f } );

		_subcontainer = sfg::Box::Create( sfg::Box::Orientation::VERTICAL );

		_observer = new ItemObserver( _subcontainer );

		_button = sfg::Button::Create( "" );

		_button->GetSignal( sfg::Button::OnLeftClick ).Connect( std::bind( &SlotObserver::HandleClick, this ) );

		_button->SetRequisition( { 100.0f, 0.0 } );

		right->Add( _button );

		box->Pack( _subcontainer, false, false );

		box->Pack( right, true, true );

		_frame->Add( box );

		_container->Pack( _frame, false, false );
	}

	SlotObserver::~SlotObserver()
	{}

	void SlotObserver::SetInteractive( const bool & interactive )
	{
		if ( interactive != _interactive )
		{
			_interactive = interactive;

			SetButton();
		}
	}

	void SlotObserver::StartObserving()
	{
		_frame->SetLabel( _target->type->name );

		_target->OnEquip.Subscribe( this, &SlotObserver::HandleEquip );
		_target->OnUnequip.Subscribe( this, &SlotObserver::HandleUnequip );

		if ( _target->IsEmpty() )
		{
			_observer->Observe( nullptr );
		}
		else
		{
			_observer->Observe( _target->GetEquipped() );
		}

		_button->Show( _interactive );
		
		SetButtonLabel();
	}

	void SlotObserver::StopObserving()
	{
		_target->OnEquip.Unsubscribe( this, &SlotObserver::HandleEquip );
		_target->OnUnequip.Unsubscribe( this, &SlotObserver::HandleUnequip );

		_observer->Observe( nullptr );

		_button->Show( false );
	}

	void SlotObserver::HandleEquip( Equipment const * const equipment )
	{
		_observer->Observe( equipment );

		SetButtonLabel();
	}

	void SlotObserver::HandleUnequip( Equipment const * const equipment )
	{
		_observer->Observe( nullptr );

		SetButtonLabel();
	}

	void SlotObserver::HandleClick()
	{
		if ( _target->IsEmpty() )
		{
			OnClickEquip( _target->type );
		}
		else
		{
			OnClickRemove( _target->type );
		}
	}

	void SlotObserver::SetButton()
	{
		_button->Show( _interactive && _target );
	}

	void SlotObserver::SetButtonLabel()
	{
		_button->SetLabel( _target->IsEmpty() ? "Equip" : "Unequip" );
	}
}