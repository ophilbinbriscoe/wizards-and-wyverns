#pragma once

#include "Stat.h"

class DerivedStat : public Stat<DerivedStat>
{
public:
	DerivedStat( const StatType* type, const int& score );
	~DerivedStat();

	virtual const int Min() const { return 0; }
	virtual const int Max() const { return INT32_MAX; }

	void operator = ( const DerivedStat& stat );
	void operator = ( const int& value );
	void operator+= ( const int& value );
	void operator-= ( const int& value );
};

inline void DerivedStat::operator=( const DerivedStat & stat )
{
	Value::operator=( stat._value );
}

inline void DerivedStat::operator= ( const int& value )
{
	Value::operator=( value );
}

inline void DerivedStat::operator+= ( const int& value )
{
	Value::operator+=( value );
}

inline void DerivedStat::operator-= ( const int& value )
{
	Value::operator-=( value );
}