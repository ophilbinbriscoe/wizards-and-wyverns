#include "StatFormatter.h"

string BonusString( const int& bonus )
{
	// prepend a '+' to positive-valued bonuses
	return (bonus >= 0 ? psign : estrg) + to_string( bonus );
}