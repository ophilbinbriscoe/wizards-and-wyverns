#include <stdlib.h>

#include "ApplicationController.h"

#include "MainMenuController.h"

ApplicationController::ApplicationController() : _window( new view::Window( APPLICATION_WINDOW_WIDTHi, APPLICATION_WINDOW_HEIGHTi, "Wizards and Wyverns" ) )
{
	Controller * main = new MainMenuController();

	main->Pop.Subscribe( this, &ApplicationController::EnqueuePop );

	Push( main );

	_window->Add( main->GetWindow() );

	_window->Repaint();

	while ( !_window->HasClosed() )
	{
		_window->PollEvents();

		if ( _stack.size() > 0 )
		{
			_stack.top()->Update( _window->GetMillisecondsSinceLastRepaint() );
		}

		if ( _window->GetElapsedMilliseconds() > 5 )
		{
			_window->RequestRepaint();
		}

		_window->RepaintIfRequested();

		if ( _collapse )
		{
			Collapse();
		}

		while ( _stack.top()->popped )
		{
			if ( _stack.size() > 1 )
			{
				Pop( _stack.top() );
			}
			else
			{
				_window->Close();

				break;
			}
		}

		while ( _queue.size() > 0 )
		{
			auto tuple = _queue.front();

			// if this is a Push request
			if ( get<1>( tuple ) )
			{
				Push( get<0>( tuple ) );
			}

			_queue.pop();
		}
	}

	delete main;
}

ApplicationController::~ApplicationController()
{}

void ApplicationController::HandleCollapse()
{
	_collapse = true;
}

void ApplicationController::EnqueuePush( Controller *controller )
{
	controller->Pop.Subscribe( this, &ApplicationController::EnqueuePop );
	controller->Collapse.Subscribe( this, &ApplicationController::HandleCollapse );

	_queue.push( tuple<Controller *, bool>( controller, true ) );
}

void ApplicationController::EnqueuePop( Controller * controller )
{
	_queue.push( tuple<Controller *, bool>( controller, false ) );

	controller->popped = true;
}

void ApplicationController::Collapse()
{
	while ( _stack.size() > 1 )
	{
		Pop( _stack.top() );
	}

	while ( _queue.size() > 0 )
	{
		_queue.pop();
	}

	_collapse = false;
}

void ApplicationController::Push( Controller * controller )
{
	if ( _stack.size() > 0 )
	{
		_stack.top()->GetWindow()->Show( false );
		_stack.top()->UnsubscribeInputHandlers( _window );
	}

	_stack.push( controller );

	controller->Push.Subscribe( this, &ApplicationController::EnqueuePush );

	controller->SubscribeInputHandlers( _window );

	_window->Add( controller->GetWindow() );
}

void ApplicationController::Pop( Controller * controller )
{
	controller->Push.Unsubscribe( this, &ApplicationController::EnqueuePush );
	controller->Pop.Unsubscribe( this, &ApplicationController::EnqueuePop );
	controller->Collapse.Unsubscribe( this, &ApplicationController::HandleCollapse );

	controller->UnsubscribeInputHandlers( _window );

	auto window = controller->GetWindow();

	window->Show( false );

	_window->Remove( window );

	_stack.pop();

	delete controller;

	_stack.top()->GetWindow()->Show();
	_stack.top()->SubscribeInputHandlers( _window );
}