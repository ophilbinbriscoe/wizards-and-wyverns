#pragma once

#include "Value.h"
#include "StatType.h"

template <typename T>
class Stat : public Value<T>
{
public:
	Stat( const StatType* type, const int& score );
	~Stat();

	StatType const * const type;
};

template <typename T>
Stat<T>::Stat( const StatType* type, const int& score ) : Value( score ), type( type )
{}

template <typename T>
Stat<T>::~Stat()
{}