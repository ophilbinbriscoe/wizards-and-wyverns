#include "ItemContainerWriter.h"

#pragma warning(disable:4996)

ItemContainerWriter::ItemContainerWriter( const Dataset<Item*> & items )
{
	for ( unsigned int i = 0; i < items.GetCount(); i++ )
	{
		_order.insert( pair<Item *, unsigned int>( items.GetContents()[i], i ) );
	}
}

ItemContainerWriter::~ItemContainerWriter()
{}

void ItemContainerWriter::Write( fstream& file, ItemContainer const * container ) const
{
	file << container->GetItems().size();

	for ( auto pair : container->GetItems() )
	{
		file << ' ' << _order.at( pair.first ) << '#' << pair.second;
	}
}

unsigned int ItemContainerWriter::ItemOrder( Item const * item ) const
{
	return _order.at( item );
}