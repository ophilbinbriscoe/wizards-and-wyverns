#pragma once

#include "Position.h"

/// Templated class providing 2D-coordinate based access to a flat built-in array of values.
template <typename arg> class Grid
{
public:
	Grid( const Grid & grid );
	Grid( const int& width, const int& height );
	Grid( const int& width, const int& height, const arg& value );
	~Grid();

	void Set( const Position& position, const arg& value );
	arg & Get( const Position& position ) const;

	int GetWidth() const { return _width; }
	int GetHeight() const { return _height; }

	bool Contains( const Position& position ) const;

	void operator = ( const Grid & grid );

protected:
	int PositionToIndex( const Position& position ) const;
	Position& IndexToPosition( const int& index ) const;

	int _width;
	int _height;

private:
	int _size;
	arg * _cells;
};

template<typename arg>
inline Grid<arg>::Grid( const Grid & grid ) : _width( grid._width ), _height( grid._height ), _size( grid._size )
{
	_cells = new arg[_size];

	for ( int i = 0; i < _size; i++ )
	{
		_cells[i] = grid._cells[i];
	}
}

template <typename arg>
Grid<arg>::Grid( const int& width, const int& height ) : _width( width ), _height( height ), _size( width * height )
{
	_cells = new arg[_size];
}

template<typename arg>
inline Grid<arg>::Grid( const int & width, const int & height, const arg & value ) : Grid( width, height )
{
	for ( int i = 0; i < _size; i++ )
	{
		_cells[i] = value;
	}
}

template <typename arg>
Grid<arg>::~Grid()
{
	delete [] _cells;
}

template<typename arg>
void Grid<arg>::Set( const Position & position, const arg & value )
{
	_cells[PositionToIndex( position )] = value;
}

template<typename arg>
arg & Grid<arg>::Get( const Position & position ) const
{
	return _cells[PositionToIndex( position )];
}

template<typename arg>
int Grid<arg>::PositionToIndex( const Position & position ) const
{
	return position.y * _width + position.x;
}

template<typename arg>
Position & Grid<arg>::IndexToPosition( const int & index ) const
{
	return Position( (index / _height), index % _height );
}

template<typename arg>
bool Grid<arg>::Contains( const Position & position ) const
{
	return position.x >= 0 && position.y >= 0 && position.x < _width && position.y < _height;
}

template<typename arg>
inline void Grid<arg>::operator=( const Grid & grid )
{
	delete[] _cells;

	_width = grid._width;
	_height = grid._height;
	_size = grid._size;

	_cells = new arg[_size];

	for ( int i = 0; i < _size; i++ )
	{
		_cells[i] = grid._cells[i];
	}
}
