#include "AbstractStrategyProvider.h"

AbstractStrategyProvider::AbstractStrategyProvider( StrategyProvider const * user, StrategyProvider const * npc ) : _user( user ), _npc( npc )
{}

AbstractStrategyProvider::~AbstractStrategyProvider()
{}

BarrierStrategy<Position>* AbstractStrategyProvider::GetMovementStrategy( Character const * character ) const
{
	if ( character->IsPlayable() )
	{
		return _user->GetMovementStrategy( character );
	}

	return _npc->GetMovementStrategy( character );
}

BarrierStrategy<MapObject const*>* AbstractStrategyProvider::GetInteractionStrategy( Character const * character ) const
{
	if ( character->IsPlayable() )
	{
		return _user->GetInteractionStrategy( character );
	}

	return _npc->GetInteractionStrategy( character );
}

FreeStrategy<Item const*>* AbstractStrategyProvider::GetEquipStrategy( Character const * character ) const
{
	if ( character->IsPlayable() )
	{
		return _user->GetEquipStrategy( character );
	}

	return _npc->GetEquipStrategy( character );
}

FreeStrategy<SlotType const*>* AbstractStrategyProvider::GetUnequipStrategy( Character const * character ) const
{
	if ( character->IsPlayable() )
	{
		return _user->GetUnequipStrategy( character );
	}

	return _npc->GetUnequipStrategy( character );
}

FreeStrategy<StatType const*>* AbstractStrategyProvider::GetLevelStrategy( Character const * character ) const
{
	if ( character->IsPlayable() )
	{
		return _user->GetLevelStrategy( character );
	}

	return _npc->GetLevelStrategy( character );
}
