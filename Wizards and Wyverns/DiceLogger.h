#pragma once

#include "Observer.h"
#include "Dice.h"
#include "Logger.h"

#include <iostream>

using namespace std;

class DiceLogger : public Logger, public Observer<Dice>
{
public:
	DiceLogger();

	virtual void StartObserving();
	virtual void StopObserving();

	void OnRollEventHandler(const string&, const unsigned int&);

	~DiceLogger();
};

