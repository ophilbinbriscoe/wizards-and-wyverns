#include "Weapon.h"

Weapon::Weapon( const string& name, WeaponType const * type, const Enchantments& enchantments ) : Equipment( name, Slots().WeaponHand, enchantments ), type( type )
{}

Weapon::~Weapon()
{}
