#pragma once

#include "MenuController.h"

class MessageController : public MenuController
{
public:
	MessageController( const string& message, const string& label, const bool& collapse = false );
	virtual ~MessageController();

protected:
	bool _collapse;

	void HandleClick();

	void HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke );

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );
};

