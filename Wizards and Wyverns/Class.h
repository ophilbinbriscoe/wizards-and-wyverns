#pragma once

#include <vector>

#include "DomainConcept.h"

using namespace std;

class Class : public DomainConcept
{
	typedef void( *FunctionPointer )(vector<int>&, const int&);
public:
	static Class const * const Fighter;

	const string hitDie;

	virtual void CalculateBaseAttackBonus( vector<int>& vector, const int& level ) const;

private:
	Class( const string& name, const int& enumerator, const string& hitDie, FunctionPointer baseAttackBonusCalculator );
	~Class();

	FunctionPointer _baseAttackBonusCalculator;
};