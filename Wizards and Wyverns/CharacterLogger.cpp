#include "CharacterLogger.h"

CharacterLogger::CharacterLogger()
{}

CharacterLogger::~CharacterLogger()
{}

void CharacterLogger::HandleLootedChest( Character const * const actor, Chest const * const target )
{
	if ( enabled )
	{
		cout << "(char) " << actor->GetName() << " loots " << target->GetName() << endl;
	}
}

void CharacterLogger::HandleLootedCorpse( Character const * const actor, Character const * const target )
{
	if ( enabled )
	{
		cout << "(char) " << actor->GetName() << " loots " << target->GetName() << "'s corpse" << endl;
	}
}

void CharacterLogger::HandleAttack( Character const * const actor, Character const * const target, const bool & hit, const int & attackRoll, const int& attackBonus, const int & damageRoll )
{
	if ( enabled )
	{
		string attempt;

		if ( attackRoll == 1 )
		{
			attempt = "fumbles, ";
		}
		else if ( attackRoll == 20 )
		{
			attempt = "critical! ";
		}
		else
		{
			attempt = string( "(" ) + to_string( attackRoll ) + string( " + " ) + to_string( attackBonus );

			if ( hit )
			{
				attempt += string( " >= " ) + to_string( target->GetStat( Stats().ArmorClass ) );
			}
			else
			{
				attempt += string( " < " ) + to_string( target->GetStat( Stats().ArmorClass ) );
			}

			attempt += string( "), " );
		}

		string outcome;

		if ( hit )
		{
			outcome = string( "hits for " ) + to_string( damageRoll ) + string( " damage" );
		}
		else
		{
			outcome = string( "deals no damage" );
		}

		cout << "(char) " << actor->GetName() << " attacks " << target->GetName() << ", " << attempt << outcome << endl;
	}
}

void CharacterLogger::HandleDeath( Character const * const character )
{
	if ( enabled )
	{
		cout << "(char) " << character->GetName() << " has died" << endl;
	}
}

void CharacterLogger::StartObserving()
{
	_target->OnAttack.Subscribe( this, &CharacterLogger::HandleAttack );
	_target->OnLootChest.Subscribe( this, &CharacterLogger::HandleLootedChest );
	_target->OnLootCorpse.Subscribe( this, &CharacterLogger::HandleLootedCorpse );
	_target->OnDie.Subscribe( this, &CharacterLogger::HandleDeath );
}

void CharacterLogger::StopObserving()
{
	_target->OnAttack.Unsubscribe( this, &CharacterLogger::HandleAttack );
	_target->OnLootChest.Unsubscribe( this, &CharacterLogger::HandleLootedChest );
	_target->OnLootCorpse.Unsubscribe( this, &CharacterLogger::HandleLootedCorpse );
	_target->OnDie.Unsubscribe( this, &CharacterLogger::HandleDeath );
}
