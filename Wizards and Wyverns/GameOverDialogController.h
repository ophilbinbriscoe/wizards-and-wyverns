#pragma once

#include "MenuController.h"

class GameOverDialogController : public MenuController
{
public:
	GameOverDialogController();
	virtual ~GameOverDialogController();
	
	Event<> OnRetry;

protected:
	void HandleClickRetry();
	void HandleClickQuit();
};

