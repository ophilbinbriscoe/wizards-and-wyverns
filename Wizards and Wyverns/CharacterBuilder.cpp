#include "CharacterBuilder.h"

#include "Dice.h"

CharacterBuilder::CharacterBuilder( const bool & playable, const int & level, Class const * const type, Alignment const * const alignment, const string& name, const bool & roll ) : _playable( playable ), _level( level ), _type( type ), _alignment( alignment ), _name( name )
{
	_worn.insert( pair<SlotType const * const, Slot>( Slots().Head, Slot( Slots().Head ) ) );
	_worn.insert( pair<SlotType const * const, Slot>( Slots().Body, Slot( Slots().Body ) ) );
	_worn.insert( pair<SlotType const * const, Slot>( Slots().WeaponHand, Slot( Slots().WeaponHand ) ) );
	_worn.insert( pair<SlotType const * const, Slot>( Slots().ShieldHand, Slot( Slots().ShieldHand ) ) );
	_worn.insert( pair<SlotType const * const, Slot>( Slots().Feet, Slot( Slots().Feet ) ) );
	_worn.insert( pair<SlotType const * const, Slot>( Slots().Waist, Slot( Slots().Waist ) ) );
	_worn.insert( pair<SlotType const * const, Slot>( Slots().RingFinger, Slot( Slots().RingFinger ) ) );

	if ( roll )
	{
		// roll ability scores
		RollScores();

		// roll hit points
		RollHitPoints();
	}
}

CharacterBuilder::~CharacterBuilder()
{}

void CharacterBuilder::SetName( const string & name )
{
	_name = name;
}

void CharacterBuilder::SetHitPoints( const int & maximum, const int & damage )
{
	_max = maximum;
	_damage = damage;
}

void CharacterBuilder::RollHitPoints()
{
	// initial HP max defined as full hit die value
	_max = stoi( _type->hitDie.substr( 1, _type->hitDie.length() - 1 ) );

	// plus constitution modifier / level
	_max += _level * Score::ToModifier( _scores.at( Stats().Constitution ) );

	// then a roll of the hit die / level beyond 1
	_max += Dice::Roll( to_string( _level - 1 ) + _type->hitDie );

	// reset damage
	_damage = 0;
}

void CharacterBuilder::CopyHitPointsFrom( Character const * const character )
{
	_max = character->GetStat( Stats().MaxHitPoints );

	_damage = character->GetStat( Stats().DamageTaken );
}

void CharacterBuilder::SetScore( StatType const * type, const unsigned int & score )
{
	_scores.insert_or_assign( type, Score( type, score ) );
}

void CharacterBuilder::RollScore( StatType const * type )
{
	_scores.insert_or_assign( type, Score( type, Dice::Roll( "3d6" ) ) );
}

void CharacterBuilder::RollScores()
{
	RollScore( Stats().Strength );
	RollScore( Stats().Dexterity );
	RollScore( Stats().Constitution );
	RollScore( Stats().Intelligence );
	RollScore( Stats().Wisdom );
	RollScore( Stats().Charisma );
}

void CharacterBuilder::CopyScoresFrom( Character const * const character )
{
	SetScore( Stats().Strength, character->GetAbilityScore( Stats().Strength ) );
	SetScore( Stats().Dexterity, character->GetAbilityScore( Stats().Dexterity ) );
	SetScore( Stats().Constitution, character->GetAbilityScore( Stats().Constitution ) );
	SetScore( Stats().Intelligence, character->GetAbilityScore( Stats().Intelligence ) );
	SetScore( Stats().Wisdom, character->GetAbilityScore( Stats().Wisdom ) );
	SetScore( Stats().Charisma, character->GetAbilityScore( Stats().Charisma ) );
}

void CharacterBuilder::AddWorn( Equipment const * equipment )
{
	_worn.at( equipment->slot ).Equip( equipment );
}

void CharacterBuilder::CopyWornFrom( Character const * const character )
{
	for ( auto worn : character->GetWorn() )
	{
		if ( !worn.second.IsEmpty() )
		{
			AddWorn( worn.second.GetEquipped() );
		}
	}
}

void CharacterBuilder::CopyBackpackFrom( Character const * const character )
{
	_backpack = *(character->GetBackpack());
}

ItemContainer & CharacterBuilder::AccessBackpack()
{
	return _backpack;
}

Character * CharacterBuilder::Create()
{
	return new Character( _playable, _level, _type, _alignment, _name, _max, _damage, _scores, _worn, _backpack );
}
