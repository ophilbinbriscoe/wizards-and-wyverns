#include "Window.h"
#include <iostream>

namespace view
{
	Window::Window( int width, int height, const char * title ) : _repaint( false ), _closed( false ), _hidden( false ), _title( title ), _clock(), _gui()
	{
		_window = new sf::RenderWindow( sf::VideoMode( width, height ), title, sf::Style::Titlebar | sf::Style::Close );

		sf::Texture::getMaximumSize();
		sf::Shader::isAvailable();
		
		sfg::Renderer::Set( sfg::VertexArrayRenderer::Create() );

		_window->resetGLStates();
		_window->clear();
	}

	Window::~Window()
	{
		delete _window;
	}

	void Window::Add( sf::Drawable * drawable )
	{
		_drawables.push_back( drawable );
	}

	void Window::Remove( const sf::Drawable * drawable )
	{
		_drawables.erase
		(
			std::remove
			(
				_drawables.begin(),
				_drawables.end(),
				drawable
			),
			_drawables.end()
		);
	}

	void Window::Add( shared_ptr<sfg::Window> window )
	{
		_windows.push_back( window );
	}

	void Window::Remove( shared_ptr<sfg::Window> window )
	{
		_windows.erase
		(
			std::remove
			(
				_windows.begin(),
				_windows.end(),
				window
			),
			_windows.end()
		);
	}

	void Window::RequestRepaint()
	{
		_repaint = true;
	}

	void Window::RepaintIfRequested()
	{
		if ( _repaint )
		{
			Repaint();
			_repaint = false;
		}
	}

	void Window::Repaint()
	{
		_window->setActive( true );

		_window->clear();

		for ( auto drawable : _drawables )
		{
			_window->draw( *drawable );
		}

		auto elapsed = _clock.getElapsedTime().asSeconds();

		for ( auto window : _windows )
		{
			window->Update( elapsed );
		}

		_gui.Display( *_window );

		_window->display();

		_timestamp = _clock.getElapsedTime().asMilliseconds();

		// notify listeners that a Repaint occurred
		OnRepaint( this );
	}

	void Window::PollEvents()
	{
		sf::Event event;

		while ( _window->pollEvent( event ) )
		{
			RequestRepaint();

			if ( event.type == sf::Event::EventType::Closed )
			{
				_window->close();
				_closed = true;
				OnClose( this );
			}

			for ( auto widget : _windows )
			{
				widget->HandleEvent( event );
			}

			switch ( event.type )
			{
				case sf::Event::EventType::Closed:
				{
					_window->close();
					_closed = true;
					OnClose( this );
				}
				break;

				case sf::Event::EventType::KeyPressed:
				OnKeyPressed( this, event.key );
				break;

				case sf::Event::EventType::KeyReleased:
				OnKeyReleased( this, event.key );
				break;

				case sf::Event::EventType::TextEntered:
				OnText( this, event.text );
				break;

				case sf::Event::EventType::MouseButtonPressed:
				OnMouseButtonPressed( this, event.mouseButton );
				break;

				case sf::Event::EventType::MouseButtonReleased:
				OnMouseButtonReleased( this, event.mouseButton );
				break;

				case sf::Event::EventType::MouseMoved:
				OnMouseMove( this, event.mouseMove );
				break;

				case sf::Event::EventType::MouseWheelMoved:
				OnMouseWheel( this, event.mouseWheel );
				break;

				case sf::Event::EventType::Resized:
				OnResize( this, event.size.width, event.size.height );
				break;
			}
		}
	}

	bool Window::HasClosed()
	{
		return _closed;
	}

	void Window::SetSize( unsigned int width, unsigned int height )
	{
		_window->setSize( sf::Vector2u( width, height ) );
	}

	void Window::SetTitle( const string & title )
	{
		// set the window's title
		_window->setTitle( title );
	}

	void Window::Close()
	{
		// close the window
		_window->close();

		// mark as closed
		_closed = true;

		// notify listeners that the window was closed
		OnClose( this );
	}
	
	void Window::Show()
	{
		if ( _hidden && !_closed )
		{
			// restart the clock
			_clock.restart();

			// create a new window
			_window->create( sf::VideoMode( _windowWidth, _windowHeight ), _title );

			// repaint
			Repaint();
		}
	}
	
	void Window::Hide()
	{
		if ( !_hidden )
		{
			// close the window
			_window->close();
			
			// get current size
			sf::Vector2u size = _window->getSize();
			
			// cache size for use in Show()
			_windowWidth = size.x;
			_windowHeight = size.y;
		}
	}

	sf::Vector2i Window::GetPosition() const
	{
		return _window->getPosition();
	}

	const int Window::GetElapsedMilliseconds() const
	{
		return _clock.getElapsedTime().asMilliseconds();
	}

	const int Window::GetMillisecondsSinceLastRepaint() const
	{
		return _clock.getElapsedTime().asMilliseconds() - _timestamp;
	}

	sf::Vector2u Window::GetSize() const
	{
		return _window->getSize();
	}
}
