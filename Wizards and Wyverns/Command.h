#pragma once

#include <memory>
#include <stack>

///Encapsulates the procedure for executing a single logical operation.
class Command
{
public:
	~Command();
	virtual void Execute() = 0;
protected:
	Command();
};

///Encapsulates the procedures for executing and undoing a single logical operation.
/** User defined types implementing UndoableCommand might look to the Memento pattern for saving the state of target data before the command is executed.*/
class UndoableCommand : public Command
{
public:
	~UndoableCommand();
	virtual void Undo() = 0;
protected:
	UndoableCommand();
};

/// Provides a managed context for executing and undoing commands.
/** When an UndoableCommand is executed through a CommandManager, the CommandManager assumes shared ownership of the UndoableCommand for as long as it is present in either the undo or redo stacks, or until the CommandManager goes out of scope.
	Since the CommandManager does not need a basic Command beyond the scope of the Execute procedure, it does not assume any ownership of the Command.
*/
class CommandManager
{
public:
	CommandManager();
	~CommandManager();
	void Execute( Command& command );
	void Execute( std::shared_ptr<UndoableCommand> command );
	void Undo();
	void Redo();
	int UndoStackSize() const;
	int RedoStackSize() const;
private:
	void ClearStack( std::stack<std::shared_ptr<UndoableCommand>>& stack );
	std::stack<std::shared_ptr<UndoableCommand>> _undoStack;
	std::stack<std::shared_ptr<UndoableCommand>> _redoStack;
};
