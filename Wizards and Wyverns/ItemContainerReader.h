#pragma once

#include <stdio.h>

#include "ItemContainer.h"

void ReadItemContainer( FILE * file, const int& n, const vector<Item*>& items, ItemContainer& destination );

