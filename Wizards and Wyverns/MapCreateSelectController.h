#pragma once

#include "CreateSelectMenuController.h"
#include "Map.h"

class MapCreateSelectController : public CreateSelectMenuController<Map>
{
public:
	MapCreateSelectController();
	virtual ~MapCreateSelectController();

protected:
	virtual bool Prevalidate();

	shared_ptr<sfg::Entry> _width;
	shared_ptr<sfg::Entry> _height;

	virtual void Create() const;
	virtual void Edit( Map * map ) const;
};

