#include "DomainConcept.h"

unsigned int  DomainConcept::next = 100;

map<unsigned int, DomainConcept *>& DomainConcept::Concepts()
{
	static map<unsigned int, DomainConcept *>* concepts = new map<unsigned int, DomainConcept *>();

	return *concepts;
}

unsigned int DomainConcept::Next()
{
	while ( Concepts().find( next ) != Concepts().end() )
	{
		next++;
	}

	return next++;
}

unsigned int DomainConcept::Enumerator() const
{
	return enumerator;
}

DomainConcept::DomainConcept( const string& name, const string& abbreviation, const unsigned int& enumerator ) : name( name ), enumerator( enumerator == -1 ? Next() : enumerator ), abbreviation( abbreviation )
{
	if ( enumerator >= 0 && Concepts().find( enumerator ) != Concepts().end() )
	{
		Concepts().at( enumerator )->enumerator = Next();
		Concepts().insert( pair<unsigned int, DomainConcept *>( Concepts().at( enumerator )->enumerator, Concepts().at( enumerator ) ) );
		Concepts().insert_or_assign( enumerator, this );
	}
	else
	{
		Concepts().insert( pair<unsigned int, DomainConcept *>( this->enumerator, this ) );
	}
}

DomainConcept::~DomainConcept()
{}
