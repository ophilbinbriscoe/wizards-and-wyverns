#include "AbilitiesObserver.h"

#include "StatFormatter.h"

view::AbilitiesObserver::AbilitiesObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	auto box = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 5.0f );

	_boxLabels = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 2.0f );
	
	auto boxScore = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 2.0f );
	auto boxBonus = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 2.0f );
	auto boxModifier = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 2.0f );

	CreateAbilityLabels( Stats().Strength, _boxLabels, boxScore, boxBonus, boxModifier );
	CreateAbilityLabels( Stats().Dexterity, _boxLabels, boxScore, boxBonus, boxModifier );
	CreateAbilityLabels( Stats().Constitution, _boxLabels, boxScore, boxBonus, boxModifier );
	CreateAbilityLabels( Stats().Intelligence, _boxLabels, boxScore, boxBonus, boxModifier );
	CreateAbilityLabels( Stats().Wisdom, _boxLabels, boxScore, boxBonus, boxModifier );
	CreateAbilityLabels( Stats().Charisma, _boxLabels, boxScore, boxBonus, boxModifier );

	box->Pack( _boxLabels );
	box->Pack(  boxScore );
	box->Pack(  boxBonus );
	box->Pack(  boxModifier );

	_container->Pack( box, false, false );

	_boxLabels->Show( false );
}

view::AbilitiesObserver::~AbilitiesObserver()
{
	_container->Remove( _boxLabels->GetParent() );
}

void view::AbilitiesObserver::CreateAbilityLabels( StatType const * type, shared_ptr<sfg::Box> boxLabels, shared_ptr<sfg::Box> boxScores, shared_ptr<sfg::Box> boxBonuses, shared_ptr<sfg::Box> boxModifiers )
{
	auto label = sfg::Label::Create( type->name + string( ":" ) );

	label->SetAlignment( { 0.0f, 0.5f } );

	boxLabels->Pack( label, false, false );

	CreateScoreLabel( type, boxScores );
	CreateBonusLabel( type, boxBonuses );
	CreateModifierLabel( type, boxModifiers );
}

void view::AbilitiesObserver::CreateScoreLabel( StatType const * type, shared_ptr<sfg::Box> box )
{
	shared_ptr<sfg::Label> label = sfg::Label::Create( "" );

	_scoreLabels.insert( pair<StatType const *, shared_ptr<sfg::Label>>( type, label ) );

	box->Pack( label, false, false );
}

void view::AbilitiesObserver::CreateBonusLabel( StatType const * type, shared_ptr<sfg::Box> box )
{
	shared_ptr<sfg::Label> label = sfg::Label::Create( "" );

	label->SetAlignment( { 0.0f, 0.5f } );

	_bonusLabels.insert( pair<StatType const *, shared_ptr<sfg::Label>>( type, label ) );

	box->Pack( label, false, false );
}

void view::AbilitiesObserver::CreateModifierLabel( StatType const * type, shared_ptr<sfg::Box> box )
{
	shared_ptr<sfg::Label> label = sfg::Label::Create( "" );
	
	label->SetAlignment( { 1.0f, 0.5f } );

	_modifierLabels.insert( pair<StatType const *, shared_ptr<sfg::Label>>( type, label ) );

	box->Pack( label, false, false );
}

void view::AbilitiesObserver::StartObserving()
{
	for ( auto pair : _scoreLabels )
	{
		_target->GetAbilityScore( pair.first ).OnModified.Subscribe( this, &AbilitiesObserver::HandleScoreModified );
		
		pair.second->Show( true );
		
		SetScoreLabel( &_target->GetAbilityScore( pair.first ) );
	}

	for ( auto pair : _bonusLabels )
	{
		_target->GetEquipmentBonus( pair.first ).OnModified.Subscribe( this, &AbilitiesObserver::HandleBonusModified );
		
		pair.second->Show( true );
		
		SetBonusLabel( &_target->GetEquipmentBonus( pair.first ) );
	}

	for ( auto pair : _modifierLabels )
	{
		_target->GetAbilityModifier( pair.first ).OnModified.Subscribe( this, &AbilitiesObserver::HandleModifierModified );
		
		pair.second->Show( true );
		
		SetModifierLabel( &_target->GetAbilityModifier( pair.first ) );
	}

	_boxLabels->Show( true );
}

void view::AbilitiesObserver::StopObserving()
{	
	for ( auto pair : _scoreLabels )
	{
		_target->GetAbilityScore( pair.first ).OnModified.Unsubscribe( this, &AbilitiesObserver::HandleScoreModified );
		
		pair.second->Show( false );
	}

	for ( auto pair : _bonusLabels )
	{
		_target->GetEquipmentBonus( pair.first ).OnModified.Unsubscribe( this, &AbilitiesObserver::HandleBonusModified );
		
		pair.second->Show( false );
	}

	for ( auto pair : _modifierLabels )
	{
		_target->GetAbilityModifier( pair.first ).OnModified.Unsubscribe( this, &AbilitiesObserver::HandleModifierModified );
		
		pair.second->Show( false );
	}

	_boxLabels->Show( false );
}

void view::AbilitiesObserver::HandleScoreModified( Score const * const score )
{
	SetScoreLabel( score );
}

void view::AbilitiesObserver::HandleBonusModified( Bonus const * const bonus )
{
	SetBonusLabel( bonus );
}

void view::AbilitiesObserver::HandleModifierModified( Modifier const * const modifier )
{
	SetModifierLabel( modifier );
}

void view::AbilitiesObserver::SetScoreLabel( Score const * const score )
{
	_scoreLabels.at( score->type )->SetText( to_string( (int) *score ) );
}

void view::AbilitiesObserver::SetBonusLabel( Bonus const * const bonus )
{
	_bonusLabels.at( bonus->type )->SetText( BonusString( (int) *bonus ) );
}

void view::AbilitiesObserver::SetModifierLabel( Modifier const * const modifier )
{
	_modifierLabels.at( modifier->type )->SetText( BonusString( (int) *modifier ) );
}