#pragma once

#include <vector>
#include <queue>
#include <memory>
#include <functional>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <SFGUI/Renderers.hpp>

#include "Event.h"

namespace view
{
	/// Abstracts an SFML window. Implements callback-based input event handling.
	/** Manages a RenderWindow instance. The instance is initialized and automatically displayed by the Window's constructor.
		RepaintIfRequested will redraw the Window only if a repaint has been requested.
	*/
	class Window
	{
	public:
		// event types
		typedef Event<Window*, sf::Event::MouseButtonEvent> MouseButtonEvent;
		typedef Event<Window*, sf::Event::MouseWheelEvent> MouseWheelEvent;
		typedef Event<Window*, sf::Event::MouseMoveEvent> MouseMoveEvent;
		typedef Event<Window*, sf::Event::TextEvent> TextEvent;
		typedef Event<Window*, sf::Event::KeyEvent> KeyEvent;
		typedef Event<Window*, int, int> ResizeEvent;
		typedef Event<Window*> WindowEvent;

		// event handler types
		typedef AbstractEventHandler<Window*, sf::Event::MouseButtonEvent> MouseButtonEventHandler;
		typedef AbstractEventHandler<Window*, sf::Event::MouseWheelEvent> MouseWheelEventHandler;
		typedef AbstractEventHandler<Window*, sf::Event::MouseMoveEvent> MouseMoveEventHandler;
		typedef AbstractEventHandler<Window*, sf::Event::TextEvent> TextEventHandler;
		typedef AbstractEventHandler<Window*, sf::Event::KeyEvent> KeyEventHandler;
		typedef AbstractEventHandler<Window*, int, int> ResizeEventHandler;
		typedef AbstractEventHandler<Window*> CloseEventHandler;

		Window( int width, int height, const char * title );
		~Window();

		void Add( sf::Drawable* drawable );
		void Remove( const sf::Drawable* drawable );

		void Add( shared_ptr<sfg::Window> window );
		void Remove( shared_ptr<sfg::Window> window );

		void Repaint();
		void RequestRepaint();
		void RepaintIfRequested();
		void PollEvents();
		bool HasClosed();
		void SetSize( unsigned int width, unsigned int height );
		void SetTitle( const string& title );
		void Close();
		void Show();
		void Hide();

		sf::Vector2i GetPosition() const;

		const int GetElapsedMilliseconds() const;
		const int GetMillisecondsSinceLastRepaint() const;

		sf::Vector2u GetSize() const;

		// events
		MouseButtonEvent OnMouseButtonPressed;
		MouseButtonEvent OnMouseButtonReleased;
		KeyEvent OnKeyPressed;
		KeyEvent OnKeyReleased;
		MouseWheelEvent OnMouseWheel;
		MouseMoveEvent OnMouseMove;
		TextEvent OnText;
		WindowEvent OnClose;
		WindowEvent OnRepaint;
		ResizeEvent OnResize;

	private:
		bool _repaint;
		bool _closed;
		bool _hidden;

		int _timestamp;

		unsigned int _windowWidth;
		unsigned int _windowHeight;

		string _title;

		sfg::SFGUI _gui;

		sf::Clock _clock;
		sf::RenderWindow* _window;

		std::vector <sf::Drawable*> _drawables;
		std::vector <shared_ptr<sfg::Window>> _windows;
	};
}
