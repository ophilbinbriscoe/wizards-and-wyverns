#pragma once

#include "Observer.h"
#include "Character.h"
#include "View.h"

namespace view
{
	class DamageBonusObserver : public View, public Observer<Character>
	{
	public:
		DamageBonusObserver( shared_ptr<sfg::Box> container );
		virtual ~DamageBonusObserver();

	private:
		virtual void StartObserving();
		virtual void StopObserving();

		void HandleDamageBonusModified( DerivedStat const * const stat );

		void SetLabel();

		shared_ptr<sfg::Label> _label;
	};
}