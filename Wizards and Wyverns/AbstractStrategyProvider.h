#pragma once

#include "StrategyProvider.h"

class AbstractStrategyProvider : public StrategyProvider
{
public:
	AbstractStrategyProvider( StrategyProvider const * user, StrategyProvider const * npc );
	virtual ~AbstractStrategyProvider();

	virtual BarrierStrategy<Position> * GetMovementStrategy( Character const * character ) const;
	virtual BarrierStrategy<MapObject const *> * GetInteractionStrategy( Character const * character ) const;
	virtual FreeStrategy<Item const *> * GetEquipStrategy( Character const * character ) const;
	virtual FreeStrategy<SlotType const *> * GetUnequipStrategy( Character const * character ) const;
	virtual FreeStrategy<StatType const *> * GetLevelStrategy( Character const * character ) const;

protected:
	StrategyProvider const * _user;
	StrategyProvider const * _npc;
};

