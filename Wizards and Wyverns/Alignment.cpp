#include "Alignment.h"

Alignment const * const Alignment::Good = new Alignment( "Good", 80 );
Alignment const * const Alignment::Evil = new Alignment( "Evil", 81 );

Alignment::Alignment( const string& name, const int& enumerator ) : DomainConcept( name, "", enumerator )
{}

Alignment::~Alignment()
{}