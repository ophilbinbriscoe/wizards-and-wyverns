#include "Inspectable.h"

Inspectable::Inspectable()
{}

Inspectable::~Inspectable()
{}

AbstractInspector::AbstractInspector()
{}

AbstractInspector::~AbstractInspector()
{}

void AbstractInspector::Inspect( MapObject const * object )
{}

void AbstractInspector::Inspect( CharacterProxy const * proxy )
{}

void AbstractInspector::Inspect( Character const * character )
{}

void AbstractInspector::Inspect( Chest const * chest )
{}
