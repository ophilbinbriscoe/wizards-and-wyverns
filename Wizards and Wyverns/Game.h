#pragma once

#include "Observer.h"
#include "Campaign.h"
#include "Character.h"
#include "Chest.h"
#include "StrategyProvider.h"
#include "MapAdjuster.h"

/// 
class Game
{
public:
	Game( Campaign const * const campaign, Character * const hero, StrategyProvider const * const provider, MapAdjuster * const adjuster );
	~Game();

	void Advance();

	void SetUserMovementStrategy( BarrierStrategy<Position>* strategy );
	void SetUserInteractionStrategy( BarrierStrategy<MapObject const *>* strategy );
	void SetUserEquipStrategy( FreeStrategy<Item const *>* strategy );
	void SetUserUnequipStrategy( FreeStrategy<SlotType const *>* strategy );
	void SetUserLevelStrategy( FreeStrategy<StatType const *>* strategy );

	Character const * const GetHero() const { return _hero; }

	Map const * const GetMap() const { return _map; }

	void Start();

	const bool& HasStarted() const { return _started; }
	const bool& HasEnded() const { return _ended; }
	const bool& IsOver() const { return _over; }

	Event<> OnGameOver;
	Event<> OnFinishCampaign;
	
	Event<Character const * const> OnBeginTurn;
	Event<Character const * const> OnEndTurn;

	Event<Character const * const> OnBeginInteraction;
	Event<Character const * const> OnEndInteraction;

	Event<Character const * const> OnBeginMovement;
	Event<Character const * const> OnEndMovement;

	Event<const Position &> OnBlockIllegalMovement;
	Event<Character const * const> OnYieldPhase;

	Event<Map const *> OnStartMap;
	Event<Map const *> OnFinishMap;

private:
	void AddPlayer( Character * character );
	void CleanUp();

	void UpdateStrategies( Character * character );

	void HandleHeroDeath( Character const * const character );
	void HandleCharacterSpawned( Character * character );
	void HandleChestSpawned( Chest * chest );
	void HandleCorpseLooted(Character const * const character, Character const * const body);

	bool ProcessMovement( const Position & position );
	void ProcessInteraction( MapObject const * object );

	void ProcessEquipRequest( FreeStrategy<Item const *> const * strategy, Character const * character );
	void ProcessUnequipRequest( FreeStrategy<SlotType const *> const * strategy, Character const * character );
	void ProcessIncreaseRequest( FreeStrategy<StatType const *> const * strategy, Character const * character );

	void Decouple( Character * character );

	void RollInitiative();

	unsigned int RollInitiative( Character * character );

	void IncrementTurnSegment();
	void IncrementActivePlayer();

	void BeginTurn();
	void EndTurn();

	void BeginMovement();
	void BeginInteraction();

	void PollMovement();
	void PollInteraction();

	void EndMovement();
	void EndInteraction();

	void StartMap();
	void FinishMap();

	void End();
	void Over();

	// track whether this Game has started, or is still in setup
	bool _started;

	// track whether the Game has ended (win state)
	bool _ended;

	// track whether the Game has ended (fail state)
	bool _over;

	// allow fast lookup of mutable objects from immutable ones
	map<MapObject const *, MapObject *> _privileged;

	// enumeration of the discrete logic segments comprised by a single turn of play
	vector<void (Game::*)()> _segments;

	// enumerates the discrete logic segments comprised by a single turn of play
	int _segment;

	// query the active player
	Character * ActivePlayer() const;

	// players, sorted by initiative
	vector<Character *> _players;

	// the index of the player who presently has their turn
	int _player;

	map<Character *, BarrierStrategy<Position>*> _movementStrategies;
	map<Character *, BarrierStrategy<MapObject const *>*> _interactionStrategies;
	map<Character *, FreeStrategy<StatType const *>*> _levelStrategies;
	map<Character *, FreeStrategy<Item const *>*> _equipStrategies;
	map<Character *, FreeStrategy<SlotType const *>*> _unequipStrategies;

	// the main Character
	Character * const _hero;

	// the current CampaignNode
	CampaignNode * _node;

	// the current Map
	Map * _map;

	// Map adjuster
	MapAdjuster * const _adjuster;

	// Strategy provider
	StrategyProvider const * const _provider;
};

