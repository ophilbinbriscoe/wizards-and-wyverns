#include "HitPointObserver.h"

#include "StatFormatter.h"

view::HitPointObserver::HitPointObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	auto boxHP = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL );

	_barHP = sfg::ProgressBar::Create( sfg::ProgressBar::Orientation::HORIZONTAL );
	
	_labelHP = sfg::Label::Create( "" );
	_labelHP->SetAlignment( { 0.0f, 0.5f } );

	_barHP->SetRequisition( { 100.0f, 18.0f } );

	auto label = sfg::Label::Create( Stats().HitPoints->name + string( ": " ) );

	label->SetAlignment( { 0.0f, 0.5f } );

	boxHP->Pack( label );
	boxHP->Pack( _labelHP );
	boxHP->Pack( _barHP );

	_container->Pack( boxHP, false, false );
}

view::HitPointObserver::~HitPointObserver()
{}

void view::HitPointObserver::StartObserving()
{	
	_target->GetStat( Stats().MaxHitPoints ).OnModified.Subscribe( this, &HitPointObserver::HandleHitPointsModified );
	_target->GetStat( Stats().HitPoints ).OnModified.Subscribe( this, &HitPointObserver::HandleHitPointsModified );

	_barHP->Show( true );
	
	SetHitPointBar();
}

void view::HitPointObserver::StopObserving()
{
	_target->GetStat( Stats().MaxHitPoints ).OnModified.Unsubscribe( this, &HitPointObserver::HandleHitPointsModified );
	_target->GetStat( Stats().HitPoints ).OnModified.Unsubscribe( this, &HitPointObserver::HandleHitPointsModified );

	_barHP->Show( false );
	
	_labelHP->SetText( "" );
}

void view::HitPointObserver::HandleHitPointsModified( DerivedStat const * const stat )
{
	SetHitPointBar();
}

void view::HitPointObserver::SetHitPointBar()
{
	_barHP->SetFraction( _target->GetStat( Stats().HitPoints ) / (float) _target->GetStat( Stats().MaxHitPoints ) );
	_labelHP->SetText( to_string( (int) _target->GetStat( Stats().HitPoints ) ) + string( "/" ) + to_string( (int) _target->GetStat( Stats().MaxHitPoints ) ) );
}
