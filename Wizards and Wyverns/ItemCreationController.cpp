#include "ItemCreationController.h"
#include "Item.h"
#include "Equipment.h"
#include "Database.h"
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/Renderers.hpp>


ItemCreationController::ItemCreationController()
{

	auto masterBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

	auto button = sfg::Button::Create();
	button->SetLabel("Create");
	button->GetSignal(sfg::Widget::OnLeftClick).Connect(std::bind(&ItemCreationController::CreateItem, this));

	auto name_label = sfg::Label::Create("Name");
	success_label = sfg::Label::Create("");
	name_entry = sfg::Entry::Create();


	auto radio_box = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	item_radio = sfg::RadioButton::Create("Generic Item");
	armor_radio = sfg::RadioButton::Create("Armor", item_radio->GetGroup());
	belt_radio = sfg::RadioButton::Create("Belt", item_radio->GetGroup());
	boots_radio = sfg::RadioButton::Create("Boots", item_radio->GetGroup());
	helmet_radio = sfg::RadioButton::Create("Helmet", item_radio->GetGroup());
	ring_radio = sfg::RadioButton::Create("Ring", item_radio->GetGroup());
	shield_radio = sfg::RadioButton::Create("Shield", item_radio->GetGroup());
	weapon_radio = sfg::RadioButton::Create("Weapon", item_radio->GetGroup());
	radio_box->Pack(item_radio);
	radio_box->Pack(armor_radio);
	radio_box->Pack(belt_radio);
	radio_box->Pack(boots_radio);
	radio_box->Pack(helmet_radio);
	radio_box->Pack(ring_radio);
	radio_box->Pack(shield_radio);
	radio_box->Pack(weapon_radio);

	auto stat_radio_box = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	str_radio = sfg::RadioButton::Create("Strength");
	dex_radio = sfg::RadioButton::Create("Dexterity", str_radio->GetGroup());
	int_radio = sfg::RadioButton::Create("Intelligence", str_radio->GetGroup());
	wis_radio = sfg::RadioButton::Create("Wisdom", str_radio->GetGroup());
	con_radio = sfg::RadioButton::Create("Constitution", str_radio->GetGroup());
	char_radio = sfg::RadioButton::Create("Charisma", str_radio->GetGroup());
	ac_radio = sfg::RadioButton::Create("Armor Class", str_radio->GetGroup());

	not_selected_radio = sfg::RadioButton::Create("", str_radio->GetGroup());

	stat_radio_box->Pack(str_radio);
	stat_radio_box->Pack(dex_radio);
	stat_radio_box->Pack(int_radio);
	stat_radio_box->Pack(wis_radio);
	stat_radio_box->Pack(con_radio);
	stat_radio_box->Pack(char_radio);
	stat_radio_box->Pack(ac_radio);

	stat_radio_box->Pack(not_selected_radio);

	auto entry_box = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	str_entry = sfg::Entry::Create("");
	dex_entry = sfg::Entry::Create("");
	int_entry = sfg::Entry::Create("");
	wis_entry = sfg::Entry::Create("");
	con_entry = sfg::Entry::Create("");
	char_entry = sfg::Entry::Create("");
	ac_entry = sfg::Entry::Create("");
	dmg_entry = sfg::Entry::Create("Damage Bonus");
	atk_entry = sfg::Entry::Create("Attack Bonus");

	

	entry_box->Pack(str_entry);
	entry_box->Pack(dex_entry);
	entry_box->Pack(int_entry);
	entry_box->Pack(wis_entry);
	entry_box->Pack(con_entry);
	entry_box->Pack(char_entry);
	entry_box->Pack(ac_entry);
	entry_box->Pack(dmg_entry);
	entry_box->Pack(atk_entry);

	name_entry->SetRequisition(sf::Vector2f(80.f, 0.f));
	weapon_dropdown = sfg::ComboBox::Create();

	auto dropdown_box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

	for ( int i = 30; i <= 57; i++ )
	{
		weapon_dropdown->AppendItem( DomainConcept::Get<WeaponType>( i )->name );
	}

	weapon_dropdown->SelectItem( 0 );

	dropdown_box->Pack( weapon_dropdown );

	masterBox->Pack(name_label);
	masterBox->Pack(name_entry);
	masterBox->Pack(radio_box);
	masterBox->Pack(stat_radio_box);
	masterBox->Pack(dropdown_box);
	masterBox->Pack(entry_box);
	masterBox->Pack(button);
	masterBox->Pack(success_label);

	item_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::ItemSelect, this));
	armor_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::ArmorSelect, this));
	belt_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::BeltSelect, this));
	boots_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::BootsSelect, this));
	helmet_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::HelmetSelect, this));
	ring_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::RingSelect, this));
	shield_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::ShieldSelect, this));
	weapon_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::WeaponSelect, this));

	str_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::StrSelect, this));
	dex_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::DexSelect, this));
	int_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::IntSelect, this));
	wis_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::WisSelect, this));
	con_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::ConSelect, this));
	char_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::CharSelect, this));
	ac_radio->GetSignal(sfg::ToggleButton::OnToggle).Connect(std::bind(&ItemCreationController::ACSelect, this));
	auto buttonQuit = sfg::Button::Create("Quit");
	buttonQuit->GetSignal(sfg::Button::OnLeftClick).Connect(bind(&ItemCreationController::HandleClickQuit, this));

	HideAllRadios();

	masterBox->SetSpacing(5.f);

	_menu->Pack(masterBox);
	_menu->Pack(buttonQuit);

	Finalize();
}

ItemCreationController::~ItemCreationController()
{}

//! Returns to the previous menu
void ItemCreationController::HandleClickQuit()
{
	Pop(this);
}
//! Hides all radio boxes
void ItemCreationController::HideAllRadios()
{

	not_selected_radio->SetActive(true);
	not_selected_radio->Show(false);

	str_radio->Show(false);
	dex_radio->Show(false);
	wis_radio->Show(false);
	con_radio->Show(false);
	int_radio->Show(false);
	char_radio->Show(false);
	ac_radio->Show(false);

	HideAllEntries();

}
//! Hides all the entry boxes
void ItemCreationController::HideAllEntries()
{
	str_entry->Show(false);
	dex_entry->Show(false);
	int_entry->Show(false);
	wis_entry->Show(false);
	con_entry->Show(false);
	char_entry->Show(false);
	ac_entry->Show(false);
	dmg_entry->Show(false);
	atk_entry->Show(false);
	weapon_dropdown->Show(false);
}
//! Resets all the entries to their initial state
void ItemCreationController::ResetEntries()
{
	str_entry->SetText("");
	dex_entry->SetText("");
	int_entry->SetText("");
	wis_entry->SetText("");
	con_entry->SetText("");
	char_entry->SetText("");
	ac_entry->SetText("");
	dmg_entry->SetText("Damage");
	atk_entry->SetText("Attack");
}
//! Hides all radios except Item (selected by default)
void ItemCreationController::ItemSelect()
{
	HideAllRadios();
}
//! Hides all radios except Armor
void ItemCreationController::ArmorSelect()
{
	HideAllRadios();
	ac_radio->Show(true);

}
//! Hides all radios except Belt
void ItemCreationController::BeltSelect()
{
	HideAllRadios();
	str_radio->Show(true);
	con_radio->Show(true);

}
//! Hides all radios except Boots
void ItemCreationController::BootsSelect()
{
	HideAllRadios();
	dex_radio->Show(true);
	con_radio->Show(true);
	ac_radio->Show(true);
}
//! Hides all radios except Helmet
void ItemCreationController::HelmetSelect()
{
	HideAllRadios();
	wis_radio->Show(true);
	int_radio->Show(true);
	ac_radio->Show(true);
}
//! Hides all radios except Ring
void ItemCreationController::RingSelect()
{
	HideAllRadios();
	str_radio->Show(true);
	wis_radio->Show(true);
	con_radio->Show(true);
	char_radio->Show(true);
	ac_radio->Show(true);
}
//! Hides all radios except Shield
void ItemCreationController::ShieldSelect()
{
	HideAllRadios();
	ac_radio->Show(true);
}
//! Hides all radios except those necessary for Weapon Creation
void ItemCreationController::WeaponSelect()
{
	HideAllRadios();
	atk_entry->Show(true);
	dmg_entry->Show(true);
	//melee_radio->Show(true);
	//ranged_radio->Show(true);
	weapon_dropdown->Show(true);
}
//! Hides all entries except strength
void ItemCreationController::StrSelect()
{
	HideAllEntries();
	str_entry->Show(true);
}
//! Hides all entries except dexterity
void ItemCreationController::DexSelect()
{
	HideAllEntries();
	dex_entry->Show(true);
}
//! Hides all entries except intelligence
void ItemCreationController::IntSelect()
{
	HideAllEntries();
	int_entry->Show(true);
}
//! Hides all entries except constitution
void ItemCreationController::ConSelect()
{
	HideAllEntries();
	con_entry->Show(true);
}
//! Hides all entries except wisdom
void ItemCreationController::WisSelect()
{
	HideAllEntries();
	wis_entry->Show(true);
}
//! Hides all entries except charisma
void ItemCreationController::CharSelect()
{
	HideAllEntries();
	char_entry->Show(true);
}
//! Hides all entries except armor class
void ItemCreationController::ACSelect()
{
	HideAllEntries();
	ac_entry->Show(true);
}
//! Creates a dropdown of the list of melee weapons
void ItemCreationController::MeleeSelect()
{
	weapon_dropdown->Clear();
	for (int i = 30; i <= 57; i++)
	{
		weapon_dropdown->AppendItem(DomainConcept::Get<WeaponType>(i)->name);
	}
}

//! Creates an item
void ItemCreationController::CreateItem()
{

	string itemName = name_entry->GetText();
	string string_bonus = "";
	string string_bonus2 = "";
	SlotType const * slot_type = Slots().Body;
	StatType const * enchant_type = Stats().ArmorClass;
	StatType const * enchant_type2;
	
	unsigned int enchant_bonus = 0;
	unsigned int enchant_bonus2 = 0;

	
	bool validItem = false;
	char * end;

	//Finds which radio is currently active
	if (item_radio->IsActive())
	{
		if (itemName != "")
		{
			Data().items.Add(&Item(itemName));
			validItem = true;
			name_entry->SetText("");
			success_label->SetText("Item created!");
		}
		else
		{
			success_label->SetText("No item name, item could not be created");
		}
	}
	else if (weapon_radio->IsActive())
	{
		string_bonus = atk_entry->GetText();
		string_bonus2 = dmg_entry->GetText();
		//Makes sure both bonuses are not empty
		if (string_bonus != "" && string_bonus2 != "" && itemName != "")
		{
			//Creates a new enchantment for each bonus
			enchant_bonus = std::strtol(string_bonus.c_str(), &end, 10);
			Enchantment enchant1 = Enchantment(Stats().AttackBonus, enchant_bonus);

			enchant_bonus2 = std::strtol(string_bonus2.c_str(), &end, 10);
			Enchantment enchant2 = Enchantment(Stats().DamageBonus, enchant_bonus2);
			
			//If both bonuses are between 1 and 5, creates a new item
			if (enchant_bonus <= 5 && enchant_bonus >= 1 && enchant_bonus2 <= 5 && enchant_bonus2 >= 1)
			{
				Data().items.Add(new Weapon(itemName, DomainConcept::Get<WeaponType>(30 + weapon_dropdown->GetSelectedItem()), Enchantments{ enchant1, enchant2 }));
				validItem = true;
			}
			else
				success_label->SetText("Invalid bonuses, item could not be created");
		}
		else if (itemName != "")
			success_label->SetText("Invalid bonuses, weapon could not be created");
		else
			success_label->SetText("A weapon needs a name");
	}
	else	
	{
		//Finds which equipement radio is active
		if (armor_radio->IsActive())
		{
			slot_type = Slots().Body;
		}

		if (belt_radio->IsActive())
		{
			slot_type = Slots().Waist;
		}

		if (boots_radio->IsActive())
		{
			slot_type = Slots().Feet;
		}

		if (helmet_radio->IsActive())
		{
			slot_type = Slots().Head;
		}

		if (ring_radio->IsActive())
		{
			slot_type = Slots().RingFinger;
		}

		if (shield_radio->IsActive())
		{
			slot_type = Slots().ShieldHand;
		}
		//Finds which bonus radio is active
		if (str_radio->IsActive())
		{
			enchant_type = Stats().Strength;
			string_bonus = str_entry->GetText();
		}

		if (dex_radio->IsActive())
		{
			enchant_type = Stats().Dexterity;
			string_bonus = dex_entry->GetText();
		}

		if (int_radio->IsActive())
		{
			enchant_type = Stats().Intelligence;
			string_bonus = int_entry->GetText();
		}

		if (wis_radio->IsActive())
		{
			enchant_type = Stats().Wisdom;
			string_bonus = wis_entry->GetText();
		}

		if (con_radio->IsActive())
		{
			enchant_type = Stats().Constitution;
			string_bonus = con_entry->GetText();
		}

		if (char_radio->IsActive())
		{
			enchant_type = Stats().Charisma;
			string_bonus = char_entry->GetText();
		}

		if (ac_radio->IsActive())
		{
			enchant_type = Stats().ArmorClass;
			string_bonus = ac_entry->GetText();
		}
		//Making sure the bonus is filled converts it to an int
		if (string_bonus != "")
			enchant_bonus = std::strtol(string_bonus.c_str(), &end, 10);
		
		//Verifies if the bonus is between 1 and 5
		if (enchant_bonus <= 5 && enchant_bonus >= 1)
		{
			//Adds the item to the database
			Data().items.Add( new Equipment(itemName, slot_type, Enchantments{ Enchantment(enchant_type, enchant_bonus) }));
			validItem = true;
		}
		else if (itemName != "")
			success_label->SetText("Invalid bonus, item could not be created");
		else
		{
			success_label->SetText("No item name, item could not be created");
		}	
	}
	//If a valid item has been created, the menu is reset
	if (validItem == true)
	{
		success_label->SetText("Item created!");
		HideAllRadios();
		item_radio->SetActive(true);
		name_entry->SetText("");
		ResetEntries();
	}
}
