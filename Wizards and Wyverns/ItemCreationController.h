#pragma once
#include "Character.h"
#include "MenuController.h"

#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/Renderers.hpp>

class ItemCreationController : public MenuController
{
public:
	ItemCreationController();
	virtual ~ItemCreationController();
	void HandleClickQuit();

	void HideAllRadios();

	void HideAllEntries();

	void ResetEntries();

	void ItemSelect();

	void ArmorSelect();
	void BeltSelect();
	void BootsSelect();
	void HelmetSelect();
	void RingSelect();
	void ShieldSelect();
	void WeaponSelect();

	void StrSelect();
	void DexSelect();
	void IntSelect();
	void ConSelect();
	void WisSelect();
	void CharSelect();
	void ACSelect();

	void MeleeSelect();

	void RangedSelect();

	void CreateItem();
private:

	sfg::Entry::Ptr name_entry;
	sfg::Entry::Ptr str_entry;
	sfg::Entry::Ptr dex_entry;
	sfg::Entry::Ptr wis_entry;
	sfg::Entry::Ptr int_entry;
	sfg::Entry::Ptr con_entry;
	sfg::Entry::Ptr char_entry;
	sfg::Entry::Ptr ac_entry;
	sfg::Entry::Ptr dmg_entry;
	sfg::Entry::Ptr atk_entry;

	sfg::RadioButton::Ptr armor_radio;
	sfg::RadioButton::Ptr belt_radio;
	sfg::RadioButton::Ptr boots_radio;
	sfg::RadioButton::Ptr helmet_radio;
	sfg::RadioButton::Ptr ring_radio;
	sfg::RadioButton::Ptr shield_radio;
	sfg::RadioButton::Ptr weapon_radio;
	sfg::RadioButton::Ptr item_radio;

	sfg::RadioButton::Ptr str_radio;
	sfg::RadioButton::Ptr dex_radio;
	sfg::RadioButton::Ptr int_radio;
	sfg::RadioButton::Ptr wis_radio;
	sfg::RadioButton::Ptr con_radio;
	sfg::RadioButton::Ptr char_radio;
	sfg::RadioButton::Ptr ac_radio;
	sfg::RadioButton::Ptr melee_radio;
	sfg::RadioButton::Ptr ranged_radio;
	sfg::RadioButton::Ptr not_selected_radio;

	sfg::Label::Ptr success_label;

	sfg::ComboBox::Ptr weapon_dropdown;
};