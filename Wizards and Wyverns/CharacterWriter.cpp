#include "CharacterWriter.h"

#pragma warning(disable:4996)

CharacterWriter::CharacterWriter( std::fstream& file, const char * filepath ) : Writer( file, filepath )
{}

CharacterWriter::~CharacterWriter()
{}

void CharacterWriter::Write( const string& header, Character * character, ItemContainerWriter const * writer )
{
	_file << header << ": " << character->IsPlayable() << ' ';
	_file << character->GetClass()->Enumerator() << ' ';
	_file << character->GetLevel() << ' ';
	_file << character->GetAlignment()->Enumerator() << ' ';

	_file << character->GetStat( Stats().MaxHitPoints ) << ' ' << character->GetStat( Stats().DamageTaken ) << ' ';

	_file << (int) character->GetAbilityScore( Stats().Strength ) << ' ';
	_file << (int) character->GetAbilityScore( Stats().Dexterity ) << ' ';
	_file << (int) character->GetAbilityScore( Stats().Constitution ) << ' ';
	_file << (int) character->GetAbilityScore( Stats().Intelligence ) << ' ';
	_file << (int) character->GetAbilityScore( Stats().Wisdom ) << ' ';
	_file << (int) character->GetAbilityScore( Stats().Charisma ) << ' ';

	unsigned int n = 0;

	for ( auto pair : character->GetWorn() )
	{
		if ( !pair.second.IsEmpty() )
		{
			n++;
		}
	}

	_file << n << ' ';

	for ( auto pair : character->GetWorn() )
	{
		if ( !pair.second.IsEmpty() )
		{
			_file << writer->ItemOrder( pair.second.GetEquipped() ) << ' ';
		}
	}

	writer->Write( _file, character->GetBackpack() );

	_file << ' ' << character->GetName() << '\n';
}
