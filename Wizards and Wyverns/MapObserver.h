#pragma once

#include "Map.h"
#include "View.h"

#include "Chest.h"
#include "CharacterProxy.h"

namespace view
{
	const int MAP_TILE_SIZEi = 30;
	const float MAP_TILE_SIZEf = (float) MAP_TILE_SIZEi;

	const sf::Color MEDIUM_GREY = sf::Color( 128, 128, 128 );
	const sf::Color DARK_GREY = sf::Color( 32, 32, 32 );

	/// Implementation of an Observer for a Map.
	/** Uses a GridView to present an always-up-to-date representation of a Map's tiles.*/
	class MapObserver : public View, public Observer<Map>
	{
	public:
		MapObserver( shared_ptr<sfg::Box> container );
		virtual ~MapObserver();
		
		void SetOffset( const Position& offset );
		Position GetOffset() const { return _offset; }

		Position MapToView( const Position& position ) const;
		Position ViewToMap( const Position& position ) const;

		Event<const Position&> OnTileClicked;

		void SetWindow( Window const * window );

	private:
		Window const * _window;

		virtual void StartObserving();
		virtual void StopObserving();

		void HandleTileModified( const Position& position );

		void HandleObjectAdded( MapObject const * object );
		void HandleObjectRemoved( MapObject const * object );
		void HandleObjectMoved( MapObject const * object );

		void HandleAlignmentModified( Character const * const character );
		void HandleAlignmentModified( CharacterProxy const * const proxy );

		virtual void SubscribeToRedrawEvents( MapObject const * object );
		virtual void UnsubscribeFromRedrawEvents( MapObject const * object );

		void HandleClick();

		void Update();
		void Update( sf::RectangleShape & button, TileType const * type );

		void Draw();

		Position _offset;

		shared_ptr<sfg::Frame> _frame;
		Grid<sf::RectangleShape>* _rects;

		unsigned int _serial;
	};
}

