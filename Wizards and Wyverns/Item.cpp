#include "Item.h"

using namespace std;

Item::Item( const string& name ) : Resource( name )
{}

Item::~Item()
{}

void Item::Accept( AbstractUser * user ) const
{
	user->Use( this );
}

