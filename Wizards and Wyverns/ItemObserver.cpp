#include "ItemObserver.h"

#include "StatFormatter.h"

ItemObserver::ItemObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	_box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 0.0f );

	_label = sfg::Label::Create( "" );

	_label->SetAlignment( { 0.0f, 0.0f } );

	_box->Pack( _label );

	_enchantments = sfg::Label::Create( "" );

	_enchantments->SetAlignment( { 0.0f, 0.0f } );

	_box->Pack( _enchantments );

	_container->Pack( _box );
}

ItemObserver::~ItemObserver()
{
	_box->Show( false );

	_container->Remove( _box );
}

void ItemObserver::Use( Item const * item )
{
	// do nothing
}

void ItemObserver::Use( Equipment const * equipment )
{
	if ( equipment->GetEnchantments().size() > 0 )
	{
		_enchantments->SetRequisition( { 0.0f, 0.0f } );

		string str = "";

		for ( auto enchantment : equipment->GetEnchantments() )
		{
			str += BonusString( enchantment.bonus ) + string( " " ) + enchantment.type->abbreviation + string( ", " );
		}

		str = str.substr( 0, max( str.length() - 2, (unsigned int) 0 ) );

		_enchantments->SetText( str );
	}
}

void ItemObserver::HandleRename( Resource const * resource )
{
	_label->SetText( resource->GetName() );
}

void ItemObserver::StartObserving()
{
	_box->Show( true );

	_label->SetText( _target->GetName() );

	_target->Accept( this );

	_target->OnRename.Subscribe( this, &ItemObserver::HandleRename );
}

void ItemObserver::StopObserving()
{
	_target->OnRename.Unsubscribe( this, &ItemObserver::HandleRename );

	_label->SetText( "" );

	_enchantments->SetText( "" );

	_box->Show( false );
}