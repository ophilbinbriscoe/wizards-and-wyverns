#pragma once

#include <SFML\Graphics.hpp>

#include "DomainConcept.h"

using namespace std;

class TileType : public DomainConcept
{
public:
	static TileType const * const Free;
	static TileType const * const Wall;
	static TileType const * const Entrance;
	static TileType const * const Exit;
	
	const sf::Color fill;
	const sf::Color outline;

private:
	TileType( const string& name, const sf::Color& fill, const sf::Color& outline, const unsigned int enumerator = -1 );
	~TileType();
};

