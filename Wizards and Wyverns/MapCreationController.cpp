#include "MapCreationController.h"

#include "MessageController.h"
#include "CopyAdjuster.h"
#include "Database.h"

MapCreationController::MapCreationController( Map * hardcopy, const bool & add ) : CreationController( hardcopy, Data().maps, add ), _tool( TOOL_TILE )
{
	Prepare();

	_toolsWindow = sfg::Window::Create( sfg::Window::Style::BACKGROUND );

	_toolsWindow->SetPosition( { 50.0f, 200.0f } );
	_toolsWindow->SetRequisition( { 300.0f, 150.0f } );

	auto toolsBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	auto toolTile = sfg::RadioButton::Create( "Tile Brush" );

	auto toolsGroup = toolTile->GetGroup();

	auto toolNPC = sfg::RadioButton::Create( "Place NPC", toolsGroup );
	auto toolChest = sfg::RadioButton::Create( "Place Chest", toolsGroup );
	auto toolMove = sfg::RadioButton::Create( "Move", toolsGroup );
	auto toolRemove = sfg::RadioButton::Create( "Remove", toolsGroup );
	auto toolInspect = sfg::RadioButton::Create( "Inspect", toolsGroup );

	toolTile->GetSignal( sfg::RadioButton::OnLeftClick ).Connect( bind( [&] ()-> void { this->_tool = TOOL_TILE; this->ShowToolOptions( this->_tileOptionWindow ); this->HideInspectors(); } ) );
	toolNPC->GetSignal( sfg::RadioButton::OnLeftClick ).Connect( bind( [&] ()-> void { this->_tool = TOOL_NPC; this->ShowToolOptions( this->_npcOptionWindow ); this->InspectObject(); } ) );
	toolChest->GetSignal( sfg::RadioButton::OnLeftClick ).Connect( bind( [&] ()-> void { this->_tool = TOOL_CHEST; this->ShowToolOptions( this->_chestInspectorWindow ); this->InspectObject(); } ) );
	toolMove->GetSignal( sfg::RadioButton::OnLeftClick ).Connect( bind( [&] ()-> void { this->_tool = TOOL_MOVE; this->HideToolOptions(); this->HideInspectors(); } ) );
	toolRemove->GetSignal( sfg::RadioButton::OnLeftClick ).Connect( bind( [&] ()-> void { this->_tool = TOOL_REMOVE; this->HideToolOptions(); this->HideInspectors(); } ) );
	toolInspect->GetSignal( sfg::RadioButton::OnLeftClick ).Connect( bind( [&] ()-> void { this->_tool = TOOL_INSPECT; this->HideToolOptions(); this->InspectObject(); } ) );

	toolsBox->Pack( sfg::Label::Create( "Tools" ), false, false );

	toolsBox->Pack( toolTile, false, false );
	toolsBox->Pack( toolNPC, false, false );
	toolsBox->Pack( toolChest, false, false );
	toolsBox->Pack( toolMove, false, false );
	toolsBox->Pack( toolRemove, false, false );
	toolsBox->Pack( toolInspect, false, false );

	toolTile->SetActive( true );

	_toolsWindow->Add( toolsBox );
	_toolsWindow->Refresh();

	_tileOptionWindow = sfg::Window::Create( sfg::Window::Style::BACKGROUND );

	_tileOptionWindow->SetPosition( { 50.0f, 350.0f } );
	_tileOptionWindow->SetRequisition( { 300.0f, 0.0f } );

	auto tilesBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	_tilesCombo = sfg::ComboBox::Create();

	_tilesCombo->AppendItem( TileType::Free->name );
	_tilesCombo->AppendItem( TileType::Wall->name );
	_tilesCombo->AppendItem( TileType::Entrance->name );
	_tilesCombo->AppendItem( TileType::Exit->name );

	_tilesCombo->SelectItem( 0 );

	tilesBox->Pack( sfg::Label::Create( "Tile Types" ), false, false );

	tilesBox->Pack( _tilesCombo, false, false );

	_tileOptionWindow->Add( tilesBox );
	_tileOptionWindow->Refresh();

	_npcOptionWindow = sfg::Window::Create( sfg::Window::Style::BACKGROUND );

	_npcOptionWindow->SetPosition( { 50.0f, 350.0f } );
	_npcOptionWindow->SetRequisition( { 300.0f, 0.0f } );

	auto npcBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	_npcCombo = sfg::ComboBox::Create();

	for ( auto character : Data().characters.GetContents() )
	{
		if ( !character->IsPlayable() )
		{
			_npcCombo->AppendItem( character->GetName() );
			_npcs.push_back( character );
		}
	}

	_npcCombo->SelectItem( 0 );

	npcBox->Pack( sfg::Label::Create( "NPCs" ), false, false );

	npcBox->Pack( _npcCombo, false, false );

	_npcOptionWindow->Add( npcBox );
	_npcOptionWindow->Refresh();

	_chestInspectorWindow = sfg::Window::Create( sfg::Window::Style::BACKGROUND );

	_chestInspectorWindow->SetPosition( { 850.0f, 200.0f } );
	_chestInspectorWindow->SetRequisition( { 300.0f, 0.0f } );

	auto chestBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	chestBox->Pack( sfg::Label::Create( "Items" ), false, false );

	auto containerBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 2.0f );

	_itemContainerObserver = new view::ItemContainerObserver( containerBox );

	chestBox->Pack( containerBox );

	_itemCombo = sfg::ComboBox::Create();

	for ( auto item : Data().items.GetContents() )
	{
		_itemCombo->AppendItem( item->GetName() );
	}

	_itemCombo->SelectItem( 0 );

	chestBox->Pack( _itemCombo, false, false );

	auto buttonAddItem= sfg::Button::Create( "Add" );

	buttonAddItem->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MapCreationController::HandleClickAddItem, this ) );

	chestBox->Pack( buttonAddItem, false, false );

	_chestInspectorWindow->Add( chestBox );
	_chestInspectorWindow->Refresh();

	_npcInspectorWindow = sfg::Window::Create( sfg::Window::Style::BACKGROUND );

	_npcInspectorWindow->SetPosition( { 850.0f, 200.0f } );
	_npcInspectorWindow->SetRequisition( { 300.0f, 0.0f } );

	auto alignmentBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	alignmentBox->Pack( sfg::Label::Create( "Alignment" ), false, false );

	_alignmentCombo = sfg::ComboBox::Create();

	_alignmentCombo->AppendItem( Alignment::Good->name );
	_alignmentCombo->AppendItem( Alignment::Evil->name );

	_alignmentCombo->SelectItem( 0 );

	_alignmentCombo->GetSignal( sfg::ComboBox::OnSelect ).Connect( bind( &MapCreationController::HandleSetAlignment, this ) );

	alignmentBox->Pack( _alignmentCombo, false, false );

	_npcInspectorWindow->Add( alignmentBox );
	_npcInspectorWindow->Refresh();

	auto container = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 0.0f );

	_mapObserver = new view::MapObserver( container );

	_mapObserver->OnTileClicked.Subscribe( this, &MapCreationController::HandleTileClicked );

	_mapObserver->Observe( _target );

	_menu->Pack( container );

	_menu->Pack( sfg::Label::Create( "L/R/U/D to pan view" ) );
	_menu->Pack( sfg::Label::Create( "ENTER to reset view" ) );

	PackButtons();

	Finalize();
}

MapCreationController::~MapCreationController()
{
	// decouple
	_mapObserver->Observe( nullptr );
	_itemContainerObserver->Observe( nullptr );

	// clean up observers
	delete _mapObserver;
	delete _itemContainerObserver;
}

void MapCreationController::SubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Subscribe( this, &MapCreationController::HandleKeyPressed );

	_mapObserver->SetWindow( window );

	window->Add( _toolsWindow );
	window->Add( _tileOptionWindow );
	window->Add( _npcOptionWindow );
	window->Add( _chestInspectorWindow );
	window->Add( _npcInspectorWindow );

	ShowToolOptions( _tileOptionWindow );
	HideInspectors();
}

void MapCreationController::UnsubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Unsubscribe( this, &MapCreationController::HandleKeyPressed );

	HideToolOptions();
	HideInspectors();

	window->Remove( _toolsWindow );
	window->Remove( _tileOptionWindow );
	window->Remove( _npcOptionWindow );
	window->Remove( _chestInspectorWindow );
	window->Remove( _npcInspectorWindow );
}

Map * MapCreationController::MakeWorkingCopy( Map * hardcopy )
{
	// create an adjuster to copy the hard copy as is
	MapAdjuster * adjuster = new CopyAdjuster();

	// cache non-const-qualified pointers for later use
	adjuster->OnSpawnChest.Subscribe( this, &MapCreationController::HandleSpawn );
	adjuster->OnSpawnProxy.Subscribe( this, &MapCreationController::HandleSpawn );

	// copy chests and proxies
	auto result = adjuster->Adjust( hardcopy );

	// clean up
	delete adjuster;

	return result;
}

void MapCreationController::Inspect( Chest const * chest )
{
	_itemContainerObserver->Observe( chest );

	_chestInspectorWindow->SetRequisition( { 300.0f, 0.0f } );

	_chestInspectorWindow->RefreshAll();

	ShowInspector( _chestInspectorWindow );
}

void MapCreationController::Inspect( CharacterProxy const * proxy )
{
	_alignmentCombo->SelectItem( proxy->GetOverride() == Alignment::Good ? 0 : 1 );

	ShowInspector( _npcInspectorWindow );
}

void MapCreationController::HandleSpawn( Chest * chest )
{
	_chests.insert_or_assign( chest, chest );
}

void MapCreationController::HandleSpawn( CharacterProxy * proxy )
{
	_proxies.insert_or_assign( proxy, proxy );
}

void MapCreationController::HandleTileClicked( const Position & position )
{
	_position = position;

	if ( _target->Contains( _position ) )
	{
		switch ( _tool )
		{
			case TOOL_TILE:
			_target->SetTileAt( _position, DomainConcept::Get<TileType>( _tilesCombo->GetSelectedItem() + 90 ) );

			HideInspectors();
			break;

			case TOOL_NPC:
			{
				ClearObject();

				auto proxy = new CharacterProxy( _npcs[_npcCombo->GetSelectedItem()] );

				_proxies.insert_or_assign( proxy, proxy );

				_target->AddObjectAt( proxy, _position );

				InspectObject();
			}
			break;

			case TOOL_CHEST:
			{
				ClearObject();
				
				auto chest = new Chest();

				_chests.insert_or_assign( chest, chest );

				_target->AddObjectAt( chest, _position );

				InspectObject();
			}
			break;

			case TOOL_MOVE:
			{
				HideInspectors();
			}
			break;

			case TOOL_REMOVE:
			{
				ClearObject();

				HideInspectors();
			}
			break;

			case TOOL_INSPECT:
			{
				InspectObject();
			}
			break;
		}
	}
}

void MapCreationController::HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke )
{
	switch ( ke.code )
	{
		case sf::Keyboard::Return:
		_mapObserver->SetOffset( Position::ZERO );
		return;

		case sf::Keyboard::Up:
		_mapObserver->SetOffset( _mapObserver->GetOffset() + Position::UP );
		return;

		case sf::Keyboard::Down:
		_mapObserver->SetOffset( _mapObserver->GetOffset() + Position::DOWN );
		return;

		case sf::Keyboard::Left:
		_mapObserver->SetOffset( _mapObserver->GetOffset() + Position::LEFT );
		return;

		case sf::Keyboard::Right:
		_mapObserver->SetOffset( _mapObserver->GetOffset() + Position::RIGHT );
		return;
	}
}

void MapCreationController::ShowToolOptions( shared_ptr<sfg::Window> options )
{
	HideToolOptions();

	options->Show( true );
}

void MapCreationController::HideToolOptions()
{
	_tileOptionWindow->Show( false );
	_npcOptionWindow->Show( false );
}

void MapCreationController::ShowInspector( shared_ptr<sfg::Window> inspector )
{
	HideInspectors();

	inspector->Show( true );
}

void MapCreationController::HideInspectors()
{
	_npcInspectorWindow->Show( false );
	_chestInspectorWindow->Show( false );
}

void MapCreationController::HandleClickAddItem()
{
	auto chest = dynamic_cast<Chest const *>(_target->GetObjectAt( _position ));

	if ( chest )
	{
		_chests.at( chest )->AddItem( Data().items.GetContents()[_itemCombo->GetSelectedItem()] );
	}
}

void MapCreationController::HandleClickClearItems()
{
	if ( _target->Contains( _position ) )
	{
		auto chest = dynamic_cast<Chest const *>(_target->GetObjectAt( _position ));
	}
}

void MapCreationController::HandleSetAlignment()
{
	if ( _target->Contains( _position ) )
	{
		auto proxy = dynamic_cast<CharacterProxy const *>(_target->GetObjectAt( _position ));

		_proxies.at( proxy )->SetOverride( _alignmentCombo->GetSelectedItem() == 0 ? Alignment::Good : Alignment::Evil );
	}
}

void MapCreationController::ClearObject()
{
	auto object = _target->GetObjectAt( _position );

	if ( object )
	{
		_itemContainerObserver->Observe( nullptr );

		_target->RemoveObject( object );

		auto proxy = dynamic_cast<CharacterProxy const *>(_target->GetObjectAt( _position ));

		if ( proxy )
		{
			_proxies.erase( proxy );
		}
		else
		{
			auto chest = dynamic_cast<Chest const *>(_target->GetObjectAt( _position ));

			if ( chest )
			{
				_chests.erase( chest );
			}
		}

		delete object;
	}
}

void MapCreationController::InspectObject()
{
	if ( _target->IsObjectAt( _position ) )
	{
		_target->GetObjectAt( _position )->Accept( this );
	}
	else
	{
		HideInspectors();
	}
}
