#pragma once

/// Lightweight templated Observer base class.
template <class arg> class Observer
{
public:
	virtual ~Observer();

	void Observe( arg const * target );

protected:
	Observer();

	virtual void StartObserving() = 0;
	virtual void StopObserving() = 0;

	arg const * _target;
};

// Observer implementation

template<class arg>
inline Observer<arg>::Observer()
{}

template<class arg>
inline void Observer<arg>::Observe( arg const * target )
{
	// verify that the new target is not the same as the current target
	if ( target != _target )
	{
		// if this Observer was already observing something
		if ( _target )
		{
			// ensure that it is properly decoupled from the current target
			StopObserving();
		}

		// assign the new target
		_target = target;

		// verify that the new target is not the nullptr
		if ( _target )
		{
			// setup to Observe the new target
			StartObserving();
		}
	}
}

template<class arg>
inline Observer<arg>::~Observer()
{}