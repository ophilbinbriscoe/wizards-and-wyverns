#pragma once

// this is a defintion of the Visitor pattern to avoid having to use dynamic_cast when
// the full derrived type of an Inspectable needs to be known

class AbstractInspector;

class Inspectable
{
public:
	Inspectable();
	virtual ~Inspectable();

	virtual void Accept( AbstractInspector * inspector ) const = 0;
};

class MapObject;
class CharacterProxy;
class Character;
class Chest;

class AbstractInspector
{
public:
	AbstractInspector();
	virtual ~AbstractInspector();

	virtual void Inspect( MapObject const * object );
	virtual void Inspect( CharacterProxy const * proxy );
	virtual void Inspect( Character const * character );
	virtual void Inspect( Chest const * chest );
};