//! @file 
//! @brief Implementation file for the ReadersTestSuite class
//!

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>

#include "Reader.h"
#include "CampaignReader.h"
#include "MapReader.h"
#include "CharacterReader.h"
#include "ItemReader.h"

#include "Writer.h"
#include "CampaignWriter.h"
#include "MapWriter.h"
#include "CharacterWriter.h"
#include "ItemWriter.h"

#include "Campaign.h"
#include "Map.h"
#include "Character.h"
#include "CharacterBuilder.h"
#include "Item.h"

using namespace CppUnit;

//! Test Class for the Reader class
class ReadersTestSuite : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(ReadersTestSuite);
	CPPUNIT_TEST(CampaignRead);
	CPPUNIT_TEST(MapRead);
	CPPUNIT_TEST(CharacterRead);
	CPPUNIT_TEST(ItemRead);
	CPPUNIT_TEST_SUITE_END();
protected:
	void CampaignRead();
	void MapRead();
	void CharacterRead();
	void ItemRead();

};

//! cppunit test cases registration
CPPUNIT_TEST_SUITE_REGISTRATION(ReadersTestSuite);


//! Tests reading a campaign
//! Test Case: Reading a campaign should return the info of the one written in the file
//! Tested item: CampaignReader::CampaignReader()
void ReadersTestSuite::CampaignRead()
{
	Map * map = new Map(5, 5, "TestMap");
	CampaignNode * cn = new CampaignNode(map);
	Campaign * cm = new Campaign("TestCampaign", cn);
	Dataset<Map*> mapdata;
	Dataset<Campaign*> emptyCampaignData;
	vector<Campaign*> campVector;
	fstream file;
	mapdata.Add(map);
	CampaignWriter cw(file, "tests/campaignTest.dat", mapdata);

	cw.Write(cm);
	file.close();

	CampaignReader("tests/campaignTest.dat",  emptyCampaignData, mapdata );
	campVector = emptyCampaignData.GetContents();

	CPPUNIT_ASSERT(campVector.at(0)->GetName() == cm->GetName());
}
//! Tests reading a Map
//! Test Case: Reading a Map should return the info of the one written in the file
//! Tested item: MapReader::Read() && MapReader::GetResults()
void ReadersTestSuite::MapRead()
{
	Map * map = new Map(5, 5, "TestMap");
	Character * testchar;
	CharacterBuilder builder(true, 6, Class::Fighter, Alignment::Good, "Test", true);
	testchar = builder.Create();
	fstream file;
	Dataset<Character *> chardata;
	chardata.Add(testchar);
	Dataset<Item *> itemdata;
	Item * item = new Item("TestItem");
	ItemContainerWriter * icw = new ItemContainerWriter(itemdata);
	MapWriter mw(file, "tests/mapsTest.dat", chardata);

	mw.Write(map, icw);
	file.close();

	MapReader reader("tests/mapsTest.dat", chardata, itemdata);
	reader.Read();
	CPPUNIT_ASSERT(reader.GetResult()->GetName() == map->GetName());
}
//! Tests reading a Character
//! Test Case: Reading a Character should return the info of the one written in the file
//! Tested item: CharacterReader::Read() && CharacterReader::GetResults()
void ReadersTestSuite::CharacterRead()
{
	fstream file;
	Dataset<Item *> itemdata;
	Item * item = new Item("TestItem");
	itemdata.Add(item);
	Character * testchar;
	CharacterBuilder builder(true, 6, Class::Fighter, Alignment::Good, "TestChar", true);
	testchar = builder.Create();
	ItemContainerWriter * icw = new ItemContainerWriter(itemdata);
	CharacterWriter pw(file, "tests/charactersTest.dat");

	pw.Write("char", testchar, icw);
	file.close();

	CharacterReader reader("tests/charactersTest.dat", itemdata);
	reader.Read();

	CPPUNIT_ASSERT(reader.GetResult()->GetName() == testchar->GetName());
}

//! Tests reading a Item
//! Test Case: Reading a Item should return the info of the one written in the file
//! Tested item: ItemReader::Read() && ItemReader::GetResults()
void ReadersTestSuite::ItemRead()
{
	fstream file;
	ItemWriter er(file, "tests/itemsTest.dat");
	Item * item = new Item("TestItem");

	er.Write(item);
	file.close();

	ItemReader reader("tests/itemsTest.dat");
	reader.Read();
	CPPUNIT_ASSERT(reader.GetResult()->GetName() == item->GetName());
}