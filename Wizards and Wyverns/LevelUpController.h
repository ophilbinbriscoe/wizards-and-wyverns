#pragma once

#include "MenuController.h"
#include "UserFreeStrategy.h"

class LevelUpController : public MenuController
{
public:
	LevelUpController( Character const * character, UserFreeStrategy<StatType const *> * strategy );
	virtual ~LevelUpController();

protected:
	Character const * const _character;
	UserFreeStrategy<StatType const *> * const _strategy;

	bool CreateAbilityWidgets( StatType const * type, shared_ptr<sfg::Box> box, shared_ptr<sfg::Box> scores, shared_ptr<sfg::Box> buttons );

	void HandleSelect( StatType const * type );

	void HandleClickOK();

	void HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke );

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );
};

