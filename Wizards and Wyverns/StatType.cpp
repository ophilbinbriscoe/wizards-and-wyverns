#include "StatType.h"

StatType::StatType( const string & name, const int & min, const int & max, const unsigned int enumerator ) : StatType( name, "", min, max, enumerator )
{}

StatType::StatType( const string& name, const string& abbreviation, const int& min, const int& max, const unsigned int enumerator ) : DomainConcept( name, abbreviation, enumerator ), min( min ), max( max )
{}

StatType::~StatType()
{}

StatType::StatTypeData::StatTypeData() :
Strength(			new StatType( "Strength", "STR",	1, 20, 10 ) ),
Dexterity(			new StatType( "Dexterity", "DEX",	1, 20, 11 ) ),
Constitution(		new StatType( "Constitution", "CON",1, 20, 12 ) ),
Intelligence(		new StatType( "Intelligence", "INT",1, 20, 13 ) ),
Wisdom(				new StatType( "Wisdom",	"WIS",		1, 20, 14 ) ),
Charisma(			new StatType( "Charisma", "CHA",	1, 20, 15 ) ),
MaxHitPoints(		new StatType( "Max Hit Points",		0, INT_MAX ) ),
HitPoints(			new StatType( "Hit Points",	"HP",	0, INT_MAX ) ),
DamageTaken(		new StatType( "Damage Taken",		0, INT_MAX ) ),
AttackBonus(		new StatType( "Attack Bonus", "ATK",0, INT_MAX, 16 ) ),
DamageBonus(		new StatType( "Damage Bonus", "DMG",0, INT_MAX, 17 ) ),
ArmorClass(			new StatType( "Armor Class", "AC",	10, INT_MAX, 18 ) )
{}

StatType::StatTypeData::~StatTypeData()
{}

const StatType::StatTypeData & Stats()
{
	static StatType::StatTypeData* data = new StatType::StatTypeData();
	return *data;
}
