#include "AttacksObserver.h"

#include "StatFormatter.h"

view::AttacksObserver::AttacksObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	_box = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL );

	auto label = sfg::Label::Create( Stats().AttackBonus->name + string( ": " ) );

	label->SetAlignment( { 0.0f, 0.5f } );

	_labelAttacks = sfg::Label::Create( "" );

	_labelAttacks->SetAlignment( { 0.0f, 0.5f } );

	_box->Pack( label, false, false );
	_box->Pack( _labelAttacks, false, false );

	_container->Pack( _box, false, false );
}

view::AttacksObserver::~AttacksObserver()
{}

void view::AttacksObserver::StartObserving()
{
	_target->OnAttackBonusesModified.Subscribe( this, &AttacksObserver::HandleAttacksModified );

	_box->Show( true );

	SetAttacksLabel();
}

void view::AttacksObserver::StopObserving()
{	
	_target->OnAttackBonusesModified.Unsubscribe( this, &AttacksObserver::HandleAttacksModified );

	_box->Show( false );
}

void view::AttacksObserver::HandleAttacksModified( const vector<int>& bonuses )
{
	SetAttacksLabel();
}

void view::AttacksObserver::SetAttacksLabel()
{
	string text = "";

	for ( auto bonus : _target->GetAttackBonuses() )
	{
		text += BonusString( bonus ) + string( ", " );
	}

	text = text.substr( 0, max( text.length() - 2, (unsigned int) 0 ) );

	_labelAttacks->SetText( text );
}