#pragma once

#include "MenuController.h"

#include "ItemContainer.h"

class EquipMenuController : public MenuController, protected AbstractUser
{
public:
	EquipMenuController( ItemContainer const * container, SlotType const * type );
	virtual ~EquipMenuController();

	bool WasCancelled() const { return _cancel; }
	Equipment const * GetSelection() const { return _selection; }
	
protected:
	SlotType const * _type;

	virtual void Use( Item const * item );
	virtual void Use( Equipment const * equipment );
	
	shared_ptr<sfg::ComboBox> _combo;
	vector<Equipment const *> _options;
	bool _cancel;
	Equipment const * _selection;

	void HandleEquip();
	void HandleBack();
};

