#include <iostream>

using namespace std;

#include "Database.h"
#include "ApplicationController.h"

/// Application entry point
int main ()
{
	// set the seed for the pseudo-random number genetator
	srand( (unsigned int) time( NULL ) );

	// make sure that StatType flyweights are initialized first
	Stats();

	// followed by SlotType flyweights
	Slots();

	// read serialized data
	Data().ReadAll();

	// create the ApplicationController (main loop)
	ApplicationController* controller = new ApplicationController();

	// serialize data
	Data().SaveAll();

	// wait for user input before terminating
	cin.ignore();

	return 0;
};