#pragma once

#include "Observer.h"
#include "Character.h"
#include "View.h"

namespace view
{
	class ArmorClassObserver : public View, public Observer<Character>
	{
	public:
		ArmorClassObserver( shared_ptr<sfg::Box> container );
		virtual ~ArmorClassObserver();

	private:
		virtual void StartObserving();
		virtual void StopObserving();

		void HandleArmorClassModified( DerivedStat const * const stat );

		void SetLabel();

		shared_ptr<sfg::Label> _label;
	};
}