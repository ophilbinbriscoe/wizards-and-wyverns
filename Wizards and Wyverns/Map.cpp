#include "Map.h"

#include <set>

Map::Map( Map const & map ) : Resource( map ), Grid( map ), _objects( map._objects )
{}

Map::Map( const int& width, const int& height, const string& name ) : Resource( name ), Grid( width, height, TileType::Free )
{}

Map::~Map()
{}

/// Reused logic for flood-fill search
bool Map::ExpandTowards( const Position& position, queue<Position>* queue, bool* visited ) const
{
	int i = PositionToIndex( position );

	// only process unvisited cells
	if ( !visited[i] )
	{
		// get the cell at x, y
		TileType const * tile = GetTileAt( position );

		// goal case
		if ( tile == TileType::Exit )
		{
			return true;
		}

		// mark the cell as visited
		visited[i] = true;

		// only expand towards passable cells (monsters can be killed, friendlies can be walked past)
		if ( tile == TileType::Free && (!IsObjectAt( position ) || !GetObjectAt( position )->IsFixed()) )
		{
			queue->push( position );
		}
	}

	return false;
}

/// Implementation of the map verification
/// @return bool value, true of the map is valid (there is at least one clear path between the mandatory begin and end cell). 
bool Map::Validate() const
{
	if ( !Resource::Validate() )
	{
		return false;
	}

	vector<Position> entrances;

	// iterate over the Map
	for ( int y = 0; y < _height; y++ )
	{
		for ( int x = 0; x < _width; x++ )
		{
			// if the tile is an entrance
			if ( GetTileAt( Position( x, y ) ) == TileType::Entrance )
			{
				// add its position to the list of entrance positions
				entrances.push_back( Position( x, y ) );
			}
		}
	}

	// perform a flood-fill search from each entrance
	for ( auto entrance : entrances )
	{
		// queue of cells to expand
		queue<Position> queue;

		// visited cells
		bool* visited = new bool[_width * _height];

		for ( int i = 0; i < _width * _height; i++ )
		{
			visited[i] = false;
		}

		// start search from entrance
		queue.push( entrance );
		visited[PositionToIndex( entrance )] = true;

		// bounds
		int xmin = 0;
		int ymin = 0;
		int xmax = _width - 1;
		int ymax = _height - 1;

		// exhaust queue
		while ( queue.size() > 0 )
		{
			// dequeue the next cell to expand
			Position point = queue.front();
			queue.pop();

			// shorthand
			int x = point.x;
			int y = point.y;

			// neighbours
			int left = x - 1;
			int right = x + 1;
			int up = y + 1;
			int down = y - 1;

			// check if any neighbouring cells are an exit
			if ( (left	>= xmin && ExpandTowards( Position( left,  y ), &queue, visited )) ||
				 (right <= xmax && ExpandTowards( Position( right, y ), &queue, visited )) ||
				 (up	<= ymax && ExpandTowards( Position( x, up   ), &queue, visited )) ||
				 (down	>= ymin && ExpandTowards( Position( x, down ), &queue, visited )) )
			{
				// clean up
				delete visited;

				return true;
			}
		}

		// clean up
		delete visited;
	}

	return false;
}

/// Set the TileType at a position
void Map::SetTileAt( const Position& position, TileType const * const type )
{
	// set the tile to the new type
	Set( position, type );

	// notify observers of a change at position
	OnTileModified( position );
}

/// Query the TileType at a position
TileType const * Map::GetTileAt( const Position& position ) const
{
	return Get( position );
}

/// Add an object to the Map
void Map::AddObjectAt( MapObject const * object, const Position & position )
{
	_objects.insert( pair<MapObject const *, Position>( object, position ) );

	OnAdd( object );
}

/// Set an object's position
void Map::SetPosition( MapObject const * object, const Position & position )
{
	if ( position != _objects.at( object ) )
	{
		_objects.at( object ) = position;

		OnSetPosition( object );
	}
}

/// Remove an object from the Map
void Map::RemoveObject( MapObject const * object )
{
	_objects.erase( object );

	OnRemove( object );
}

/// Query an object's position
const Position & Map::GetPosition( MapObject const * object ) const
{
	return _objects.at( object );
}

/// Query the presence of an object on the Map
bool Map::Contains( MapObject const * object ) const
{
	return _objects.find( object ) != _objects.end();
}

bool Map::Contains( const Position & position ) const
{
	return Grid::Contains( position );
}

const map<MapObject const *, Position>& Map::GetObjects() const
{
	return _objects;
}

/// Query the presence of an object at a position
bool Map::IsObjectAt( const Position& position ) const
{
	return GetObjectAt( position ) != nullptr;
}

/// Query the MapObject at a position
MapObject const * Map::GetObjectAt( const Position& position ) const
{
	for ( auto pair : _objects )
	{
		if ( pair.second == position )
		{
			return pair.first;
		}
	}

	return nullptr;
}

/// Query whether or not a position is conceptually occupied
bool Map::IsOccupied( const Position & position ) const
{
	return (GetTileAt( position ) == TileType::Wall) || IsObjectAt( position );
}

/// Query whether an object is adjacent to a position
bool Map::IsAdjacent( MapObject const * object, const Position & position ) const
{
	auto delta = _objects.at( object ) - position;

	return abs( delta.x ) <= 1 && abs( delta.y ) <= 1;
}

/// Count the number of tiles of Entrance type
int Map::GetEntranceCount() const
{
	int count = 0;

	for ( int y = 0; y < _height; y++ )
	{
		for ( int x = 0; x < _width; x++ )
		{
			if ( Get( { x, y } ) == TileType::Entrance )
			{
				count++;
			}
		}
	}

	return count;
}

/// Return the index'th Entrance in a row-major search from top left to bottom right
Position Map::GetEntrance( int index ) const
{
	int count = 0;

	Position position;

	for ( int y = 0; y < _height; y++ )
	{
		for ( int x = 0; x < _width; x++ )
		{
			position = Position( x, y );

			if ( GetTileAt( position ) == TileType::Entrance )
			{
				if ( count++ <= index )
				{
					return position;
				}
			}
		}
	}

	return position;
}

void Map::operator=( const Map & map )
{
	SetName( map.GetName() );

	this->Grid::operator=( map );

	_objects = map._objects;
}

