#pragma once

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>

#include "Bonus.h"
#include "Modifier.h"
#include "Score.h"
#include "DerivedStat.h"
#include "CharacterBuilder.h"

using namespace CppUnit;

static class ValuesTestSuite : public CppUnit::TestFixture {
public:
	CPPUNIT_TEST_SUITE(ValuesTestSuite);

	CPPUNIT_TEST(testAtkBonus); 
	CPPUNIT_TEST(testModifiers); 
	CPPUNIT_TEST(testScores); 
	CPPUNIT_TEST(testHitPoints);

	CPPUNIT_TEST_SUITE_END();

protected:
	void testAtkBonus();
	void testModifiers();
	void testScores();
	void testHitPoints();
};

//! cppunit test cases registration
CPPUNIT_TEST_SUITE_REGISTRATION(ValuesTestSuite);

//! Tests creating a character, and making sure its attack bonuses are right
//! Test Case: A new character should have its first attack bonus equal to its level
//! Tested item: CharacterBuilder::CharacterBuilder() && Character::GetAttackBonuses() && Character::GetAbilityModifiers()
void ValuesTestSuite::testAtkBonus()
{
	int level = 6;
	int st, de, co, in, wi, ch;
	int modst;
	vector<int> atkBonuses;
	string name = "TestChar";
	Character * testchar;
	// setup a CharacterBuilder with the user-defined parameters
	CharacterBuilder builder(true, level, Class::Fighter, Alignment::Good, name, true);
	testchar = builder.Create();
	atkBonuses = testchar->GetAttackBonuses();
	modst = testchar->GetAbilityModifier(Stats().Strength);

	CPPUNIT_ASSERT(atkBonuses.at(0) == level + modst );

}
//! Tests creating a character, and making sure its ability modifiers
//! Test Case: A new character's ability modifiers should be equal to (score / 2) - 5)
//! Tested item: CharacterBuilder::CharacterBuilder() && Character::GetAbilityScore() && Character::GetAbilityModifiers()
void ValuesTestSuite::testModifiers()
{
	int level = 6;
	int st, de, co, in, wi, ch;
	int modst, modde, modco, modin, modwi, modch;
	bool testmod = false;
	string name = "TestChar";
	Character * testchar;
	// setup a CharacterBuilder with the user-defined parameters
	CharacterBuilder builder(true, level, Class::Fighter, Alignment::Good, name, true);
	testchar = builder.Create();
	st = testchar->GetAbilityScore(Stats().Strength);
	de = testchar->GetAbilityScore(Stats().Dexterity);
	co = testchar->GetAbilityScore(Stats().Constitution);
	in = testchar->GetAbilityScore(Stats().Intelligence);
	wi = testchar->GetAbilityScore(Stats().Wisdom);
	ch = testchar->GetAbilityScore(Stats().Charisma);

	modst = testchar->GetAbilityModifier(Stats().Strength);
	modde = testchar->GetAbilityModifier(Stats().Dexterity);
	modco = testchar->GetAbilityModifier(Stats().Constitution);
	modin = testchar->GetAbilityModifier(Stats().Intelligence);
	modwi = testchar->GetAbilityModifier(Stats().Wisdom);
	modch = testchar->GetAbilityModifier(Stats().Charisma);

	if (modst == (st / 2) - 5)
		testmod = true;
	if (modde == (de / 2) - 5)
		testmod = true;
	if (modco == (co / 2) - 5)
		testmod = true;
	if (modin == (in / 2) - 5)
		testmod = true;
	if (modwi == (wi / 2) - 5)
		testmod = true;
	if (modch == (ch / 2) - 5)
		testmod = true;

	CPPUNIT_ASSERT(testmod);
}
//! Tests creating a character, and making sure its ability scores are within 3 and 18
//! Test Case: A new character's ability scores should be between 3 and 18
//! Tested item: CharacterBuilder::CharacterBuilder() && Character::GetAbilityScore()
void ValuesTestSuite::testScores()
{
	int level = 6;
	int st, de, co, in, wi, ch;
	string name = "TestChar";
	Character * testchar;
	// setup a CharacterBuilder with the user-defined parameters
	CharacterBuilder builder(true, level, Class::Fighter, Alignment::Good, name, true);
	testchar = builder.Create();
	st = testchar->GetAbilityScore(Stats().Strength);
	de = testchar->GetAbilityScore(Stats().Dexterity);
	co = testchar->GetAbilityScore(Stats().Constitution);
	in = testchar->GetAbilityScore(Stats().Intelligence);
	wi = testchar->GetAbilityScore(Stats().Wisdom);
	ch = testchar->GetAbilityScore(Stats().Charisma);

	CPPUNIT_ASSERT(st >= 3 && st < 18 &&
		de >= 3 && de < 18 &&
		co >= 3 && co < 18 &&
		in >= 3 && in < 18 &&
		wi >= 3 && wi < 18 &&
		ch >= 3 && ch < 18);
}
//! Tests creating a character, and hitting it
//! Test Case: A character's HitPoints should go down by taking damage
//! Tested item: CharacterBuilder::CharacterBuilder() && Character::Hit() && Character::GetStat()
void ValuesTestSuite::testHitPoints()
{
	int level = 6;
	int st, de, co, in, wi, ch;
	string name = "TestChar";
	Character * testchar;
	// setup a CharacterBuilder with the user-defined parameters
	CharacterBuilder builder(true, level, Class::Fighter, Alignment::Good, name, true);
	testchar = builder.Create();

	testchar->Hit(10);
	CPPUNIT_ASSERT(testchar->GetStat(Stats().MaxHitPoints) == testchar->GetStat(Stats().HitPoints) + 10);
}