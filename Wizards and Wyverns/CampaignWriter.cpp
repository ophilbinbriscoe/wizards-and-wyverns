#include "CampaignWriter.h"

#include <vector>
#include <stdio.h>
#include <string>
#include <cstring>

#include <algorithm>


using namespace std;

#pragma warning(disable:4996)

CampaignWriter::CampaignWriter( fstream& file, const char* filename, const Dataset<Map*> & maps ) : Writer( file, filename )
{
	for ( unsigned int i = 0; i < maps.GetCount(); i++ )
	{
		_order.insert( pair<Map *, unsigned int>( maps.GetContents()[i], i ) );
	}
}

CampaignWriter::~CampaignWriter()
{}

void CampaignWriter::Write( const Campaign * campaign )
{
	string maps = "";

	CampaignNode * node = campaign->GetStart();

	unsigned int n = 0;

	do
	{
		n++;
		maps += to_string( _order.at( node->GetMap() ) ) + string( " " );
		node = node->GetNext();
	}
	while ( node );

	// write the header, number of nodes, map order at each node, and name
	_file << "campaign: " << n << ' ' << maps << campaign->GetName() << endl;
}
