#pragma once

#include "Equipment.h"
#include "WeaponType.h"

class Weapon : public Equipment
{
public:
	Weapon( const string& name, WeaponType const * type, const Enchantments& enchantments );
	~Weapon();

	WeaponType const * const type;
};

