#include "View.h"

View::View( shared_ptr<sfg::Box> container ) : _container( container )
{}

View::~View()
{}

void View::SetContainer( shared_ptr<sfg::Box> container )
{
	_container = container;
}

shared_ptr<sfg::Box> View::GetContainer() const
{
	return _container;
}
