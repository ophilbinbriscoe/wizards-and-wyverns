#include "TraitsObserver.h"

#include "StatFormatter.h"

view::TraitsObserver::TraitsObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	_labelClassLevel = sfg::Label::Create( "" );
	
	_labelClassLevel->SetAlignment( { 0.0f, 0.0f } );

	_container->Pack( _labelClassLevel, false, false );
}

view::TraitsObserver::~TraitsObserver()
{}

void view::TraitsObserver::StartObserving()
{	
	_target->OnLevelUp.Subscribe( this, &TraitsObserver::HandleLevelModified );

	_labelClassLevel->Show( true );

	SetClassLevelLabel();
}

void view::TraitsObserver::StopObserving()
{
	_target->OnLevelUp.Unsubscribe( this, &TraitsObserver::HandleLevelModified );
	
	_labelClassLevel->Show( false );

	_labelClassLevel->SetText( "" );
}

void view::TraitsObserver::HandleLevelModified( Character const * character )
{
	SetClassLevelLabel();
}

void view::TraitsObserver::SetClassLevelLabel()
{
	_labelClassLevel->SetText( string( "Level " ) + to_string( _target->GetLevel() ) + string( " " ) + _target->GetClass()->name );
}
