#include "MapAdjuster.h"

MapAdjuster::MapAdjuster()
{}

MapAdjuster::~MapAdjuster()
{}

Map * MapAdjuster::Adjust( Map const * map )
{
	// create an empty Map with the same dimensions and name as the original Map
	_adjusted = new Map( map->GetWidth(), map->GetHeight(), map->GetName() );

	// iterate over the original Map's tiles
	for ( int x = 0; x < map->GetWidth(); x++ )
	{
		for ( int y = 0; y < map->GetHeight(); y++ )
		{
			// copy the tile from the original Map to the new Map
			_adjusted->SetTileAt( { x, y }, map->GetTileAt( { x, y } ) );
		}
	}
	
	for ( auto pair : map->GetObjects() )
	{
		_position = pair.second;

		pair.first->Accept( this );
	}

	return _adjusted;
}

void MapAdjuster::Inspect( CharacterProxy const * proxy )
{
	Adjust( proxy, _position, _adjusted );
}

void MapAdjuster::Inspect( Chest const * chest )
{
	Adjust( chest, _position, _adjusted );
}
