#pragma once

#include "Character.h"
#include "ItemObserver.h"
#include "View.h"

namespace view
{
	class HitPointObserver : public View, public Observer<Character>
	{
	public:
		HitPointObserver( shared_ptr<sfg::Box> container );
		virtual ~HitPointObserver();

	private:
		virtual void StartObserving();
		virtual void StopObserving();

		void HandleHitPointsModified( DerivedStat const * const stat );

		void SetHitPointBar();

		shared_ptr<sfg::ProgressBar> _barHP;
		shared_ptr<sfg::Label> _labelHP;
	};
}