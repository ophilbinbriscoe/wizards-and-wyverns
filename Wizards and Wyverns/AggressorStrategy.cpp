#include "AggressorStrategy.h"

AggressorStrategy::AggressorStrategy( Character const * target ) : _target( target )
{}

AggressorStrategy::~AggressorStrategy()
{}

void AggressorStrategy::RequestDecision( Character const * character, Map const * map )
{
	// check if Character is adjacent to target
	if ( map->IsAdjacent( character, map->GetPosition( _target ) ) )
	{
		_decision = _target;
		_yield = false;
	}
	else
	{
		_yield = true;
	}

	_ready = true;
}
