#pragma once

#include "MapObject.h"
#include "ItemContainer.h"

class Chest : public MapObject, public ItemContainer
{
public:
	// Default constructor
	Chest();

	// Copy constructor
	Chest( Chest const * const chest );

	~Chest();

	virtual bool IsFixed() const { return true; }

	virtual void Accept( AbstractInteractor * interactor );
	virtual void Accept( AbstractInspector * inspector ) const;
};

