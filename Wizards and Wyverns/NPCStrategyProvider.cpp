#include "NPCStrategyProvider.h"

NPCStrategyProvider::NPCStrategyProvider( BarrierStrategy<Position> * movement, BarrierStrategy<MapObject const *> * aggressor, BarrierStrategy<MapObject const *> * friendly, FreeStrategy<Item const *> * equip, FreeStrategy<SlotType const *> * unequip, FreeStrategy<StatType const *> * level ) : _movement( movement ), _aggressor( aggressor ), _friendly( friendly ), _equip( equip ), _unequip( unequip ), _level( level )
{}

NPCStrategyProvider::~NPCStrategyProvider()
{}

BarrierStrategy<Position>* NPCStrategyProvider::GetMovementStrategy( Character const * character ) const
{
	return _movement;
}

BarrierStrategy<MapObject const*>* NPCStrategyProvider::GetInteractionStrategy( Character const * character ) const
{
	if ( character->GetAlignment() == Alignment::Evil )
	{
		return _aggressor;
	}

	return _friendly;
}

FreeStrategy<Item const*>* NPCStrategyProvider::GetEquipStrategy( Character const * character ) const
{
	return _equip;
}

FreeStrategy<SlotType const*>* NPCStrategyProvider::GetUnequipStrategy( Character const * character ) const
{
	return _unequip;
}

FreeStrategy<StatType const*>* NPCStrategyProvider::GetLevelStrategy( Character const * character ) const
{
	return _level;
}
