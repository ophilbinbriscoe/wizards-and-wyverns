#include "MenuController.h"

MenuController::MenuController()
{
	_horizontal = sfg::Alignment::Create();

	_container = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL );

	_container->Pack( _horizontal, true, true );

	_horizontal->SetScale( { 0.0f, 0.0f } );
	_horizontal->SetAlignment( { 0.5f, 0.5f } );

	_vertical = sfg::Alignment::Create();

	_box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 0.0f );

	_box->Pack( _vertical );

	_menu = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 10.0f );
}

MenuController::~MenuController()
{}

void MenuController::Finalize()
{
	_vertical->Add( _menu );

	_horizontal->Add( _box );

	_window->Add( _container );
}

