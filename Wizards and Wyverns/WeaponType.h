#pragma once

#include <vector>

#include "DomainConcept.h"

using namespace std;

class WeaponType : public DomainConcept
{
public:

	// simple melee weapon types

	static WeaponType const * const Club;
	static WeaponType const * const Dagger;
	static WeaponType const * const Greatclub;
	static WeaponType const * const Handaxe;
	static WeaponType const * const Javelin;
	static WeaponType const * const LightHammer;
	static WeaponType const * const Mace;
	static WeaponType const * const Quarterstaff;
	static WeaponType const * const Sickle;
	static WeaponType const * const Spear;

	// martial melee weapon types

	static WeaponType const * const Battleaxe;
	static WeaponType const * const Flail;
	static WeaponType const * const Glaive;
	static WeaponType const * const Greataxe;
	static WeaponType const * const Greatsword;
	static WeaponType const * const Halberd;
	static WeaponType const * const Lance;
	static WeaponType const * const Longsword;
	static WeaponType const * const Maul;
	static WeaponType const * const Morningstar;
	static WeaponType const * const Pike;
	static WeaponType const * const Rapier;
	static WeaponType const * const Scimitar;
	static WeaponType const * const Shortsword;
	static WeaponType const * const Trident;
	static WeaponType const * const WarPick;
	static WeaponType const * const Warhammer;
	static WeaponType const * const Whip;

	//simple ranged weapon types
	static WeaponType const * const CrossbowLight;
	static WeaponType const * const Dart;
	static WeaponType const * const Shortbow;
	static WeaponType const * const Sling;

	// martial ranged weapon types
	static WeaponType const * const Blowgun;
	static WeaponType const * const CrossbowHand;
	static WeaponType const * const CrossbowHeavy;
	static WeaponType const * const Longbow;
	//

	const string damage;

private:
	WeaponType( const string& name, const int& enumerator, const string& damage );
	~WeaponType();
};