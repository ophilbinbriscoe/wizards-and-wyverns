#pragma once

#include "Map.h"

/// A linked-list node holding a pointer to a MAp.
class CampaignNode
{
public:
	/// Parametric constructor.
	CampaignNode( Map const * map = nullptr );

	/// Destructor.
	~CampaignNode();

	/// Set the next CampaignNode.
	void SetNext( CampaignNode * node );

	/// Query the next CampaignNode.
	CampaignNode* GetNext() const;

	/// Set the Map pointed to by this CampaignNode.
	void SetMap( Map const * map );

	/// Query the Map pointed to by this CampaignNode.
	Map const * GetMap() const;

	/// Event fired when this CampaignNode is assigned a new Map. 
	Event<Map const *> OnSetMap;

	/// Event fired when this CampaignNode's successor is modified.
	Event<CampaignNode const *> OnSetNext;

private:
	Map const * _map;
	CampaignNode * _next;
};

/// Wraps the first node in a linked-list of CampaignNodes.
class Campaign : public Resource
{
public:
	/// Parametric constructor.
	Campaign( const string& name = "", CampaignNode * start = nullptr );

	/// Destructor.
	~Campaign();

	/// Assign this a new value to this Campaign's start CampaignNode pointer.
	void SetStart( CampaignNode * node );

	/// Query the CampaignNode pointed to by this Campaign.
	CampaignNode* GetStart() const;

	/// Event fired when this Campaign's start CampaignNode pointer is assigned a new value.
	Event<CampaignNode *> OnSetStart;

private:
	CampaignNode * _start;
};