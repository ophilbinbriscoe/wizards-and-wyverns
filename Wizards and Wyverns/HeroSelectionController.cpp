#include "HeroSelectionController.h"

#include "CampaignSelectionController.h"

#include "Database.h"

bool Playable( Character * character )
{
	return character->IsPlayable();
}

HeroSelectionController::HeroSelectionController( const Dataset<Character *>& dataset ) : SelectionMenuController( dataset, &Playable )
{
	PackOptionButtons( "Playable Characters", "No playable characters found." );
	Finalize();
}

HeroSelectionController::~HeroSelectionController()
{}

void HeroSelectionController::HandleSelect( Character * character )
{
	Push( new CampaignSelectionController( Data().campaigns, character ) );
}