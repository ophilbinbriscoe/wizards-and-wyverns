#include "CharacterCreationController.h"
#include "Database.h"
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/Renderers.hpp>

#include "CharacterBuilder.h"

CharacterCreationController::CharacterCreationController()
{
	auto topBox = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL );
	auto midBox = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL );
	auto lowBox = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL );
	auto masterBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL );

	playable_button = sfg::CheckButton::Create( "Playable" );
	playable_button->SetActive( true );
	playable_button->GetSignal( sfg::ToggleButton::OnToggle ).Connect( bind( &CharacterCreationController::HandleTogglePlayable, this ) );

	auto createButton = sfg::Button::Create();

	createButton->SetLabel("Create");
	createButton->GetSignal( sfg::Widget::OnLeftClick ).Connect( std::bind( &CharacterCreationController::CreateCharacter, this ) );

	auto name_label = sfg::Label::Create( "Name" );
	auto class_label = sfg::Label::Create( "Class" );
	top_mid_label = sfg::Label::Create( "Level" );

	success_label = sfg::Label::Create( "" );

	topBox->Pack( name_label );
	topBox->Pack( top_mid_label );
	topBox->Pack( class_label );

	name_entry = sfg::Entry::Create();

	class_combo = sfg::ComboBox::Create();
	class_combo->AppendItem( Class::Fighter->name );
	class_combo->SelectItem( 0 );

	alignment_combo = sfg::ComboBox::Create();
	alignment_combo->AppendItem( Alignment::Good->name );
	alignment_combo->AppendItem( Alignment::Evil->name );
	alignment_combo->SelectItem( 1 );

	level_entry = sfg::Entry::Create();

	name_entry->SetRequisition( sf::Vector2f( 100.0f, 0.f ) );
	alignment_combo->SetRequisition( sf::Vector2f( 100.0f, 0.f ) );
	level_entry->SetRequisition( sf::Vector2f( 100.0f, 0.f ) );
	class_combo->SetRequisition( sf::Vector2f( 100.0f, 0.f ) );

	midBox->Pack( name_entry );
	midBox->Pack( alignment_combo );
	midBox->Pack( level_entry );
	midBox->Pack( class_combo );

	lowBox->Pack( playable_button );

	masterBox->Pack( topBox );
	masterBox->Pack( midBox );
	masterBox->Pack( lowBox );
	masterBox->Pack( createButton );
	masterBox->Pack( success_label );

	auto buttonQuit = sfg::Button::Create( "Quit" );
	buttonQuit->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CharacterCreationController::HandleClickQuit, this ) );

	masterBox->SetSpacing( 5.f );

	_menu->Pack( masterBox );
	_menu->Pack( buttonQuit );

	Finalize();

	alignment_combo->Show( false );
}

CharacterCreationController::~CharacterCreationController()
{}
//! Returns to the previous menu
void CharacterCreationController::HandleClickQuit()
{
	Pop(this);
}
//! Defines if the character is playable or not
void CharacterCreationController::HandleTogglePlayable()
{
	if ( playable_button->IsActive() )
	{
		alignment_combo->Show( false );
		level_entry->Show( true );
		top_mid_label->SetText( "Level" );
	}
	else
	{
		alignment_combo->Show( true );
		level_entry->Show( false );
		top_mid_label->SetText( "Alignment" );
	}
}
//! Creates a new character
void CharacterCreationController::CreateCharacter()
{
	string lvl = level_entry->GetText();
	Class const * type = DomainConcept::Get<Class>( class_combo->GetSelectedItem() );
	string name = name_entry->GetText();
	bool playable = playable_button->IsActive();
	Alignment const * alignment = DomainConcept::Get<Alignment>( alignment_combo->GetSelectedItem() + 80 );
	int level = 0;
	char * end;
	 //If the character is playable, it initializes it to the proper level
	if ( playable )
	{
		if ( lvl != "" )
		{
			level = std::strtol( lvl.c_str(), &end, 10 );
		}
	}
	else
	{
		level = 1;
	}

	if ( level > 0 && (type == Class::Fighter) && (name !=""))
	{
		// setup a CharacterBuilder with the user-defined parameters
		CharacterBuilder builder( playable, level, type, playable ? Alignment::Good : alignment, name, true );

		// create and add character to database
		Data().characters.Add( builder.Create() );

		success_label->SetText("Success!");
		level_entry->SetText("");
		name_entry->SetText("");
		class_combo->SelectItem( 0 );
	}
	else
	{
		//Returns the right error message for invalid inputs
		if ( name == "")
		{
			success_label->SetText( "Character needs to have a name" );
			name_entry->SetText("");
		}
		else if (type != Class::Fighter)
		{
			success_label->SetText("Class must be Fighter");
		}
		else if ( level < 1 )
		{
			success_label->SetText( "Invalid Level, level has to be higher than 1" );
			level_entry->SetText("");
		}
	}
}

