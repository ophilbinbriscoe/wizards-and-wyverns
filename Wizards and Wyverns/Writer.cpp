#include "Writer.h"

#pragma warning(disable:4996)

#include <iostream>

using namespace std;

Writer::Writer( std::fstream& file, const char * filepath ) : _file( file )
{
	file.open( filepath, std::ofstream::out | std::ofstream::trunc );

	if ( !file )
	{
		file.clear();
		file.open( filepath, std::ofstream::out );
		
		if ( !file )
		{
			cout << "could not create file";
		}

		file.close();
		file.open( filepath, std::ofstream::out );

		if ( !file )
		{
			cout << "could not access file";
		}
	}
}

Writer::~Writer()
{}