#pragma once

#include "Writer.h"
#include "Item.h"

class NameWriter : public Writer
{
public:
	NameWriter( fstream& file, const char * filepath );
	virtual ~NameWriter();

	void Write( fstream& file, const Resource * item );
};

