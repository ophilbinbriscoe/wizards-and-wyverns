#include "Resource.h"

Resource::Resource( const Resource & resource ) : _name( resource._name )
{}

Resource::Resource( const string& name ) : _name( name )
{}

Resource::~Resource()
{}

void Resource::SetName( const string & name )
{
	_name = name;
	OnRename( this );
}
