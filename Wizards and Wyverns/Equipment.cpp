#include "Equipment.h"

Equipment::Equipment( const string& name, SlotType const * const slot ) : Item( name ), slot( slot )
{}

Equipment::Equipment( const string& name, SlotType const * const slot, const Enchantments & enchantments ) : Item( name ), slot( slot )
{
	for ( auto enchantment : enchantments )
	{
		Enchant( enchantment );
	}
}

Equipment::~Equipment()
{}

unsigned int Equipment::IncreaseTo( StatType const * const type ) const
{
	unsigned int accumulator = 0;

	for ( auto enchantment : _enchantments )
	{
		if ( enchantment.type == type )
		{
			accumulator += enchantment.bonus;
		}
	}

	return accumulator;
}

void Equipment::Accept( AbstractUser * user ) const
{
	user->Use( this );
}

/// Helper method. Validates an Enchantment and adds it if it passes validation.
bool Equipment::Enchant( const Enchantment& enchantment )
{
	// validate the Enchantment's target Stats against this Equipment's SlotType
	if ( slot->SupportsEnchantmentOf( enchantment.type ) )
	{
		// add the enchantment
		_enchantments.push_back( enchantment );

		return true;
	}

	return false;
}

const vector<Enchantment>& Equipment::GetEnchantments() const
{
	return _enchantments;
}


