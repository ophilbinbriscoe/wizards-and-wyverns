#pragma once

#include "View.h"

const int APPLICATION_WINDOW_WIDTHi = 1200;
const int APPLICATION_WINDOW_HEIGHTi = 600;

const float APPLICATION_WINDOW_WIDTHf = (float) APPLICATION_WINDOW_WIDTHi;
const float APPLICATION_WINDOW_HEIGHTf = (float) APPLICATION_WINDOW_HEIGHTi;

class Controller
{
public:
	Controller();
	virtual ~Controller();

	Event<Controller*> Push;
	Event<Controller*> Pop;
	Event<> Collapse;

	virtual shared_ptr<sfg::Window> GetWindow() const { return _window; }

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );

	virtual void Update( const float & time );

	bool popped;

protected:
	shared_ptr<sfg::Window> _window;
};

