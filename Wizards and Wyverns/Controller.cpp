#include "Controller.h"

Controller::Controller() : _window( sfg::Window::Create( sfg::Window::NO_STYLE ) )
{
	_window->SetRequisition( { APPLICATION_WINDOW_WIDTHf, APPLICATION_WINDOW_HEIGHTf } );
}

Controller::~Controller()
{}

void Controller::SubscribeInputHandlers( view::Window * window )
{}

void Controller::UnsubscribeInputHandlers( view::Window * window )
{}

void Controller::Update( const float & time )
{}
