#include "CampaignCreationController.h"

#include "MessageController.h"

#include "Database.h"

CampaignCreationController::CampaignCreationController()
{
	_labelName = sfg::Label::Create( "" );

	_boxMaps = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 1.0f );

	_entryName = sfg::Entry::Create( "" );

	_entryName->SetRequisition( { 100.0f, 0.0f } );

	_buttonCreate = sfg::Button::Create( "Create" );
	_buttonCreate->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CampaignCreationController::HandleClickCreate, this ) );

	_comboMaps = sfg::ComboBox::Create();
	_buttonAdd = sfg::Button::Create( "Append" );
	_buttonAdd->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CampaignCreationController::HandleClickAppend, this ) );

	for ( auto map : Data().maps.GetContents() )
	{
		_comboMaps->AppendItem( map->GetName() );
	}

	auto boxMaps = sfg::Box::Create();

	boxMaps->Pack( _comboMaps );
	boxMaps->Pack( _buttonAdd );

	auto boxMeta = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 10.0f );

	_buttonSave = sfg::Button::Create( "Save" );
	_buttonBack = sfg::Button::Create( "Back" );

	_buttonSave->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CampaignCreationController::HandleClickSave, this ) );
	_buttonBack->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CampaignCreationController::HandleClickClear, this ) );

	boxMeta->Pack( _buttonSave );
	boxMeta->Pack( _buttonBack );

	_buttonQuit = sfg::Button::Create( "Quit" );
	_buttonQuit->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CampaignCreationController::HandleClickQuit, this ) );

	_menu->Pack( _labelName, false, false );
	_menu->Pack( _boxMaps, false, false );
	_menu->Pack( _entryName, false, false );
	_menu->Pack( _buttonCreate, false, false );
	_menu->Pack( boxMaps, false, false );
	_menu->Pack( boxMeta, false, false );
	_menu->Pack( _buttonQuit, false, false );

	Finalize();

	Reset();
}

CampaignCreationController::~CampaignCreationController()
{
	if ( _campaign )
	{
		delete _campaign;
	}
}

void CampaignCreationController::Reset()
{
	_labelName->SetText( "Name" );

	for ( auto child : _boxMaps->GetChildren() )
	{
		child->Show( false );
	}

	_boxMaps->RemoveAll();
	_boxMaps->Show( false );

	_entryName->SetText( "" );
	_entryName->Show( true );
	_buttonCreate->Show( true );

	_buttonBack->Show( false );
	_buttonSave->Show( false );

	_buttonAdd->GetParent()->Show( false );
}

void CampaignCreationController::HandleClickCreate()
{
	if ( _entryName->GetText().isEmpty() )
	{
		Push( new MessageController( "You must enter a name.", "Back" ) );
	}
	else
	{
		_campaign = new Campaign( _entryName->GetText() );

		_labelName->SetText( _campaign->GetName() );

		_boxMaps->Show( true );

		_entryName->Show( false );
		_buttonCreate->Show( false );

		_buttonBack->Show( true );
		_buttonSave->Show( true );

		_buttonAdd->GetParent()->Show( true );
	}
}

void CampaignCreationController::HandleClickAppend()
{
	CampaignNode * node = new CampaignNode( Data().maps.GetContents()[_comboMaps->GetSelectedItem()] );

	if ( _campaign->GetStart() )
	{
		_last->SetNext( node );
	}
	else
	{
		_campaign->SetStart( node );
	}

	auto label = sfg::Label::Create( to_string( _boxMaps->GetChildren().size() + 1 ) + string( ". " ) + node->GetMap()->GetName() );

	label->SetAlignment( { 0.0f, 0.5f } );

	_boxMaps->Pack( label );

	_last = node;
}

void CampaignCreationController::HandleClickSave()
{
	if ( _campaign->GetStart() )
	{
		Data().campaigns.Add( _campaign );
		
		_campaign = nullptr;

		Reset();

		Push( new MessageController( "Validation successful. Resource saved.", "Back" ) );
	}
	else
	{
		Push( new MessageController( "A campaign must have at least one map.", "Back" ) );
	}
}

void CampaignCreationController::HandleClickQuit()
{
	Pop( this );
}

void CampaignCreationController::HandleClickClear()
{
	delete _campaign;

	_campaign = nullptr;

	Reset();
}
