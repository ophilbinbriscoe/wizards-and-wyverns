#pragma once

#include <string>

#include "Resource.h"
#include "Usable.h"

using namespace std;

class Item : public Resource, public Usable
{
public:
	Item( const string& name = "" );
	virtual ~Item();
	
	virtual void Accept( AbstractUser * user ) const;
};