#pragma once

#include "Controller.h"

class MenuController : public Controller
{
public:
	MenuController();
	virtual ~MenuController();

protected:
	void Finalize();

	sfg::Box::Ptr _menu;
	sfg::Box::Ptr _container;

private:
	sfg::Box::Ptr _box;
	sfg::Alignment::Ptr _vertical;
	sfg::Alignment::Ptr _horizontal;
};

