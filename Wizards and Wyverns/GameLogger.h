#pragma once

#include "Observer.h"
#include "Game.h"
#include "Logger.h"

class GameLogger : public Logger, public Observer<Game>
{
public:
	GameLogger();
	virtual ~GameLogger();

	virtual void StartObserving();
	virtual void StopObserving();

	void OnGameOverHandle();
	void OnFinishCampaignHandle();

	void OnBeginTurnHandle(Character const * const);
	void OnEndTurnHandle(Character const * const);

	void OnBeginCombatHandle(Character const * const);
	void OnEndCombatHandle(Character const * const);

	void OnBeginMovementHandle(Character const * const);
	void OnEndMovementHandle(Character const * const);

	void OnBlockIllegalMovementHandle(const Position &);
	void OnYieldPhaseHandle(Character const * const);

	void OnStartMapHandle(Map const *);
	void OnFinishMapHandle(Map const *);
};