#pragma once

#include <tuple>

#include "Writer.h"
#include "Map.h"
#include "Dataset.h"
#include "Character.h"

#include "ItemContainerWriter.h"

/// Class allowing a Map template to be read from a file.
class MapWriter : public Writer
{
public:
	MapWriter( fstream& file, const char* filename, Dataset<Character *> characters );
	virtual ~MapWriter();

	void Write( Map const * map, ItemContainerWriter const * writer );

	map<Character const *, unsigned int> _order;
};

