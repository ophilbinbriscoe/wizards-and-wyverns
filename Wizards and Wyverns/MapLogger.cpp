#include "MapLogger.h"

MapLogger::MapLogger()
{}

MapLogger::~MapLogger()
{}

void MapLogger::StartObserving()
{
	_target->OnSetPosition.Subscribe(this, &MapLogger::OnSetPositionEventHandler);
}

void MapLogger::StopObserving()
{
	_target->OnSetPosition.Unsubscribe(this, &MapLogger::OnSetPositionEventHandler);
}

void MapLogger::OnSetPositionEventHandler(MapObject const * object)
{
	if ( enabled )
	{
		cout << "(map)  " << object->GetName() << " to " << _target->GetPosition( object ) << endl;
	}
}