//! @file 
//! @brief Implementation file for the PositionTestSuite class
//!

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>

#include "Position.h"

using namespace CppUnit;

//! Test Class for the Position class
class PositionTestSuite : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(PositionTestSuite);
	CPPUNIT_TEST(PositionAddOperator);
	CPPUNIT_TEST(PositionAddEqualOperator);
	CPPUNIT_TEST(PositionRemoveOperator);
	CPPUNIT_TEST(PositionRemoveEqualOperator);
	CPPUNIT_TEST(PositionEqualOperator);
	CPPUNIT_TEST_SUITE_END();
protected:
	void PositionAddOperator();
	void PositionAddEqualOperator();
	void PositionRemoveOperator();
	void PositionRemoveEqualOperator();
	void PositionEqualOperator();
};

//! cppunit test cases registration
CPPUNIT_TEST_SUITE_REGISTRATION(PositionTestSuite);

//! Tests adding positions together
//! Test Case: A position should have its x and y modified when it is moved
//! Tested item: Position::Operator+()
void PositionTestSuite::PositionAddOperator()
{
	Position pos1 = Position(5, 5);
	Position pos2 = Position(1, 0);
	Position pos3 = pos1 + pos2;
	CPPUNIT_ASSERT(pos3.x == 6 && pos3.y == 5);
}
//! Tests incrementing a position by an other position
//! Test Case: A position that is the sum of two position should have its x and y be the composition on each axis
//! Tested item: Position::Operator+=()
void PositionTestSuite::PositionAddEqualOperator()
{
	Position pos1 = Position(5, 5);
	Position pos2 = Position(1, 0);
	pos1 += pos2;
	CPPUNIT_ASSERT(pos1.x == 6 && pos1.y == 5);
}
//! Tests substracting a position by an other position
//! Test Case: A position that is the result of the substractions should have its x and y be the distance on each axis
//! Tested item: Position::Operator-()
void PositionTestSuite::PositionRemoveOperator()
{
	Position pos1 = Position(5, 5);
	Position pos2 = Position(1, 0);
	Position pos3 = pos1 - pos2;
	CPPUNIT_ASSERT(pos3.x == 4 && pos3.y == 5);
}
//! Tests decrementing a position by an other position
//! Test Case: A position should have its x and y modified when it is moved
//! Tested item: Position::Operator-=()
void PositionTestSuite::PositionRemoveEqualOperator()
{
	Position pos1 = Position(5, 5);
	Position pos2 = Position(1, 0);
	pos1 -= pos2;
	CPPUNIT_ASSERT(pos1.x == 4 && pos1.y == 5);
}

//! Tests copying a position
//! Test Case: A position that is copied should have the same x and y as its original
//! Tested item: Position::Operator=()
void PositionTestSuite::PositionEqualOperator()
{
	Position pos1 = Position(5, 5);
	Position pos2 = pos1;

	CPPUNIT_ASSERT(pos1.x == pos2.x && pos1.y == pos2.y);
}