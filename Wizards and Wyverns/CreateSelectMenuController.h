#pragma once

#include "MessageController.h"

#include "ItemContainer.h"

template<class type>
class CreateSelectMenuController : public MenuController
{
public:
	CreateSelectMenuController( const vector<type *> & existing );
	virtual ~CreateSelectMenuController();

	bool WasCancelled() const { return _cancel; }
	Equipment const * GetSelection() const { return _selection; }
	
protected:
	void PackDefaults();

	virtual bool Prevalidate();

	string _error;

	shared_ptr<sfg::Box> _fields;

	shared_ptr<sfg::Entry> _name;

	virtual void Create() const = 0;
	virtual void Edit( type * existing ) const = 0;

	void HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke );

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );

private:
	shared_ptr<sfg::ComboBox> _combo;
	vector<type *> _existing;
	type * _selection;

	void HandleEdit();
	void HandleBack();
	void HandleCreate();
};

template<class type>
CreateSelectMenuController<type>::CreateSelectMenuController( const vector<type *> & existing ) : MenuController(), _existing( existing )
{
	_fields = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 0.0f );

	auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL );

	auto label = sfg::Label::Create( "Name" );

	_name = sfg::Entry::Create( "" );
	_name->SetRequisition( { 100.0f, 0.0f } );

	box->Pack( label );
	box->Pack( _name );

	_fields->Pack( box );
}

template<class type>
CreateSelectMenuController<type>::~CreateSelectMenuController()
{}

template<class type>
inline void CreateSelectMenuController<type>::PackDefaults()
{
	_menu->Pack( _fields );

	auto buttonCreate = sfg::Button::Create( "Create" );

	buttonCreate->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CreateSelectMenuController<type>::HandleCreate, this ) );

	_menu->Pack( buttonCreate );

	if ( _existing.size() > 0 )
	{
		auto boxEdit = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 0.0f );

		_combo = sfg::ComboBox::Create();

		for ( auto element : _existing )
		{
			_combo->AppendItem( element->GetName() );
		}

		_combo->SelectItem( 0 );

		boxEdit->Pack( _combo );

		auto buttonEdit = sfg::Button::Create( "Edit" );

		buttonEdit->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CreateSelectMenuController<type>::HandleEdit, this ) );

		boxEdit->Pack( buttonEdit );

		_menu->Pack( boxEdit );
	}

	auto buttonBack = sfg::Button::Create( "Quit" );

	buttonBack->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &CreateSelectMenuController<type>::HandleBack, this ) );

	_menu->Pack( buttonBack );
}

template<class type>
inline bool CreateSelectMenuController<type>::Prevalidate()
{
	if ( _name->GetText().getSize() > 0 )
	{
		return true;
	}

	_error = "Please specify a name.";

	return false;
}

template<class type>
void CreateSelectMenuController<type>::HandleEdit()
{
	if ( _combo->GetSelectedItem() != sfg::ComboBox::NONE )
	{
		Edit( _existing[_combo->GetSelectedItem()] );

		Pop( this );
	}
}

template<class type>
inline void CreateSelectMenuController<type>::HandleCreate()
{
	if ( Prevalidate() )
	{
		Create();

		Pop( this );
	}
	else
	{
		Push( new MessageController( _error, "OK" ) );
	}
}

template<class type>
void CreateSelectMenuController<type>::HandleBack()
{
	Pop( this );
}

template<class type>
void CreateSelectMenuController<type>::HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke )
{
	if ( ke.code == sf::Keyboard::Escape )
	{
		HandleBack();
	}
}

template<class type>
void CreateSelectMenuController<type>::SubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Subscribe( this, &CreateSelectMenuController::HandleKeyPressed );
}

template<class type>
void CreateSelectMenuController<type>::UnsubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Unsubscribe( this, &CreateSelectMenuController::HandleKeyPressed );
}

