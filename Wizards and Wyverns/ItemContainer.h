#pragma once

#include <map>

#include "Equipment.h"

class ItemContainer
{
public:
	ItemContainer();
	ItemContainer( const ItemContainer & container );
	~ItemContainer();

	Event<Item const *> OnAddItem;
	Event<Item const *> OnRemoveItem;
	Event<Item const *> OnItemQuantityModified;

	bool AddItem( Item const * item, const unsigned int& quantity = 1 );
	bool RemoveItem( Item const * item, const unsigned int& quantity = 1 );
	void SetItemQuantity( Item const * item, const unsigned int& quantity );

	unsigned int GetItemQuantity( Item const * item ) const;
	const map<Item const *, unsigned int>& GetItems() const;

	void operator = ( const ItemContainer & container );

	void Empty();

	void EmptyInto( ItemContainer& container );

protected:
	map<Item const *, unsigned int> _items;
};

inline ItemContainer& operator<< ( ItemContainer& destination, const ItemContainer& source )
{
	for ( auto pair : source.GetItems() )
	{
		destination.AddItem( pair.first, pair.second );
	}

	return destination;
}