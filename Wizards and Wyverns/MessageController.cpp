#include "MessageController.h"

#include "HeroSelectionController.h"
#include "Database.h"

MessageController::MessageController( const string& message, const string& label, const bool& collapse ) : MenuController(), _collapse( collapse )
{
	_menu->Pack( sfg::Label::Create( message ) );

	auto button = sfg::Button::Create( label );

	button->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MessageController::HandleClick, this ) );

	_menu->Pack( button );

	Finalize();
}

MessageController::~MessageController()
{}

void MessageController::HandleClick()
{
	if ( _collapse )
	{
		Collapse();
	}
	else
	{
		Pop( this );
	}
}

void MessageController::HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke )
{
	if ( ke.code == sf::Keyboard::Space )
	{
		HandleClick();
	}
}

void MessageController::SubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Subscribe( this, &MessageController::HandleKeyPressed );
}

void MessageController::UnsubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Unsubscribe( this, &MessageController::HandleKeyPressed );
}
