#include "GameOverDialogController.h"

GameOverDialogController::GameOverDialogController() : MenuController()
{
	_menu->Pack( sfg::Label::Create( "You have died!" ) );

	auto buttons = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 10.0f );

	auto buttonQuit = sfg::Button::Create( "Quit" );

	buttonQuit->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &GameOverDialogController::HandleClickQuit, this ) );

	buttons->Pack( buttonQuit );

	//auto buttonRetry = sfg::Button::Create( "Retry" );

	//buttonRetry->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &GameOverDialogController::HandleClickRetry, this ) );

	//buttons->Pack( buttonRetry );

	_menu->Pack( buttons );

	Finalize();
}

GameOverDialogController::~GameOverDialogController()
{}

void GameOverDialogController::HandleClickRetry()
{
	Pop( this );
	OnRetry();
}

void GameOverDialogController::HandleClickQuit()
{
	Collapse();
}

