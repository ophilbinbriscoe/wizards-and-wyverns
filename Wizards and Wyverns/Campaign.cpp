#include "Campaign.h"

CampaignNode::CampaignNode( Map const * map ) : _map( map )
{}

CampaignNode::~CampaignNode()
{
	if ( _next )
	{
		delete _next;
	}
}

void CampaignNode::SetNext( CampaignNode * node )
{
	if ( _next != node )
	{
		_next = node;

		OnSetNext( node );
	}
}

CampaignNode * CampaignNode::GetNext() const
{
	return _next;
}

void CampaignNode::SetMap( Map const * map )
{
	if ( _map != map )
	{
		_map = map;

		OnSetMap( map );
	}
}

Map const * CampaignNode::GetMap() const
{
	return _map;
}

Campaign::Campaign( const string& name, CampaignNode * start ) : Resource( name ), _start( start )
{}

Campaign::~Campaign()
{
	if ( _start )
	{
		delete _start;
	}
}

void Campaign::SetStart( CampaignNode * node )
{
	if ( _start != node )
	{
		_start = node;

		OnSetStart( node );
	}
}

CampaignNode * Campaign::GetStart() const
{
	return _start;
}
