#include <iostream>
#include <regex>
#include <vector>
#include <string>
#include <stdlib.h>

#include "Dice.h"

using namespace std;

// regex to match a string following the format: xdy[+z]
regex r("[1-9][0-9]*d[1-9][0-9]*(\\+[0-9]+[0-9]*)?");

// initialize singleton
Dice const * const Dice::Singleton = new Dice();

Dice::Dice()
{}

Dice::~Dice()
{}

unsigned int Dice::Roll( const string& dice )
{
	unsigned int rolls = 0;
	unsigned int extra = 0;
	unsigned int value = 0;

	unsigned int total = 0;

	if ( regex_match( dice, r ) )
	{
		// check for matching format of given string

		string fullExpression = dice;

		string delimiter = "+";
		string mainExpression = fullExpression.substr( 0, fullExpression.find( delimiter ) );

		// if a (+) was detected, then read the extra amount
		if ( fullExpression.find( delimiter ) != -1 )
		{
			string extra_string = fullExpression.substr( fullExpression.find( delimiter ) + 1, fullExpression.length() );
			//cout << "Extra: " << extra_string << "\n";
			extra = (unsigned int) atoi( extra_string.c_str() );
		}

		delimiter = "d";
		string number_string = mainExpression.substr( 0, mainExpression.find( delimiter ) );
		string value_string = mainExpression.substr( mainExpression.find( delimiter ) + 1, mainExpression.length() );

		rolls = (unsigned int) atoi( number_string.c_str() );
		value = (unsigned int) atoi( value_string.c_str() );

		// roll dice for number of rolls
		for ( int i = 0; i < rolls; i++ )
		{
			total += (((unsigned int) rand()) % value) + 1u;
		}

		total += extra;

		Singleton->OnRoll( dice, total );

		return total;
	}
	else
	{
		return 0;
	}
}
