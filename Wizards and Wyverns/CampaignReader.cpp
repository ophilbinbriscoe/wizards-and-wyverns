#include "CampaignReader.h"

#include <vector>
#include <stdio.h>
#include <string>
#include <cstring>

#include <algorithm>

using namespace std;

#pragma warning(disable:4996)

CampaignReader::CampaignReader(const char* filename, Dataset<Campaign *>& dataset, Dataset<Map *>& maps )
{
	//open file
	FILE * file = fopen( filename, "r" );

	char header[32];
	char name[1024];

	int indices[32];

	do
	{
		int n = 0;

		int res = fscanf( file, "%s %i ", header, &n );

		if ( res == EOF )
		{
			break;
		}

		int index = 0;

		for ( int i = 0; i < n; i++ )
		{
			fscanf( file, "%i ", &index );
			indices[i] = index;
		}

		fgets( name, 1024, file );

		name[strcspn( name, "\r\n" )] = 0;

		Campaign * campaign = new Campaign( name );
		
		CampaignNode * node = new CampaignNode( maps.GetContents()[indices[0]] );

		campaign->SetStart( node );

		for ( int i = 1; i < n; i++ )
		{
			node->SetNext( new CampaignNode( maps.GetContents()[indices[i]] ) );
			node = node->GetNext();
		}

		dataset.Add( campaign );
	}
	while ( true );

	std::fclose(file);
}

CampaignReader::~CampaignReader()
{}
