#include "LevelUpController.h"

#include "HeroSelectionController.h"
#include "Database.h"

LevelUpController::LevelUpController( Character const * character, UserFreeStrategy<StatType const *> * strategy ) : MenuController(), _character( character ), _strategy( strategy )
{
	_menu->Pack( sfg::Label::Create( "You have levelled up!" ) );

	if ( _character->GetPoints() > 0 )
	{
		auto frame = sfg::Frame::Create( "Ability Scores" );

		frame->SetAlignment( { 0.5f, 0.0f } );

		auto verticals = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 5.0f );

		auto labels = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );
		auto scores = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );
		auto buttons = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

		bool max = true;

		max = CreateAbilityWidgets( Stats().Strength, labels, scores, buttons ) && max;
		max = CreateAbilityWidgets( Stats().Dexterity, labels, scores, buttons ) && max;
		max = CreateAbilityWidgets( Stats().Constitution, labels, scores, buttons ) && max;
		max = CreateAbilityWidgets( Stats().Intelligence, labels, scores, buttons ) && max;
		max = CreateAbilityWidgets( Stats().Wisdom, labels, scores, buttons ) && max;
		max = CreateAbilityWidgets( Stats().Charisma, labels, scores, buttons ) && max;

		if ( max )
		{
			_menu->Pack( sfg::Label::Create( "All ability scores maxed." ) );

			auto button = sfg::Button::Create( "OK" );

			button->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &LevelUpController::HandleClickOK, this ) );

			_menu->Pack( button );
		}
		else
		{
			verticals->Pack( labels );
			verticals->Pack( scores );
			verticals->Pack( buttons );

			frame->Add( verticals );

			_menu->Pack( frame );
		}
	}
	else
	{
		auto button = sfg::Button::Create( "OK" );

		button->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &LevelUpController::HandleClickOK, this ) );

		_menu->Pack( button );
	}

	Finalize();
}

LevelUpController::~LevelUpController()
{}

bool LevelUpController::CreateAbilityWidgets( StatType const * type, shared_ptr<sfg::Box> labels, shared_ptr<sfg::Box> scores, shared_ptr<sfg::Box> buttons )
{
	auto horizontal = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 5.0f );

	auto label = sfg::Label::Create( type->name + string( ": " ) );

	labels->Pack( label );

	auto score = sfg::Label::Create( to_string( (int) _character->GetAbilityScore( type ) ) );

	scores->Pack( score );

	if ( _character->GetAbilityScore( type ) < 20 )
	{
		auto button = sfg::Button::Create( "+1 " );

		button->GetSignal( sfg::Button::OnLeftClick ).Connect( [&, ptr = type] ()->void { HandleSelect( ptr ); } );

		buttons->Pack( button );

		return false;
	}
	else
	{
		auto max = sfg::Label::Create( "(max)" );

		buttons->Pack( max );

		return true;
	}
}

void LevelUpController::HandleSelect( StatType const * type )
{
	_strategy->Decide( _character, type );

	Pop( this );
}

void LevelUpController::HandleClickOK()
{
	Pop( this );
}

void LevelUpController::HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke )
{
	if ( ke.code == sf::Keyboard::Space )
	{
		HandleClickOK();
	}
}

void LevelUpController::SubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Subscribe( this, &LevelUpController::HandleKeyPressed );
}

void LevelUpController::UnsubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Unsubscribe( this, &LevelUpController::HandleKeyPressed );
}

