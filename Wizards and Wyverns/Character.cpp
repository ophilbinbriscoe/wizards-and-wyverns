// @file 
// @brief Implementation file for the Character class  
//!

#include "Character.h"

#include <stdlib.h>
#include <iostream>
#include <string>
#include <time.h>

#include "Dice.h"

Character::Character( const Character & character ) : Character( character._playable, character._level, character._type, character._alignment, character.GetName(), character._stats.at( Stats().MaxHitPoints ), character._stats.at( Stats().DamageTaken ), character._scores, character._worn, character._backpack )
{}

Character::Character( const bool& playable, const int & lvl, Class const * type, Alignment const * alignment, const string & name, const int & max, const int & dmg, const map<StatType const * const, Score> & scores, const map<const SlotType const * const, Slot> & worn, const ItemContainer & backpack ) : MapObject( name ), _playable( playable ), _level( lvl ), _type( type ), _alignment( alignment )
{
	BuildAbilities( scores );

	BuildBonuses();

	BuildStats( max, dmg );

	BuildSlots();

	CalculateModifiers();
	CalculateDamageBonus();
	CalculateAttacks();
	CalculateArmorClass();
	CalculateHitPoints();

	for ( auto pair : worn )
	{
		if ( !pair.second.IsEmpty() )
		{
			Equip( pair.second.GetEquipped() );
		}
	}

	_backpack = backpack;
}

Character::~Character()
{}

void Character::Accept( AbstractInteractor * interactor )
{
	interactor->Interact( this );
}

void Character::Accept( AbstractInspector * inspector ) const
{
	inspector->Inspect( this );
}

void Character::Use( Item const * usable )
{
	// do nothing
}

void Character::Use( Equipment const * usable )
{
	Equip( usable );
}

// Assigns a random value between 3 and 18 to each base attribute and to 
// attributes with equpiment, since at creation the character is naked.
void Character::BuildAbilities( const map<StatType const * const, Score> & scores )
{
	BuildAbility( Stats().Strength, scores );
	BuildAbility( Stats().Dexterity, scores );
	BuildAbility( Stats().Constitution, scores );
	BuildAbility( Stats().Wisdom, scores );
	BuildAbility( Stats().Intelligence, scores );
	BuildAbility( Stats().Charisma, scores );

	_modifiers.at( Stats().Strength ).OnModified.Subscribe( this, &Character::HandleStrengthModifierModified );
	_modifiers.at( Stats().Dexterity ).OnModified.Subscribe( this, &Character::HandleDexterityModifierModified );
}

void Character::BuildAbility( StatType const * const type, const map<StatType const * const, Score> & scores )
{
	_scores.insert( pair<StatType const *, Score>( type, Score( type, (int) scores.at( type ) ) ) );
	_scores.at( type ).OnModified.Subscribe( this, &Character::HandleAbilityScoreModified );

	_modifiers.insert( pair<StatType const *, Modifier>( type, Modifier( type, Score::ToModifier( _scores.at( type ) ) ) ) );
}

void Character::BuildStats( const int & max, const int & dmg )
{
	BuildStat( Stats().MaxHitPoints, max );
	BuildStat( Stats().HitPoints, max - dmg );
	BuildStat( Stats().DamageTaken, dmg );

	BuildStat( Stats().DamageBonus, 0 );

	BuildStat( Stats().ArmorClass, 0 );
	
	_stats.at( Stats().MaxHitPoints ).OnModified.Subscribe( this, &Character::HandleHPFactorModified );
	_stats.at( Stats().DamageTaken ).OnModified.Subscribe( this, &Character::HandleHPFactorModified );
}

DerivedStat& Character::BuildStat( StatType const * const type, const int& value )
{
	_stats.insert( pair<StatType const *, DerivedStat>( type, DerivedStat( type, value ) ) );
	return _stats.at( type );
}

void Character::BuildSlots()
{
	BuildSlot( Slots().Head );
	BuildSlot( Slots().Body );
	BuildSlot( Slots().WeaponHand );
	BuildSlot( Slots().ShieldHand );
	BuildSlot( Slots().Waist );
	BuildSlot( Slots().Feet );
	BuildSlot( Slots().RingFinger );
}

Slot& Character::BuildSlot( SlotType const * const type )
{
	_worn.insert( pair<SlotType const *, Slot>( type, Slot( type ) ) );

	_worn.at( type ).OnEquip.Subscribe( this, &Character::HandleEquip );
	_worn.at( type ).OnUnequip.Subscribe( this, &Character::HandleUnequip );

	return _worn.at( type );
}

void Character::BuildBonuses()
{
	BuildBonus( Stats().Strength ).OnModified.Subscribe( this, &Character::HandleAbilityBonusModified );
	BuildBonus( Stats().Dexterity ).OnModified.Subscribe( this, &Character::HandleAbilityBonusModified );
	BuildBonus( Stats().Constitution ).OnModified.Subscribe( this, &Character::HandleAbilityBonusModified );
	BuildBonus( Stats().Intelligence ).OnModified.Subscribe( this, &Character::HandleAbilityBonusModified );
	BuildBonus( Stats().Wisdom ).OnModified.Subscribe( this, &Character::HandleAbilityBonusModified );
	BuildBonus( Stats().Charisma ).OnModified.Subscribe( this, &Character::HandleAbilityBonusModified );

	BuildBonus( Stats().AttackBonus ).OnModified.Subscribe( this, &Character::HandleAttackBonusBonusModified );
	BuildBonus( Stats().DamageBonus ).OnModified.Subscribe( this, &Character::HandleDamageBonusBonusModified );
	BuildBonus( Stats().ArmorClass ).OnModified.Subscribe( this, &Character::HandleArmorClassBonusModified );
}

Bonus& Character::BuildBonus( StatType const * const type )
{
	_bonuses.insert( pair<StatType const *, Bonus>( type, Bonus( type, 0 ) ) );
	return _bonuses.at( type );
}

void Character::HandleAbilityScoreModified( Score const * const score )
{
	CalculateModifier( score->type );
}

void Character::HandleAbilityBonusModified( Bonus const * const bonus )
{
	CalculateModifier( bonus->type );
}

void Character::HandleStrengthModifierModified( Modifier const * const modifier )
{
	CalculateAttacks();
	CalculateDamageBonus();
}

void Character::HandleDexterityModifierModified( Modifier const * const modifier )
{
	CalculateArmorClass();
}

void Character::HandleAttackBonusBonusModified( Bonus const * const stat )
{
	CalculateAttacks();
}

void Character::HandleDamageBonusBonusModified( Bonus const * const bonus )
{
	CalculateDamageBonus();
}

void Character::HandleArmorClassBonusModified( Bonus const * const bonus )
{
	CalculateArmorClass();
}

void Character::HandleHPFactorModified( DerivedStat const * const stat )
{
	CalculateHitPoints();
}

void Character::HandleEquip( Equipment const * const equipment )
{
	for ( auto enchantment : equipment->GetEnchantments() )
	{
		_bonuses.at( enchantment.type ) += enchantment.bonus;
	}
}

void Character::HandleUnequip( Equipment const * const equipment )
{
	for ( auto enchantment : equipment->GetEnchantments() )
	{
		_bonuses.at( enchantment.type ) -= enchantment.bonus;
	}
}

void Character::CalculateModifiers()
{
	CalculateModifier( Stats().Strength );
	CalculateModifier( Stats().Dexterity );
	CalculateModifier( Stats().Constitution );
	CalculateModifier( Stats().Intelligence );
	CalculateModifier( Stats().Wisdom );
	CalculateModifier( Stats().Charisma );
}

void Character::CalculateModifier( StatType const * const type )
{
	_modifiers.at( type ) = Score::ToModifier( _scores.at( type ) + _bonuses.at( type ) );
}

void Character::CalculateHitPoints()
{
	_stats.at( Stats().HitPoints ) = _stats.at( Stats().MaxHitPoints ) - _stats.at( Stats().DamageTaken );
}

void Character::CalculateAttacks()
{
	vector<int> attacks;

	_type->CalculateBaseAttackBonus( attacks, _level );

	_attacks.clear();

	int bonus = _bonuses.at( Stats().AttackBonus ) + _modifiers.at( Stats().Strength );

	for ( unsigned int i = 0; i < attacks.size(); i++ )
	{
		_attacks.push_back( attacks[i] + bonus );
	}

	OnAttackBonusesModified( _attacks );
}

void Character::CalculateDamageBonus()
{
	_stats.at( Stats().DamageBonus ) = _bonuses.at( Stats().DamageBonus ) + _modifiers.at( Stats().Strength );
}

void Character::CalculateArmorClass()
{
	_stats.at( Stats().ArmorClass ) = 10 + _bonuses.at( Stats().ArmorClass ) + _modifiers.at( Stats().Dexterity );
}

void Character::Equip( Equipment const * equipment )
{
	_worn.at( equipment->slot ).Equip( equipment );
}

// Implements the hit function that reduces the current Hit Points of the character when hit
// @param damage: damage sustained by the character
void Character::Hit( int damage )
{
	_stats.at( Stats().DamageTaken ) += damage;

	if ( _stats.at( Stats().HitPoints ) <= 0 )
	{
		OnDie( this );
	}
}

void Character::LevelUp()
{
	_stats.at( Stats().MaxHitPoints ) += Dice::Roll( string( "1" ) + _type->hitDie ) + _modifiers.at( Stats().Constitution );
	_stats.at( Stats().DamageTaken ) = 0;
	
	_level++;
	
	if ( _level % 4 == 0 )
	{
		_points++;
	}

	CalculateAttacks();

	OnLevelUp( this );
}

const bool & Character::IsPlayable() const
{
	return _playable;
}

const int& Character::GetLevel() const
{
	return _level;
}

Class const * Character::GetClass() const
{
	return _type;
}

Alignment const * Character::GetAlignment() const
{
	return _alignment;
}

const unsigned int& Character::GetPoints() const
{
	return _points;
}

void Character::IncreaseAbilityScore( StatType const * type )
{
	if ( _points > 0 )
	{
		_scores.at( type ) += 1;
		_points -= 1;
	}
}

void Character::Unequip( SlotType const * type )
{
	_worn.at( type ).Unequip();
}

void Character::Interact( MapObject * object )
{
	// do noting
}

void Character::Interact( Character * character )
{

	// check that the target Character is not already dead
	if ( character->GetStat( Stats().HitPoints ) >0 )
	{
		// do all attacks
		for ( int i = 0; i < _attacks.size(); i++ )
		{
			// roll to hit
			int attackRoll = (int) Dice::Roll( "1d20" );

			int damageRoll = 0;

			bool hit;

			// check for automatic miss
			if ( attackRoll == 1 )
			{
				hit = false;
			}

			// check for automatic "critical" hit
			else if ( attackRoll == 20 )
			{
				hit = true;
			}

			// otherwise, normal hit conditions apply
			else
			{
				hit = attackRoll + _attacks[i] >= character->GetStat( Stats().ArmorClass );
			}

			// check if the attack hit
			if ( hit )
			{
				// default case: unarmed, deal 1d3 of damage
				string str = "1d3";

				// check if this Character is armed
				if ( !_worn.at( Slots().WeaponHand ).IsEmpty() )
				{
					auto weapon = dynamic_cast<Weapon const *>(_worn.at( Slots().WeaponHand ).GetEquipped());

					// verify that the equipped Item is a Weapon
					if ( weapon )
					{
						// deal damage according to the equipped weapon's damage dice
						str = weapon->type->damage;
					}
				}

				// add the DamageBonus stat to the roll
				str += string( "+" ) + to_string( _stats.at( Stats().DamageBonus ) );

				// roll damage
				damageRoll = (int) Dice::Roll( str );

				// apply damage
				character->Hit( damageRoll );
			}

			if ( this->_playable )
			{
				character->_alignment = Alignment::Evil;

				character->OnAlignmentModified( character );
			}

			// notify listeners of an attack
			OnAttack( this, character, hit, attackRoll, _attacks[i], damageRoll );
		}
	}

	// otherwise, loot corpse
	else
	{
		// iterate over the target Character's equipment slots
		for ( auto pair : character->_worn )
		{
			// if the slot has something equipped
			if ( !pair.second.IsEmpty() )
			{
				// add the slot's equipment to this Character's backpack
				_backpack.AddItem( pair.second.GetEquipped() );

				// remove the equipment
				pair.second.Unequip();
			}
		}

		// empty the contents of the target Character's backpack into this Character's backpack
		character->_backpack.EmptyInto( _backpack );

		// notify listener's of a looted Corpse
		OnLootCorpse( this, character );
	}
}

void Character::Interact( Chest * chest )
{
	// empty the contents of the chest into this Character's backpack
	chest->EmptyInto( _backpack );

	// notify listeners of a looted Chest
	OnLootChest( this, chest );
}

bool Character::Validate()
{
	return _level > 0;
}

void Character::operator=( const Character & character )
{
	SetName( character.GetName() );

	_type = character._type;
	_level = character._level;
	_alignment = character._alignment;

	_scores.at( Stats().Strength ) = character._scores.at( Stats().Strength );
	_scores.at( Stats().Dexterity ) = character._scores.at( Stats().Dexterity );
	_scores.at( Stats().Constitution ) = character._scores.at( Stats().Constitution );
	_scores.at( Stats().Intelligence ) = character._scores.at( Stats().Intelligence );
	_scores.at( Stats().Wisdom ) = character._scores.at( Stats().Wisdom );
	_scores.at( Stats().Charisma ) = character._scores.at( Stats().Charisma );

	_stats.at( Stats().MaxHitPoints ) = character._stats.at( Stats().MaxHitPoints );
	_stats.at( Stats().HitPoints ) = character._stats.at( Stats().HitPoints );

	for ( auto pair : character._worn )
	{
		if ( pair.second.GetEquipped() )
		{
			Equip( pair.second.GetEquipped() );
		}
		else
		{
			_worn.at( pair.first ).Unequip();
		}
	}

	_backpack = character._backpack;
}

const Score & Character::GetAbilityScore( StatType const * const type ) const
{
	return _scores.at( type );
}

const Modifier & Character::GetAbilityModifier( StatType const * const type ) const
{
	return _modifiers.at( type );
}

const Bonus & Character::GetEquipmentBonus( StatType const * const type ) const
{
	return _bonuses.at( type );
}

const DerivedStat & Character::GetStat( StatType const * const type ) const
{
	return _stats.at( type );
}

const Slot & Character::GetSlot( SlotType const * const type ) const
{
	return _worn.at( type );
}

ItemContainer & Character::AccessBackpack()
{
	return _backpack;
}

ItemContainer const * const Character::GetBackpack() const
{
	return &_backpack;
}

const vector<int>& Character::GetAttackBonuses() const
{
	return _attacks;
}

const map<SlotType const*const, Slot>& Character::GetWorn() const
{
	return _worn;
}
