#pragma once

#include <vector>

#include "DomainConcept.h"

using namespace std;

class Alignment : public DomainConcept
{
	typedef void( *FunctionPointer )(vector<int>&, const int&);
public:
	static Alignment const * const Good;
	static Alignment const * const Evil;

private:
	Alignment( const string& name, const int& enumerator );
	~Alignment();

	FunctionPointer _baseAttackBonusCalculator;
};