#include "CharacterProxy.h"

CharacterProxy::CharacterProxy( Character const * character ) : Proxy( character ), _override( character->GetAlignment() )
{}

CharacterProxy::~CharacterProxy()
{}

void CharacterProxy::SetOverride( Alignment const * alignment )
{
	if ( alignment != _override )
	{
		_override = alignment;

		OnAlignmentModified( this );
	}
}

void CharacterProxy::Accept( AbstractInteractor * interactor )
{
	// do nothing
}

void CharacterProxy::Accept( AbstractInspector * inspector ) const
{
	inspector->Inspect( this );
}
