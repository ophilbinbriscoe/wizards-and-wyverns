#include "MapObject.h"

MapObject::MapObject( const string& name ) : Resource( name )
{}

MapObject::~MapObject()
{}

void MapObject::Accept( AbstractInteractor * interactor )
{
	interactor->Interact( this );
}

void MapObject::Accept( AbstractInspector * inspector ) const
{
	inspector->Inspect( this );
}
