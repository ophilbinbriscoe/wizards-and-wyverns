#pragma once

#include "SelectionMenuController.h"

#include "Campaign.h"
#include "Character.h"

class CampaignSelectionController : public SelectionMenuController<Campaign>
{
public:
	CampaignSelectionController( const Dataset<Campaign *>& dataset, Character * const player );
	virtual ~CampaignSelectionController();

protected:
	Character * const _player;

	virtual void HandleSelect( Campaign * campaign );
};

