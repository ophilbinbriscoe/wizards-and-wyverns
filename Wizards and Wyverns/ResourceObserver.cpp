#include "ResourceObserver.h"

ResourceObserver::ResourceObserver( shared_ptr<sfg::Box> container ) : View( container )
{}

ResourceObserver::~ResourceObserver()
{}

void ResourceObserver::HandleRename( Resource const * resource )
{
	// update the label text to match the Resource's new name
	_label->SetText( _target->GetName() );
}

void ResourceObserver::StartObserving()
{
	// listen for rename events
	_target->OnRename.Subscribe( this, &ResourceObserver::HandleRename );

	// create the label
	_label = sfg::Label::Create( _target->GetName() );

	// set its alignment to be left-adjusted
	_label->SetAlignment( { 0.0f, 0.0f } );

	// pack the label into the parent Box
	_container->Pack( _label, false, false );
}

void ResourceObserver::StopObserving()
{
	// stop listening for rename events
	_target->OnRename.Unsubscribe( this, &ResourceObserver::HandleRename );

	// stop displaying the label
	_label->Show( false );

	// remove the label from the parent Box
	_container->Remove( _label );
}
