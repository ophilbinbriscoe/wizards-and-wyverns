#include "Enchantment.h"

#include <algorithm>

using namespace std;

Enchantment::Enchantment( StatType const * const type, const unsigned int & bonus ) : type( type ), bonus( max( (unsigned int) 1, min( bonus, (unsigned int) 5 ) ) )
{}

Enchantment::~Enchantment()
{}