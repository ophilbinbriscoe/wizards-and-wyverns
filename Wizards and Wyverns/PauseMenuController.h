#pragma once

#include "MessageController.h"
#include "GameController.h"

class PauseMenuController : public MenuController
{
public:
	PauseMenuController( Game const * const game, GameController * const controller );
	virtual ~PauseMenuController();

	Event<> OnSave;
	Event<> OnLoad;
	Event<> OnQuit;
	
protected:
	Game const * const _game;
	GameController * const _controller;

	shared_ptr<sfg::CheckButton> _gameToggle;
	shared_ptr<sfg::CheckButton> _diceToggle;
	shared_ptr<sfg::CheckButton> _mapToggle;
	shared_ptr<sfg::CheckButton> _characterToggle;

	shared_ptr<sfg::ComboBox> _autoCombo;

	void HandleToggleGameLogger();
	void HandleToggleDiceLogger();
	void HandleToggleMapLogger();
	void HandleToggleCharacterLogger();

	void HandleSelectAuto();

	void HandleClickSave();
	void HandleClickLoad();
	void HandleClickQuit();
	void HandleClickBack();

	void HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke );

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );
};

