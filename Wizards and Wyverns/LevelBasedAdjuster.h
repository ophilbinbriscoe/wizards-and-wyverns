#pragma once

#include "MapAdjuster.h"

class LevelBasedAdjuster : public MapAdjuster
{
public:
	LevelBasedAdjuster( Character const * target );
	virtual ~LevelBasedAdjuster();

protected:
	virtual void Adjust( Chest const * chest, const Position & position, Map * map );
	virtual void Adjust( CharacterProxy const * proxy, const Position & position, Map * map );

private:
	Character const * const _target;
};

