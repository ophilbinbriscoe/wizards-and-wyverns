#pragma once

#include "StatType.h"

using namespace std;

class SlotType : public DomainConcept
{
public:
	struct SlotTypeData
	{
		SlotType const * const Head;
		SlotType const * const Body;
		SlotType const * const ShieldHand;
		SlotType const * const WeaponHand;
		SlotType const * const Waist;
		SlotType const * const Feet;
		SlotType const * const RingFinger;

		SlotTypeData();
		~SlotTypeData();
	};

	bool SupportsEnchantmentOf( StatType const * const statType ) const;

private:
	SlotType( const string& name, const StatTypes& supported, const unsigned int enumerator = -1 );
	~SlotType();

	const StatTypes _supported;
};

const SlotType::SlotTypeData& Slots();

