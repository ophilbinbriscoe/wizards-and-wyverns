#pragma once

#include "Strategy.h"

template <typename type>
class DummyStrategy : public FreeStrategy<type>
{
public:
	DummyStrategy();
	~DummyStrategy();

	virtual void RequestDecision( Character const * const character, Map const * const map );
};

template<typename type>
inline DummyStrategy<type>::DummyStrategy()
{}

template<typename type>
inline DummyStrategy<type>::~DummyStrategy()
{}

template<typename type>
inline void DummyStrategy<type>::RequestDecision( Character const * const character, Map const * const map )
{}
