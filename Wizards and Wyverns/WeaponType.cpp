#include "WeaponType.h"

// simple melee weapon types

WeaponType const * const WeaponType::Club = new WeaponType( "Club", 30, "1d4" );
WeaponType const * const WeaponType::Dagger = new WeaponType( "Dagger", 31, "1d4" );
WeaponType const * const WeaponType::Greatclub = new WeaponType( "Greatclub", 32, "1d8" );
WeaponType const * const WeaponType::Handaxe = new WeaponType( "Handaxe", 33, "1d6" );
WeaponType const * const WeaponType::Javelin = new WeaponType( "Javelin", 34, "1d6" );
WeaponType const * const WeaponType::LightHammer = new WeaponType( "Light Hammer", 35, "1d4" );
WeaponType const * const WeaponType::Mace = new WeaponType( "Mace", 36, "1d6" );
WeaponType const * const WeaponType::Quarterstaff = new WeaponType( "Quarterstaff", 37, "1d6" );
WeaponType const * const WeaponType::Sickle = new WeaponType( "Sickle", 38, "1d4" );
WeaponType const * const WeaponType::Spear = new WeaponType( "Spear", 39, "1d6" );

// martial melee weapon types

WeaponType const * const WeaponType::Battleaxe = new WeaponType( "Battleaxe", 40, "1d8" );
WeaponType const * const WeaponType::Flail = new WeaponType( "Flail", 41, "1d8" );
WeaponType const * const WeaponType::Glaive = new WeaponType( "Glaive", 42, "1d10" );
WeaponType const * const WeaponType::Greataxe = new WeaponType( "Greataxe", 43, "1d12" );
WeaponType const * const WeaponType::Greatsword = new WeaponType( "Greatsword", 44, "2d6" );
WeaponType const * const WeaponType::Halberd = new WeaponType( "Halberd", 45, "1d10" );
WeaponType const * const WeaponType::Lance = new WeaponType( "Lance", 46, "1d12" );
WeaponType const * const WeaponType::Longsword = new WeaponType( "Longsword", 47, "1d8" );
WeaponType const * const WeaponType::Maul = new WeaponType( "Maul", 48, "2d6" );
WeaponType const * const WeaponType::Morningstar = new WeaponType( "Morningstar", 49, "1d8" );
WeaponType const * const WeaponType::Pike = new WeaponType( "Pike", 50, "1d10" );
WeaponType const * const WeaponType::Rapier = new WeaponType( "Rapier", 51, "1d8" );
WeaponType const * const WeaponType::Scimitar = new WeaponType( "Scimitar", 52, "1d6" );
WeaponType const * const WeaponType::Shortsword = new WeaponType( "Shortsword", 53, "1d6" );
WeaponType const * const WeaponType::Trident = new WeaponType( "Trident", 54, "1d6" );
WeaponType const * const WeaponType::WarPick = new WeaponType( "War Pick", 55, "1d8" );
WeaponType const * const WeaponType::Warhammer = new WeaponType( "Warhammer", 56, "1d8" );
WeaponType const * const WeaponType::Whip = new WeaponType( "Whip", 57, "1d4" );

//simple ranged weapon types
WeaponType const * const WeaponType::CrossbowLight = new WeaponType("Crossbow, Light", 58, "1d8");
WeaponType const * const WeaponType::Dart = new WeaponType("Dart", 59, "1d4");
WeaponType const * const WeaponType::Shortbow = new WeaponType("Shortbow", 60, "1d6");
WeaponType const * const WeaponType::Sling = new WeaponType("Sling", 61, "1d4");

// martial ranged weapon types
WeaponType const * const WeaponType::Blowgun = new WeaponType("Blowgun", 62, "1d1");
WeaponType const * const WeaponType::CrossbowHand = new WeaponType("Crossbow, Hand", 63, "1d6");
WeaponType const * const WeaponType::CrossbowHeavy = new WeaponType("Crossbow, Heavy", 64, "1d10");
WeaponType const * const WeaponType::Longbow = new WeaponType("Longbow", 65, "1d8");

WeaponType::WeaponType( const string& name, const int& enumerator, const string& damage ) : DomainConcept( name, "", enumerator ), damage( damage )
{}

WeaponType::~WeaponType()
{}