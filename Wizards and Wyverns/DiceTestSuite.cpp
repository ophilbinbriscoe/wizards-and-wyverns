//! @file 
//! @brief Implementation file for the DiceTest class
//!

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include "Dice.h"
using namespace CppUnit;

//! Test Class for the Dice class
class DiceTestSuite : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(DiceTestSuite);
	CPPUNIT_TEST(testDiceRollInputValidity); //test the lower bound of the returned value 
	CPPUNIT_TEST(testDiceRollingBounds); //test the upper bound of the returned value 
	CPPUNIT_TEST_SUITE_END();
protected:
	void testDiceRollInputValidity();
	void testDiceRollingBounds();
};

//! cppunit test cases registration
CPPUNIT_TEST_SUITE_REGISTRATION(DiceTestSuite);

//! test method for role function of the Dice class 
//! Test Case: the the validity of input string
void DiceTestSuite::testDiceRollInputValidity()
{
	int result = 0;

	// test that if the string is valid, the result is calculated
	result = Dice::Roll("4d20");
	CPPUNIT_ASSERT(result > 0);
	result = Dice::Roll("3d10+1");
	CPPUNIT_ASSERT(result > 0);

	//test that is the string is invalid, Roll() returns 0
	result = Dice::Roll("4d");
	CPPUNIT_ASSERT(result == 0);
	result = Dice::Roll("d20");
	CPPUNIT_ASSERT(result == 0);
	result = Dice::Roll("420");
	CPPUNIT_ASSERT(result == 0);
	result = Dice::Roll("4d20+");
	CPPUNIT_ASSERT(result == 0);

	// Added test cases	considering wrong input with (+)
	result = Dice::Roll("d100+");
	CPPUNIT_ASSERT(result == 0);

	result = Dice::Roll("4d+");
	CPPUNIT_ASSERT(result == 0);
}


//! test method for role function of the Dice class 
//! Test Case: the upperbound of all values returned from Roll function
void DiceTestSuite::testDiceRollingBounds()
{
	int result = 0;

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "1d4" );
		CPPUNIT_ASSERT( result >= 1 );
		CPPUNIT_ASSERT( result <= 4 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "1d6" );
		CPPUNIT_ASSERT( result >= 1 );
		CPPUNIT_ASSERT( result <= 6 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "1d8" );
		CPPUNIT_ASSERT( result >= 1 );
		CPPUNIT_ASSERT( result <= 8 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "1d10" );
		CPPUNIT_ASSERT( result >= 1 );
		CPPUNIT_ASSERT( result <= 10 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "1d12" );
		CPPUNIT_ASSERT( result >= 1 );
		CPPUNIT_ASSERT( result <= 12 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "1d20" );
		CPPUNIT_ASSERT( result >= 1 );
		CPPUNIT_ASSERT( result <= 20 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "1d100" );
		CPPUNIT_ASSERT( result >= 1 );
		CPPUNIT_ASSERT( result <= 100 );
	}

	// Added tests to check for different number of Rolls

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "2d100" );
		CPPUNIT_ASSERT( result >= 2 );
		CPPUNIT_ASSERT( result <= 2 * 100 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "10d20" );
		CPPUNIT_ASSERT( result >= 10 );
		CPPUNIT_ASSERT( result <= 10 * 20 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "4d12" );
		CPPUNIT_ASSERT( result >= 4 );
		CPPUNIT_ASSERT( result <= 4 * 12 );
	}

	// Added tests to check for different number of Rolls considering extra

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "4d12+5" );
		CPPUNIT_ASSERT( result >= 4 );
		CPPUNIT_ASSERT( result <= (4 * 12) + 5 );
	}

	for ( int j = 1; j <= 1000; j++ )
	{
		result = Dice::Roll( "2d4+25" );
		CPPUNIT_ASSERT( result >= 4 );
		CPPUNIT_ASSERT( result <= (2 * 4) + 25 );
	}
}
