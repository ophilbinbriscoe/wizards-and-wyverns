#pragma once

#include "Writer.h"
#include "Character.h"
#include "Dataset.h"
#include "ItemContainerWriter.h"

class CharacterWriter : public Writer
{
public:
	CharacterWriter( std::fstream& file, const char * filepath );
	virtual ~CharacterWriter();

	void Write( const string& header, Character * character, ItemContainerWriter const * writer );

private:
	FILE * file;
};

