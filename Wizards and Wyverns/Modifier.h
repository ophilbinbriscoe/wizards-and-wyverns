#pragma once

#include "Stat.h"

class Modifier : public Stat<Modifier>
{
public:
	Modifier( StatType const * const type, const int& score );
	~Modifier();
	
	operator int() const { return _value; }

	virtual const int Min() const { return -5; }
	virtual const int Max() const { return 5; }

	void operator = ( const Modifier& modifier );
	void operator = ( const int& value );
	void operator+= ( const int& value );
	void operator-= ( const int& value );
};

inline void Modifier::operator=( const Modifier & modifier )
{
	Value::operator=( modifier._value );
}

inline void Modifier::operator= ( const int& value )
{
	Value::operator=( value );
}

inline void Modifier::operator+= ( const int& value )
{
	Value::operator+=( value );
}

inline void Modifier::operator-= ( const int& value )
{
	Value::operator-=( value );
}

