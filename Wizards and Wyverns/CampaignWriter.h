#pragma once

#include <tuple>
#include <map>

#include "Writer.h"
#include "Map.h"
#include "Campaign.h"
#include "Dataset.h"

/// Class allowing a Map template to be read from a file.
class CampaignWriter : public Writer
{
public:
	CampaignWriter( fstream& file, const char* filename, const Dataset<Map*> & maps );
	virtual ~CampaignWriter();

	void Write( const Campaign * map );

	map<Map const *, unsigned int> _order;
};

