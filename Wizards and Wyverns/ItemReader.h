#pragma once

#include "Reader.h"
#include "Weapon.h"

class ItemReader : public Reader<Item*>
{
public:
	ItemReader( const char* filename );
	virtual ~ItemReader();

	virtual bool Read();
};

