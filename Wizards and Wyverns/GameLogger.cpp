#include "GameLogger.h"

GameLogger::GameLogger()
{}

GameLogger::~GameLogger()
{}

void GameLogger::StartObserving()
{
	_target->OnGameOver.Subscribe(this, &GameLogger::OnGameOverHandle);
	_target->OnFinishCampaign.Subscribe(this, &GameLogger::OnFinishCampaignHandle);
	_target->OnBeginTurn.Subscribe(this, &GameLogger::OnBeginTurnHandle);
	_target->OnEndTurn.Subscribe(this, &GameLogger::OnEndTurnHandle);
	_target->OnBeginInteraction.Subscribe(this, &GameLogger::OnBeginCombatHandle);
	_target->OnEndInteraction.Subscribe(this, &GameLogger::OnEndCombatHandle);
	_target->OnBeginMovement.Subscribe(this, &GameLogger::OnBeginMovementHandle);
	_target->OnEndMovement.Subscribe(this, &GameLogger::OnEndMovementHandle);
	_target->OnBlockIllegalMovement.Subscribe(this, &GameLogger::OnBlockIllegalMovementHandle);
	_target->OnYieldPhase.Subscribe(this, &GameLogger::OnYieldPhaseHandle);
	_target->OnStartMap.Subscribe(this, &GameLogger::OnStartMapHandle);
	_target->OnFinishMap.Subscribe(this, &GameLogger::OnFinishMapHandle);
}

void GameLogger::StopObserving()
{
	_target->OnGameOver.Unsubscribe(this, &GameLogger::OnGameOverHandle);
	_target->OnFinishCampaign.Unsubscribe(this, &GameLogger::OnFinishCampaignHandle);
	_target->OnBeginTurn.Unsubscribe(this, &GameLogger::OnBeginTurnHandle);
	_target->OnEndTurn.Unsubscribe(this, &GameLogger::OnEndTurnHandle);
	_target->OnBeginInteraction.Unsubscribe(this, &GameLogger::OnBeginCombatHandle);
	_target->OnEndInteraction.Unsubscribe(this, &GameLogger::OnEndCombatHandle);
	_target->OnBeginMovement.Unsubscribe(this, &GameLogger::OnBeginMovementHandle);
	_target->OnEndMovement.Unsubscribe(this, &GameLogger::OnEndMovementHandle);
	_target->OnBlockIllegalMovement.Unsubscribe(this, &GameLogger::OnBlockIllegalMovementHandle);
	_target->OnYieldPhase.Unsubscribe(this, &GameLogger::OnYieldPhaseHandle);
	_target->OnStartMap.Unsubscribe(this, &GameLogger::OnStartMapHandle);
	_target->OnFinishMap.Unsubscribe(this, &GameLogger::OnFinishMapHandle);
}

void GameLogger::OnGameOverHandle()
{
	if ( enabled )
	{
		cout << "(game) game over!" << endl;
	}
}

void GameLogger::OnFinishCampaignHandle()
{
	if ( enabled )
	{
		cout << "(game) victory!" << endl;
	}
}

void GameLogger::OnBeginTurnHandle(Character const * const character)
{
	if ( enabled )
	{
		cout << "(game) " << character->GetName() << "'s turn" << endl;
	}
}

void GameLogger::OnEndTurnHandle(Character const * const character)
{
	if ( enabled )
	{
		//cout << "(game) end turn: " << character->GetName() << endl;
	}
}

void GameLogger::OnBeginCombatHandle(Character const * const character)
{
	if ( enabled )
	{
		cout << "(game) begin combat phase" << endl;
	}
}

void GameLogger::OnEndCombatHandle(Character const * const character)
{
	if ( enabled )
	{
		//cout << "(game) end combat phase: " << character->GetName() << endl;
	}
}

void GameLogger::OnBeginMovementHandle(Character const * const character)
{
	if ( enabled )
	{
		cout << "(game) begin movement phase" << endl;
	}
}

void GameLogger::OnEndMovementHandle(Character const * const character)
{
	if ( enabled )
	{
		//cout << "(game) end movement phase: " << character->GetName() << endl;
	}
}

void GameLogger::OnBlockIllegalMovementHandle(const Position & position)
{
	if ( enabled )
	{
		cout << "(game) move request blocked: " << position << endl;
	}
}

void GameLogger::OnYieldPhaseHandle(Character const * const character)
{
	if ( enabled )
	{
		cout << "(game) yield remainder of phase" << endl;
	}
}

void GameLogger::OnStartMapHandle(Map const * map)
{
	if ( enabled )
	{
		cout << "(game) starting map: " << map->GetName() << endl;
	}
}

void GameLogger::OnFinishMapHandle(Map const * map)
{
	if ( enabled )
	{
		cout << "(game) reached end of map: " << map->GetName() << endl;
	}
}