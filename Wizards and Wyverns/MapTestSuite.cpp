//! @file 
//! @brief Implementation file for the MapTestSuite class
//!

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>

#include "Map.h"
#include "Chest.h"
#include "TileType.h"

using namespace CppUnit;

//! Test Class for the Map class
class MapTestSuite : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(MapTestSuite);
	CPPUNIT_TEST(MapAddObject);
	CPPUNIT_TEST(MapSetObjectPosition);
	CPPUNIT_TEST(MapRemoveObject);
	CPPUNIT_TEST(MapGetEntrance);
	CPPUNIT_TEST(MapEqualOperator);
	CPPUNIT_TEST_SUITE_END();
protected:
	void MapAddObject();
	void MapSetObjectPosition();
	void MapRemoveObject();
	void MapGetEntrance();
	void MapEqualOperator();
};

//! cppunit test cases registration
CPPUNIT_TEST_SUITE_REGISTRATION(MapTestSuite);

//! Tests adding an object and finding it
//! Test Case: An object added to a map should be found when looked for
//! Tested item: Map::AddObjectAt() Map::Contains()
void MapTestSuite::MapAddObject()
{
	Map map = Map(5, 5, "Test");
	Chest * chest = new Chest();
	map.AddObjectAt(chest, Position(1,1));

	CPPUNIT_ASSERT(map.Contains(chest));
}
//! Tests setting adding an object and removing it
//! Test Case: Once a map's object has been removed it shouldnt be found when looked for
//! Tested item: Map::AddObjectAt() Map::RemoveObject() Map::Contains()
void MapTestSuite::MapRemoveObject()
{
	Map map = Map(5, 5, "Test");
	Chest * chest = new Chest();
	map.AddObjectAt(chest, Position(1, 1));
	map.RemoveObject(chest);
	CPPUNIT_ASSERT(!map.Contains(chest));
}
//! Tests adding an object and modifing its position
//! Test Case: A map's object should return its new position after it has been changed
//! Tested item: Map::AddObjectAt() Map::SetPosition() Map::GetPosition()
void MapTestSuite::MapSetObjectPosition()
{
	Map map = Map(5, 5, "Test");
	Chest * chest = new Chest();
	map.AddObjectAt(chest, Position(1, 1));
	map.SetPosition(chest, Position(4, 3));
	CPPUNIT_ASSERT(map.GetPosition(chest) == Position(4,3));
}
//! Tests setting an entrance and finding it
//! Test Case: A map's entrance should be returned when get is called
//! Tested item: Map::SetTileAt(), Map::GetEntrance()
void MapTestSuite::MapGetEntrance()
{
	Map map = Map(5, 5, "Test");
	map.SetTileAt(Position(0, 0), TileType::Entrance);

	CPPUNIT_ASSERT(map.GetEntrance(0) == Position(0,0));
}
//! Tests copying a map
//! Test Case: A copied map should share the same name as the original's
//! Tested item: Map::Operator=()
void MapTestSuite::MapEqualOperator()
{
	Map map = Map(5, 5, "Test");
	Map mapCopy = map;

	CPPUNIT_ASSERT(mapCopy.GetName() == map.GetName() && mapCopy.GetWidth() == map.GetWidth() && mapCopy.GetHeight() == map.GetHeight() );
}