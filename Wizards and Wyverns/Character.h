// @file 
// @brief Header file for the Character class  
//!
// 1) List of game rules :
//!		Base attributes: 3d6 rolls, translated into a random value between 3 and 18 (1,1,1) and (6,6,6)
//!		Attribute modifiers: (ability score - 10) / 2 rounded to the lowest value
//!		Initial Hit Points: 10 (for level 1) + 1d10 (fighter's hit die) per level starting lvl 2 
//!			note: an offset of 10 was added to avoid negative Hit Points for really unlucky naked characters
//!		Max Hit Points: base Hit Points + level * Constitution Modifier
//!		Armor Class: 10 + Armor Bonus(from equipements) + Shield Bonus(from Shield) + Dexterity Modifier
//!		Base Attack Bonus: Each 5 levels you get a new strike, each strike is : level - (strike number-1)*5
//!		Attack Bonus: 
//!						If melee: Base attack bonus + Strength modifier + the weapon's attack bonus
//!						If ranged: Base attack bonus + Dexterity modifier + the weapon's attack bonus
//!		Damage Bonus:
//!						If melee: Strength modifier + the weapon's damage bonus
//!						If ranged: Dexterity modifier + the weapon's damage bonus
// 2) Brief description of the design
//!		The character class has been designed to calculate everything at character creation except
//!		for the level and class of the character, but, for now, only the fighter class has been implemented
//!		Having the futur in mind, a character object automatically refreshes its stats everytime
//!		An item has been equiped. Currently, the character is able to equip any type of item (stubs only).
//!		The character also has a fail-safe in case a character object was created with bad input (level <= 0, class != fighter)
//!		In these cases, the default level will be 1 and the default class will be fighter. The attribute
//!		validCharacter also tells if the character was valid at creation or not.
// 3) Librairies used in the code
//!		The only libraire was the STL for the vector container, because it is much more efficient than arrays

#pragma once

#include <vector>
#include <map>

#include "Class.h"
#include "Alignment.h"
#include "Slot.h"
#include "Score.h"
#include "Bonus.h"
#include "Modifier.h"
#include "DerivedStat.h"

#include "Weapon.h"
#include "Chest.h"
#include "MapObject.h"
#include "Enchantment.h"

#include "Usable.h"

using namespace std;

// Class that implements a character
class Character	: public MapObject, public AbstractUser, public AbstractInteractor
{
public:
	/// Copy constructor.
	Character( const Character & character );

	/// Parametric constructor.
	Character( const bool& playable, const int& lvl, Class const * type, Alignment const * alignment, const string& name, const int& max, const int& dmg, const map<StatType const * const, Score> & scores, const map<SlotType const * const, Slot> & worn, const ItemContainer & backpack );
	
	~Character();

	/// Accept an AbstractInteractor visitor.
	virtual void Accept( AbstractInteractor * interactor );

	/// Accept an AbstractInteractor visitor.
	virtual void Accept( AbstractInspector * inspector ) const;

	/// Equip an piece of Equipment, provided an appropriate slot exists.
	void Equip( Equipment const * equipment );

	/// Remove any Equipment currently equipped to the specified slot.
	void Unequip( SlotType const * type );

	/// Increase the amount of damage taken by this Character.
	void Hit( int damage );

	/// Increment this Character's level.
	void LevelUp();

	/// Increase one of this Character's ability scores, provided there is at least one free skill point to spend.
	void IncreaseAbilityScore( StatType const * type );

	/// Query whether or not this Character can be played by the user as a hero in a Campaign.
	const bool& IsPlayable() const;

	/// Query this Character's level.
	const int& GetLevel() const;

	/// Query this Character's Class.
	Class const * GetClass() const;

	/// Query this Character's current Alignment.
	Alignment const * GetAlignment() const;

	/// Query the number of unused skill points attached to this Character.
	const unsigned int& GetPoints() const;

	/// Event fired whenver this Character's Alignment changes.
	Event<Character const * const> OnAlignmentModified;

	/// Event fired every time this Character's level increases.
	Event<Character const * const> OnLevelUp;

	/// Event fired when this Character's attack bonuses are modified.
	Event<const vector<int>&> OnAttackBonusesModified;

	/// Event fired when this Character has no hit points left.
	Event<Character const * const> OnDie;

	/// Event fired when this Character loots a Chest.
	Event<Character const * const, Chest const * const> OnLootChest;

	/// Event fired when this Character loots another Character's backpack and worn items.
	Event<Character const * const, Character const * const> OnLootCorpse;

	/// Event fired when this Character attacks another Character.
	Event<Character const * const, Character const * const, const bool&, const int&, const int&, const int&> OnAttack;

	/// Query whether or not this Character is valid.
	bool Validate();

	/// Query whether or not this MapObject has a constant position on a Map.
	virtual bool IsFixed() const { return false; }

	/// Query one of this Character's ability scores.
	const Score& GetAbilityScore( StatType const * const type ) const;

	/// Query one of this Character's ability modifiers.
	const Modifier& GetAbilityModifier( StatType const * const type ) const;

	/// Query the compound bonus conferred to a stat by all of this Character's worn items.
	const Bonus& GetEquipmentBonus( StatType const * const type ) const;

	/// Query one of this Character's derived statistics.
	const DerivedStat& GetStat( StatType const * const type ) const;

	/// Query this Character's attack bonuses.
	const vector<int>& GetAttackBonuses() const;

	/// Query this Character's worn item slots.
	const map<SlotType const * const, Slot>& GetWorn() const;

	/// Query one of this Character's worn item slots.
	const Slot& GetSlot( SlotType const * const type ) const;
	
	/// Get a mutable reference to this Character's backpack.
	ItemContainer & AccessBackpack();

	/// Get a const-qualified pointer to this Character's backpack.
	ItemContainer const * const GetBackpack() const;

	/// Assignment operator defiend to deeply copy all of another Character's statistics, worn items, and backpack contents.
	void operator = ( const Character & character );

protected:
	
	/// Use an Item.
	virtual void Use( Item const * item );

	/// Use a piece of Equipment.
	virtual void Use( Equipment const * equipment );

	/// Interact with a MapObject (no action).
	virtual void Interact( MapObject * object );

	/// Interact with another Character. If the target is alive, attacks are attempted, otherwise its corpse is looted.
	virtual void Interact( Character * character );

	/// Interact with a Chest.
	virtual void Interact( Chest * chest );

private:
	void HandleAbilityScoreModified( Score const * const score );
	void HandleAbilityBonusModified( Bonus const * const bonus );
	void HandleStrengthModifierModified( Modifier const * const modifier );
	void HandleDexterityModifierModified( Modifier const * const modifier );
	void HandleAttackBonusBonusModified( Bonus const * const stat);
	void HandleDamageBonusBonusModified( Bonus const * const bonus );
	void HandleArmorClassBonusModified( Bonus const * const bonus );
	void HandleHPFactorModified( DerivedStat const * const stat );
	void HandleEquip( Equipment const * const equipment );
	void HandleUnequip( Equipment const * const equipment );

	void CalculateModifiers();
	void CalculateModifier( StatType const * const type );
	void CalculateHitPoints();
	void CalculateAttacks();
	void CalculateDamageBonus();
	void CalculateArmorClass();

	void BuildAbilities( const map<StatType const * const, Score> & scores );
	void BuildAbility( StatType const * const type, const map<StatType const * const, Score> & scores );

	void BuildStats( const int & max, const int & dmg );
	DerivedStat& BuildStat( StatType const * const type, const int& value );

	void BuildSlots();
	Slot& BuildSlot( SlotType const * const type );

	void BuildBonuses();
	Bonus& BuildBonus( StatType const * const type );

	int _level;
	unsigned int _points;
	
	map<StatType const * const, Score> _scores;
	map<StatType const * const, Modifier> _modifiers;
	map<StatType const * const, Bonus> _bonuses;
	map<StatType const * const, DerivedStat> _stats;
	map<SlotType const * const, Slot> _worn;

	vector<int> _attacks;

	ItemContainer _backpack;

	const bool _playable;
	Class const * _type;
	Alignment const * _alignment;
};


inline ostream& operator<< ( ostream& stream, const Character& character )
{
	stream << "Name: " << character.GetName() << endl;
	stream << "Class: " << character.GetClass()->name << endl;
	stream << "Level: " << character.GetLevel() << endl;

	stream << endl;

	stream << Stats().MaxHitPoints->name << ": " << character.GetStat( Stats().MaxHitPoints ) << endl;
	stream << Stats().HitPoints->name << ": " << character.GetStat( Stats().HitPoints ) << endl;

	stream << endl;

	stream << Stats().Strength->name << " Score: " << character.GetAbilityScore( Stats().Strength ) << endl;
	stream << Stats().Dexterity->name << " Score: " << character.GetAbilityScore( Stats().Dexterity ) << endl;
	stream << Stats().Constitution->name << " Score: " << character.GetAbilityScore( Stats().Constitution ) << endl;
	stream << Stats().Intelligence->name << " Score: " << character.GetAbilityScore( Stats().Intelligence ) << endl;
	stream << Stats().Wisdom->name << " Score: " << character.GetAbilityScore( Stats().Wisdom ) << endl;
	stream << Stats().Charisma->name << " Score: " << character.GetAbilityScore( Stats().Charisma ) << endl;

	stream << endl;

	stream << Stats().Strength->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().Strength ) << endl;
	stream << Stats().Dexterity->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().Dexterity ) << endl;
	stream << Stats().Constitution->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().Constitution ) << endl;
	stream << Stats().Intelligence->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().Intelligence ) << endl;
	stream << Stats().Wisdom->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().Wisdom ) << endl;
	stream << Stats().Charisma->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().Charisma ) << endl;
	stream << endl;
	stream << Stats().AttackBonus->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().AttackBonus ) << endl;
	stream << Stats().DamageBonus->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().DamageBonus ) << endl;
	stream << Stats().ArmorClass->name << " Eq. Bonus: " << character.GetEquipmentBonus( Stats().ArmorClass ) << endl;

	stream << endl;

	stream << Stats().Strength->name << " Modifier: " << character.GetAbilityModifier( Stats().Strength ) << endl;
	stream << Stats().Dexterity->name << " Modifier: " << character.GetAbilityModifier( Stats().Dexterity ) << endl;
	stream << Stats().Constitution->name << " Modifier: " << character.GetAbilityModifier( Stats().Constitution ) << endl;
	stream << Stats().Intelligence->name << " Modifier: " << character.GetAbilityModifier( Stats().Intelligence ) << endl;
	stream << Stats().Wisdom->name << " Modifier: " << character.GetAbilityModifier( Stats().Wisdom ) << endl;
	stream << Stats().Charisma->name << " Modifier: " << character.GetAbilityModifier( Stats().Charisma ) << endl;

	stream << endl;

	stream << Stats().ArmorClass->name << ": " << character.GetStat( Stats().ArmorClass ) << endl;
	stream << Stats().DamageBonus->name << ": " << character.GetStat( Stats().DamageBonus ) << endl;

	for ( unsigned int i = 0; i < character.GetAttackBonuses().size(); i++ )
	{
		stream << Stats().AttackBonus->name << " [" << i << "]: " << character.GetAttackBonuses()[i] << endl;
	}

	return stream;
}