#include "ItemContainerReader.h"

#pragma warning(disable:4996)

void ReadItemContainer( FILE * file, const int & n, const vector<Item*>& items, ItemContainer& destination )
{
	int index = 0, qty = 0;

	for ( int i = 0; i < n; i++ )
	{
		fscanf( file, "%i#%i ", &index, &qty );

		destination.AddItem( items.at( index ), qty );
	}
}
