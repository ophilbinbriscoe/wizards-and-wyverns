#pragma once

#include "MessageController.h"
#include "Game.h"

class VictoryMessageController : public MenuController
{
public:
	VictoryMessageController( Game * game );
	virtual ~VictoryMessageController();

	Event<const bool&> OnChoose;

protected:
	Game * _game;

	void HandleClickOK();

	void HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke );

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );
};

