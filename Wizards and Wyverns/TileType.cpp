#include "TileType.h"

TileType const * const TileType::Free = new TileType( "Free", sf::Color( 32, 32, 32 ), sf::Color( 16, 16, 16 ), 90 );
TileType const * const TileType::Wall = new TileType( "Wall", sf::Color( 128, 128, 128 ), sf::Color( 128, 128, 128 ), 91 );
TileType const * const TileType::Entrance = new TileType( "Entrance", sf::Color( 32, 32, 64 ), sf::Color( 16, 16, 16 ), 92 );
TileType const * const TileType::Exit = new TileType( "Exit", sf::Color( 32, 64, 32 ), sf::Color( 16, 16, 16 ), 93 );

TileType::TileType( const string& name, const sf::Color& color, const sf::Color& outline, const unsigned int enumerator ) : DomainConcept( name, "", enumerator ), fill( color ), outline( outline )
{}

TileType::~TileType()
{}