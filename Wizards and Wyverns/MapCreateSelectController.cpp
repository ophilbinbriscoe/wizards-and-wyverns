#include "MapCreateSelectController.h"
#include "MapCreationController.h"

#include "Database.h"

MapCreateSelectController::MapCreateSelectController() : CreateSelectMenuController( Data().maps.GetContents() )
{
	auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL );

	auto label = sfg::Label::Create( "Width" );

	_width = sfg::Entry::Create( "" );
	_width->SetRequisition( { 100.0f, 0.0f } );

	box->Pack( label );
	box->Pack( _width );

	_fields->Pack( box );

	box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL );

	label = sfg::Label::Create( "Height" );

	_height = sfg::Entry::Create( "" );
	_height->SetRequisition( { 100.0f, 0.0f } );

	box->Pack( label );
	box->Pack( _height );

	_fields->Pack( box );

	PackDefaults();

	Finalize();
}

MapCreateSelectController::~MapCreateSelectController()
{}

bool MapCreateSelectController::Prevalidate()
{
	if ( CreateSelectMenuController::Prevalidate() )
	{
		int w = (int) strtol( ((string) _width->GetText()).c_str(), (char **) 0, 10 ) > 0;
		int h = (int) strtol( ((string) _height->GetText()).c_str(), (char **) 0, 10 ) > 0;

		if ( w > 0 && h > 0 )
		{
			return true;
		}
		else
		{
			_error = "Width and height must be greater than zero.";
		}
	}

	return false;
}

void MapCreateSelectController::Edit( Map * map ) const
{
	Push( new MapCreationController( map, false ) );
}

void MapCreateSelectController::Create() const
{
	int w = (int) strtol( ((string) _width->GetText()).c_str(), nullptr, 10 );
	int h = (int) strtol( ((string) _height->GetText()).c_str(), nullptr, 10 );

	Push( new MapCreationController( new Map( w, h, _name->GetText() ), true ) );
}