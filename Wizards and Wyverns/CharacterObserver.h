#pragma once

#include "TraitsObserver.h"
#include "HitPointObserver.h"
#include "AbilitiesObserver.h"
#include "ArmorClassObserver.h"
#include "AttacksObserver.h"
#include "DamageBonusObserver.h"

namespace view
{
	class CharacterObserver : public View, public Observer<Character>
	{
	public:
		CharacterObserver( shared_ptr<sfg::Box> container );
		virtual ~CharacterObserver();

	private:
		virtual void StartObserving();
		virtual void StopObserving();

		shared_ptr<sfg::Box> _box;

		TraitsObserver * _traitsObserver;
		HitPointObserver * _hitPointObserver;
		AbilitiesObserver * _abilitiesObserver;
		ArmorClassObserver * _armorClassObserver;
		AttacksObserver * _attacksObserver;
		DamageBonusObserver * _damageBonusObserver;
	};
}