#pragma once

#include "MenuController.h"

class MainMenuController : public MenuController
{
public:
	MainMenuController();
	virtual ~MainMenuController();

protected:
	void HandleClickPlay();
	void HandleClickCamC();
	void HandleClickMapC();
	void HandleClickChaC();
	void HandleClickEquC();
	void HandleClickQuit();
};

