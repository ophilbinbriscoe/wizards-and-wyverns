#pragma once

#include "Character.h"
#include "ItemObserver.h"
#include "View.h"

namespace view
{
	class TraitsObserver : public View, public Observer<Character>
	{
	public:
		TraitsObserver( shared_ptr<sfg::Box> container );
		virtual ~TraitsObserver();

	private:
		virtual void StartObserving();
		virtual void StopObserving();

		void HandleLevelModified( Character const * const character );

		void SetClassLevelLabel();

		shared_ptr<sfg::Label> _labelClassLevel;
	};
}