#include "MainMenuController.h"

#include "HeroSelectionController.h"
#include "ItemCreationController.h"
#include "CharacterCreationController.h"
#include "MapCreateSelectController.h"
#include "CampaignCreationController.h"
#include "Database.h"

MainMenuController::MainMenuController() : MenuController()
{
	auto buttonPlay = sfg::Button::Create( "Play" );
	auto buttonCamC = sfg::Button::Create( "Campaign Creator" );
	auto buttonMapC = sfg::Button::Create( "Map Creator" );
	auto buttonChaC = sfg::Button::Create( "Character Creator" );
	auto buttonEquC = sfg::Button::Create( "Equipment Creator" );
	auto buttonQuit = sfg::Button::Create( "Quit" );

	buttonPlay->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MainMenuController::HandleClickPlay, this ) );
	buttonCamC->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MainMenuController::HandleClickCamC, this ) );
	buttonMapC->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MainMenuController::HandleClickMapC, this ) );
	buttonChaC->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MainMenuController::HandleClickChaC, this ) );
	buttonEquC->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MainMenuController::HandleClickEquC, this ) );
	buttonQuit->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &MainMenuController::HandleClickQuit, this ) );

	_menu->Pack( buttonPlay );
	_menu->Pack( buttonCamC );
	_menu->Pack( buttonMapC );
	_menu->Pack( buttonChaC );
	_menu->Pack( buttonEquC );
	_menu->Pack( buttonQuit );

	Finalize();
}

MainMenuController::~MainMenuController()
{}

void MainMenuController::HandleClickPlay()
{
	Push( new HeroSelectionController( Data().characters ) );
}

void MainMenuController::HandleClickCamC()
{
	Push( new CampaignCreationController() );
}

void MainMenuController::HandleClickMapC()
{
	Push( new MapCreateSelectController() );
}

void MainMenuController::HandleClickChaC()
{
	Push( new CharacterCreationController() );
}

void MainMenuController::HandleClickEquC()
{
	Push( new ItemCreationController() );
}

void MainMenuController::HandleClickQuit()
{
	Pop( this );
}


