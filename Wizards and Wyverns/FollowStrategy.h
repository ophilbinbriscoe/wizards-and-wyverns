#pragma once

#include "Strategy.h"

class FollowStrategy : public BarrierStrategy<Position>
{
public:
	FollowStrategy( Character const * target = nullptr );
	virtual ~FollowStrategy();

	virtual void RequestDecision( Character const * character, Map const * map );

protected:
	Character const * _target;
};

