#pragma once

#include <vector>

#include "DomainConcept.h"

class StatType : public DomainConcept
{
public:
	struct StatTypeData
	{
		// Abilities
		StatType const * const Strength;
		StatType const * const Dexterity;
		StatType const * const Constitution;
		StatType const * const Intelligence;
		StatType const * const Wisdom;
		StatType const * const Charisma;

		// Hit Points
		StatType const * const MaxHitPoints;
		StatType const * const HitPoints;
		StatType const * const DamageTaken;

		// Derived Stats
		StatType const * const AttackBonus;
		StatType const * const DamageBonus;
		StatType const * const ArmorClass;

		StatTypeData();
		~StatTypeData();
	};

	const int min, max;

private:
	StatType( const string& name, const int& min, const int& max, const unsigned int enumerator = -1 );
	StatType( const string& name, const string& abbreviation, const int& min, const int& max, const unsigned int enumerator = -1 );
	~StatType();
};

typedef vector<StatType const *> StatTypes;

const StatType::StatTypeData& Stats();

