#include "Position.h"

const Position Position::RIGHT( 1, 0 );
const Position Position::LEFT( -1, 0 );
const Position Position::UP( 0, -1 );
const Position Position::DOWN( 0, 1 );
const Position Position::ZERO( 0, 0 );

Position::Position() : x( 0 ), y( 0 )
{}

Position::Position( const int & x, const int & y ) : x( x ), y( y )
{}

Position::Position( const Position & position ) : x( position.x ), y( position.y )
{}

Position::~Position()
{}