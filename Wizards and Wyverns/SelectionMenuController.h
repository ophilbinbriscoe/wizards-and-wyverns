#pragma once

#include "MenuController.h"
#include "Dataset.h"

template <class type>
class SelectionMenuController : public MenuController
{
	typedef bool( *Filter )(type *);
public:
	SelectionMenuController( const Dataset<type *>& dataset, Filter filter = nullptr );
	virtual ~SelectionMenuController();

protected:
	void PackOptionButtons( const string& title, const string& message );

	virtual void HandleClickBack();
	virtual void HandleSelect( type * selection ) = 0;

	void HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke );

	virtual void SubscribeInputHandlers( view::Window * window );
	virtual void UnsubscribeInputHandlers( view::Window * window );

private:
	Dataset<type*> const * _dataset;

	Filter _filter;

	void CreateOption( type * option, shared_ptr<sfg::Box> box );
};

template <class type>
SelectionMenuController<type>::SelectionMenuController( const Dataset<type *>& dataset, Filter filter ) : _dataset( &dataset ), _filter( filter )
{}

template <class type>
SelectionMenuController<type>::~SelectionMenuController()
{}

template<class type>
inline void SelectionMenuController<type>::PackOptionButtons( const string& title, const string& message )
{
	if ( _dataset->GetCount() > 0 )
	{
		auto frame = sfg::Frame::Create( title );

		frame->SetAlignment( { 0.5f, 0.0f } );

		auto entries = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

		if ( _filter )
		{
			for ( auto entry : _dataset->GetContents() )
			{
				if ( _filter( entry ) )
				{
					CreateOption( entry, entries );
				}
			}
		}
		else
		{
			for ( auto entry : _dataset->GetContents() )
			{
				CreateOption( entry, entries );
			}
		}

		frame->Add( entries );

		_menu->Pack( frame );
	}
	else
	{
		_menu->Pack( sfg::Label::Create( message ) );
	}

	auto buttonBack = sfg::Button::Create( "Back" );

	buttonBack->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &SelectionMenuController::HandleClickBack, this ) );

	_menu->Pack( buttonBack );
}

template <class type>
void SelectionMenuController<type>::HandleClickBack()
{
	Pop( this );
}

template<class type>
inline void SelectionMenuController<type>::CreateOption( type * option, shared_ptr<sfg::Box> box )
{
	auto button = sfg::Button::Create( option->GetName() );

	button->GetSignal( sfg::Button::OnLeftClick ).Connect( [&, ptr = option] ()->void { HandleSelect( ptr ); } );

	box->Pack( button );
}

template<class type>
void SelectionMenuController<type>::HandleKeyPressed( view::Window * window, sf::Event::KeyEvent ke )
{
	if ( ke.code == sf::Keyboard::Escape )
	{
		HandleClickBack();
	}
}

template<class type>
void SelectionMenuController<type>::SubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Subscribe( this, &SelectionMenuController::HandleKeyPressed );
}

template<class type>
void SelectionMenuController<type>::UnsubscribeInputHandlers( view::Window * window )
{
	window->OnKeyPressed.Unsubscribe( this, &SelectionMenuController::HandleKeyPressed );
}


