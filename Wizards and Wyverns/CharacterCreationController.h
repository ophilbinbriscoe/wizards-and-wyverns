#pragma once
#include "Character.h"
#include "MenuController.h"

#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/Renderers.hpp>

class CharacterCreationController : public MenuController
{
public:
	CharacterCreationController();
	virtual ~CharacterCreationController();

	void HandleClickQuit();
	void HandleTogglePlayable();
	
	void CreateCharacter();

private:
	sfg::CheckButton::Ptr playable_button;
	sfg::ComboBox::Ptr alignment_combo;

	sfg::Entry::Ptr name_entry;
	sfg::ComboBox::Ptr class_combo;
	sfg::Entry::Ptr level_entry;

	sfg::Label::Ptr success_label;
	sfg::Label::Ptr top_mid_label;
};