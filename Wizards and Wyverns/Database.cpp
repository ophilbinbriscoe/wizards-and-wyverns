#include "Database.h"

#include "MapReader.h"
#include "CampaignReader.h"
#include "NameReader.h"
#include "ItemReader.h"
#include "CharacterReader.h"

#include "MapWriter.h"
#include "CampaignWriter.h"
#include "NameWriter.h"
#include "ItemWriter.h"
#include "CharacterWriter.h"

Database::Database()
{}

Database::~Database()
{}

void Database::SaveAll()
{
	// save items
	SaveItems();

	// save characters
	SaveCharacters();

	// save maps
	SaveMaps();

	// save campaigns
	SaveCampaigns();
}

void Database::ReadAll()
{
	// items haven no dependencies, so are loaded first
	ReadItems();

	// characters, which depend only on items, are loaded next
	ReadCharacters();

	// maps, which depend on characters and items, are loaded next
	ReadMaps();

	// campaigns, which depend on maps, are loaded last
	ReadCampaigns();
}

void Database::SaveMaps()
{
	fstream file;

	MapWriter mw( file, "data/maps.dat", characters );

	ItemContainerWriter * icw = new ItemContainerWriter( items );

	for ( auto map : maps.GetContents() )
	{
		mw.Write( map, icw );
	}

	file.close();
}

void Database::ReadMaps()
{
	auto reader = MapReader( "data/maps.dat", characters.GetContents(), items.GetContents() );

	while ( reader.Read() )
	{
		maps.Add( reader.GetResult() );
	}
}

void Database::SaveCampaigns()
{
	fstream file;

	CampaignWriter cw( file, "data/campaigns.dat", maps );

	for ( auto campaign : campaigns.GetContents() )
	{
		cw.Write( campaign );
	}

	file.close();
}

void Database::ReadCampaigns()
{
	CampaignReader( "data/campaigns.dat", campaigns, maps );
}

void Database::SaveCharacters()
{
	fstream file;

	ItemContainerWriter * icw = new ItemContainerWriter( items );

	CharacterWriter pw( file, "data/characters.dat" );

	for ( auto character : characters.GetContents() )
	{
		pw.Write( "char", character, icw );
	}

	file.close();
}

void Database::ReadCharacters()
{
	auto reader = CharacterReader( "data/characters.dat", items.GetContents() );

	while ( reader.Read() )
	{
		characters.Add( reader.GetResult() );
	}
}

void Database::SaveItems()
{
	fstream file;

	ItemWriter er( file, "data/items.dat" );

	for ( auto item : items.GetContents() )
	{
		er.Write( item );
	}

	file.close();
}

void Database::ReadItems()
{
	auto reader = ItemReader( "data/items.dat" );

	while ( reader.Read() )
	{
		items.Add( reader.GetResult() );
	}
}

Database & Data()
{
	// lazily initialized pointer
	static Database * database = new Database();

	return *database;
}