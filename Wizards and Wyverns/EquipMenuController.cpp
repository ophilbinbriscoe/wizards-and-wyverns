#include "EquipMenuController.h"

EquipMenuController::EquipMenuController( ItemContainer const * container, SlotType const * type ) : MenuController(), _type( type )
{
	_combo = sfg::ComboBox::Create();

	for ( auto pair : container->GetItems() )
	{
		pair.first->Accept( this );
	}
	
	auto box = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL, 10.0f );

	auto buttonBack = sfg::Button::Create( "Back" );

	buttonBack->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &EquipMenuController::HandleBack, this ) );
	
	box->Pack( buttonBack );

	if ( _options.size() > 0 )
	{
		auto buttonEquip = sfg::Button::Create( "Equip" );

		buttonEquip->GetSignal( sfg::Button::OnLeftClick ).Connect( bind( &EquipMenuController::HandleEquip, this ) );

		box->Pack( buttonEquip );

		_combo->SelectItem( 0 );

		_menu->Pack( _combo );
	}
	else
	{
		_menu->Pack( sfg::Label::Create( string( "No equipment found for " ) + type->name + string( "." ) ) );
	}

	_menu->Pack( box );

	Finalize();
}

EquipMenuController::~EquipMenuController()
{}

void EquipMenuController::Use( Item const * item )
{
	// do nothing
}

void EquipMenuController::Use( Equipment const * equipment )
{
	if ( equipment->slot == _type )
	{
		_options.push_back( equipment );
		_combo->AppendItem( equipment->GetName() );
	}
}

void EquipMenuController::HandleEquip()
{
	_cancel = false;

	_selection = _options[_combo->GetSelectedItem()];

	Pop( this );
}

void EquipMenuController::HandleBack()
{
	_cancel = true;

	Pop( this );
}