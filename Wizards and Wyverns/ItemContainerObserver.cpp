#include "ItemContainerObserver.h"

view::ItemContainerObserver::ItemContainerObserver( shared_ptr<sfg::Box> container ) : View( container )
{}

view::ItemContainerObserver::~ItemContainerObserver()
{}

void view::ItemContainerObserver::StartObserving()
{
	_container->RemoveAll();

	_itemBox = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	for ( auto pair : _target->GetItems() )
	{
		AddItemView( pair.first );
	}

	_container->Pack( _itemBox, false, false );

	_target->OnAddItem.Subscribe( this, &ItemContainerObserver::HandleItemAdd );
	_target->OnRemoveItem.Subscribe( this, &ItemContainerObserver::HandleItemRemove );
	_target->OnItemQuantityModified.Subscribe( this, &ItemContainerObserver::HandleItemQuantityModified );
}

void view::ItemContainerObserver::StopObserving()
{
	_target->OnAddItem.Unsubscribe( this, &ItemContainerObserver::HandleItemAdd );
	_target->OnRemoveItem.Unsubscribe( this, &ItemContainerObserver::HandleItemRemove );
	_target->OnItemQuantityModified.Unsubscribe( this, &ItemContainerObserver::HandleItemQuantityModified );

	for ( auto pair : _itemViews )
	{
		RemoveItemView( pair.first );
	}

	_itemViews.clear();

	_itemBox->SetRequisition( { 0.0f, 0.0f } );
	_container->SetRequisition( { 0.0f, 0.0f } );
}

void view::ItemContainerObserver::HandleItemAdd( Item const * item )
{
	AddItemView( item );
}

void view::ItemContainerObserver::HandleItemRemove( Item const * item )
{
	RemoveItemView( item );

	_itemViews.erase( item );
}

void view::ItemContainerObserver::HandleItemQuantityModified( Item const * item )
{
	UpdateItemQuantity( item );
}

void view::ItemContainerObserver::AddItemView( Item const * item )
{
	auto box = sfg::Box::Create( sfg::Box::Orientation::HORIZONTAL );

	auto quantity = sfg::Label::Create( to_string( _target->GetItemQuantity( item ) ) );

	quantity->SetAlignment( { 1.0f, 0.0f } );

	auto observer = new ItemObserver( box );

	observer->Observe( item );

	box->Pack( quantity );

	_itemBox->Pack( box, false, false );

	_itemViews.insert( pair<Item const *, ItemView>( item, ItemView( observer, quantity, box ) ) );
}

void view::ItemContainerObserver::RemoveItemView( Item const * item )
{
	auto tuple = _itemViews.at( item );

	get<0>( tuple )->Observe( nullptr );

	delete get<0>( tuple );

	get<2>( tuple )->Show( false );

	_itemBox->Remove( get<2>( tuple ) );

	_itemBox->SetRequisition( { 0.0f, 0.0f } );
}

void view::ItemContainerObserver::UpdateItemQuantity( Item const * item )
{
	get<1>( _itemViews.at( item ) )->SetText( to_string( _target->GetItemQuantity( item ) ) );
}