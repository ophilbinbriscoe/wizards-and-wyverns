#pragma once

#include "Stat.h"

class Score : public Stat<Score>
{
public:
	Score( StatType const * const type, const int& score );
	~Score();

	static int ToModifier( const int& score );

	virtual const int Min() const { return 1; }
	virtual const int Max() const { return 20; }

	operator int() const { return _value; }

	void operator = ( const Score& score );
	void operator = ( const int& value );
	void operator+= ( const int& value );
	void operator-= ( const int& value );
};

inline int Score::ToModifier( const int& score )
{
	return (score / 2) -5;
}

inline void Score::operator=( const Score & score )
{
	Value::operator=( score._value );
}

inline void Score::operator= ( const int& value )
{
	Value::operator=( value );
}

inline void Score::operator+= ( const int& value )
{
	Value::operator+=( value );
}

inline void Score::operator-= ( const int& value )
{
	Value::operator-=( value );
}

