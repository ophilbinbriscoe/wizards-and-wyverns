#pragma once

#include "MapAdjuster.h"

class CopyAdjuster : public MapAdjuster
{
public:
	CopyAdjuster();
	virtual ~CopyAdjuster();

protected:
	virtual void Adjust( Chest const * chest, const Position & position, Map * map );
	virtual void Adjust( CharacterProxy const * proxy, const Position & position, Map * map );
};

