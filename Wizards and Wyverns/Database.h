#pragma once

#include "Map.h"
#include "Character.h"
#include "Equipment.h"
#include "Campaign.h"

#include "Dataset.h"

class Database
{
public:
	Database();
	~Database();

	Dataset<Map *> maps;
	Dataset<Campaign *> campaigns;
	Dataset<Character *> characters;
	Dataset<Item *> items;

	void SaveAll();
	void ReadAll();

protected:
	void SaveMaps();
	void ReadMaps();
	void SaveCampaigns();
	void ReadCampaigns();
	void SaveCharacters();
	void ReadCharacters();
	void SaveItems();
	void ReadItems();
};

Database& Data();