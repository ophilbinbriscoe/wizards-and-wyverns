#include "LevelBasedAdjuster.h"

#include "CharacterBuilder.h"
#include "CharacterProxy.h"

LevelBasedAdjuster::LevelBasedAdjuster( Character const * target ) : _target( target )
{}

LevelBasedAdjuster::~LevelBasedAdjuster()
{}

void LevelBasedAdjuster::Adjust( Chest const * chest, const Position & position, Map * map )
{
	// create a copy of the original Chest
	Chest * clone = new Chest( chest );

	// add the copy to the adjusted Map, at the same position as the original
	map->AddObjectAt( clone, position );

	// notify listeners of a newly spawned Chest
	OnSpawnChest( clone );
}

void LevelBasedAdjuster::Adjust( CharacterProxy const * proxy, const Position & position, Map * map )
{
	// get a pointer to the Character pointed to by the Proxy
	auto character = proxy->GetReal();

	// create a CharacterBuilder to clone the Character, so that the databse version is not modified during play
	CharacterBuilder builder( character->IsPlayable(), _target->GetLevel(), character->GetClass(), proxy->GetOverride(), character->GetName() );

	// copy ability scores
	builder.CopyScoresFrom( character );

	// copy hit points
	builder.RollHitPoints();

	// copy backpack
	builder.CopyBackpackFrom( character );

	// copy worn items
	builder.CopyWornFrom( character );

	// create the clone
	Character * clone = builder.Create();

	// add the copy to the adjusted Map, at the same position as the original
	map->AddObjectAt( clone, position);

	// notify listeners of a newly spawned Character
	OnSpawnCharacter( clone );
}
