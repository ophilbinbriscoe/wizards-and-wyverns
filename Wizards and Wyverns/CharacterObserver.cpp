#include "CharacterObserver.h"

#include "StatFormatter.h"

view::CharacterObserver::CharacterObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	_box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.0f );

	_traitsObserver = new TraitsObserver( _box );

	_box->Pack( sfg::Separator::Create(), false, false );

	_hitPointObserver = new HitPointObserver( _box );

	_box->Pack( sfg::Separator::Create(), false, false );

	_abilitiesObserver = new AbilitiesObserver( _box );
	
	_box->Pack( sfg::Separator::Create(), false, false );

	_armorClassObserver = new ArmorClassObserver( _box );

	_box->Pack( sfg::Separator::Create(), false, false );

	_attacksObserver = new AttacksObserver( _box );

	_box->Pack( sfg::Separator::Create(), false, false );

	_damageBonusObserver = new DamageBonusObserver( _box );

	_container->Pack( _box, false, false );
}

view::CharacterObserver::~CharacterObserver()
{
	delete _traitsObserver;
	delete _hitPointObserver;
	delete _abilitiesObserver;
	delete _armorClassObserver;
	delete _attacksObserver;
	delete _damageBonusObserver;

	_container->Remove( _box );
}

void view::CharacterObserver::StartObserving()
{
	_box->Show( true );

	_traitsObserver->Observe( _target );
	_hitPointObserver->Observe( _target );
	_abilitiesObserver->Observe( _target );
	_armorClassObserver->Observe( _target );
	_attacksObserver->Observe( _target );
	_damageBonusObserver->Observe( _target );
}

void view::CharacterObserver::StopObserving()
{
	_box->Show( false );

	_traitsObserver->Observe( nullptr );
	_hitPointObserver->Observe( nullptr );
	_abilitiesObserver->Observe( nullptr );
	_armorClassObserver->Observe( nullptr );
	_attacksObserver->Observe( nullptr );
	_damageBonusObserver->Observe( nullptr );
}