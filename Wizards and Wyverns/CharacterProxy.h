#pragma once

#include "Character.h"
#include "Proxy.h"

class CharacterProxy : public Proxy<const Character>, public MapObject
{
public:
	CharacterProxy( Character const * character );
	~CharacterProxy();

	void SetOverride( Alignment const * alignment );
	Alignment const * GetOverride() const { return _override; };

	Event<CharacterProxy const * const> OnAlignmentModified;

	virtual bool IsFixed() const { return _real->IsFixed(); }

	virtual void Accept( AbstractInteractor * interactor );
	virtual void Accept( AbstractInspector * inspector ) const;

private:
	Alignment const * _override;
};

