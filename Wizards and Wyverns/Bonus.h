#pragma once

#include "Stat.h"

class Bonus : public Stat<Bonus>
{
public:
	Bonus( StatType const * const type, const int& score );
	~Bonus();

	operator int() const { return _value; }

	virtual const int Min() const { return 0; }
	virtual const int Max() const { return INT32_MAX; }

	void operator = ( const Bonus& bonus );
	void operator = ( const int& value );
	void operator+= ( const int& value );
	void operator-= ( const int& value );
};

inline void Bonus::operator=( const Bonus & bonus )
{
	Value::operator=( bonus._value );
}

inline void Bonus::operator= ( const int& value )
{
	Value::operator=( value );
}

inline void Bonus::operator+= ( const int& value )
{
	Value::operator+=( value );
}

inline void Bonus::operator-= ( const int& value )
{
	Value::operator-=( value );
}

