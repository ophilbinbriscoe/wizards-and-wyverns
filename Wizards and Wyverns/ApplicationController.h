#pragma once

#include <stack>
#include <queue>

#include "Window.h"
#include "GameController.h"
#include "ItemCreationController.h"
#include "CharacterCreationController.h"

class ApplicationController
{
public:
	ApplicationController();
	~ApplicationController();

protected:
	bool _terminate;
	bool _collapse;

	void HandleCollapse();

	void EnqueuePush( Controller * controller );
	void EnqueuePop( Controller * controller );

	void Collapse();

	void Push( Controller* controller );
	void Pop( Controller * controller );

	view::Window* _window;

	stack<Controller*> _stack;

	queue<tuple<Controller*, bool>> _queue;
};

