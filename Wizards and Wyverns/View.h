#pragma once

#include "Window.h"

class View
{
public:
	View( shared_ptr<sfg::Box> container );
	virtual ~View();

	void SetContainer( shared_ptr<sfg::Box> container );
	shared_ptr<sfg::Box> GetContainer() const;
protected:
	shared_ptr<sfg::Box> _container;
};

