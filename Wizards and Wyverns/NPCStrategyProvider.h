#pragma once

#include "StrategyProvider.h"

class NPCStrategyProvider : public StrategyProvider
{
public:
	NPCStrategyProvider( BarrierStrategy<Position> * movement, BarrierStrategy<MapObject const *> * aggressor, BarrierStrategy<MapObject const *> * friendly, FreeStrategy<Item const *> * equip, FreeStrategy<SlotType const *> * unequip, FreeStrategy<StatType const *> * level );
	virtual ~NPCStrategyProvider();

	virtual BarrierStrategy<Position> * GetMovementStrategy( Character const * character ) const;
	virtual BarrierStrategy<MapObject const *> * GetInteractionStrategy( Character const * character ) const;
	virtual FreeStrategy<Item const *> * GetEquipStrategy( Character const * character ) const;
	virtual FreeStrategy<SlotType const *> * GetUnequipStrategy( Character const * character ) const;
	virtual FreeStrategy<StatType const *> * GetLevelStrategy( Character const * character ) const;

private:
	BarrierStrategy<Position> * _movement;
	BarrierStrategy<MapObject const * > * _aggressor;
	BarrierStrategy<MapObject const * > * _friendly;
	FreeStrategy<Item const *> * _equip;
	FreeStrategy<SlotType const *> * _unequip;
	FreeStrategy<StatType const *> * _level;
};

