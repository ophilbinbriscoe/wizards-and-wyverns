#pragma once

#include "SlotType.h"
#include "Event.h"
#include "Equipment.h"

class Slot
{
public:
	Slot( SlotType const * const type );
	~Slot();

	bool IsEmpty() const;

	bool Equip( Equipment const * equipment );

	void Unequip();

	Equipment const * GetEquipped() const;

	Event<Equipment const * const> OnEquip;
	Event<Equipment const * const> OnUnequip;

	SlotType const * const type;

	unsigned int IncreaseTo( StatType const * const type ) const;

private:
	Equipment const * _equipped;
};

