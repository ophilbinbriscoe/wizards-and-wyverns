#pragma once

// this is a defintion of the Visitor pattern to avoid having to use dynamic_cast when
// the full derrived type of an Interactable needs to be known

class AbstractInteractor;

class Interactable
{
public:
	Interactable();
	virtual ~Interactable();

	virtual void Accept( AbstractInteractor * interactor ) = 0;
};

class MapObject;
class Character;
class Chest;

class AbstractInteractor
{
public:
	AbstractInteractor();
	virtual ~AbstractInteractor();

	virtual void Interact( MapObject * object );
	virtual void Interact( Character * character );
	virtual void Interact( Chest * chest );
};


