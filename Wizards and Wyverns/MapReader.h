#pragma once

#include <tuple>

#include "Reader.h"
#include "Map.h"
#include "MapObject.h"
#include "CharacterProxy.h"
#include "Chest.h"

/// Allows a Map template to be read from a file.
class MapReader : public Reader<Map *>
{
public:
	MapReader( const char* filename, const vector<Character *>& characters, const vector<Item *>& items );
	virtual ~MapReader();

	virtual bool Read();

private:
	const vector<Character *>& _characters;
	const vector<Item *>& _items;
};

