#pragma once

#include "Strategy.h"
#include "Position.h"

template <typename type>
class UserBarrierStrategy : public BarrierStrategy<type>
{
public:
	UserBarrierStrategy();
	~UserBarrierStrategy();

	virtual void RequestDecision( Character const * const character, Map const * const map );

	void Decide( const type& decision );

	void Yield();
};

template <typename type>
UserBarrierStrategy<type>::UserBarrierStrategy()
{}

template <typename type>
UserBarrierStrategy<type>::~UserBarrierStrategy()
{}

template <typename type>
void UserBarrierStrategy<type>::RequestDecision( Character const * const character, Map const * const map )
{
	_ready = false;
	_yield = false;
}

template <typename type>
void UserBarrierStrategy<type>::Decide( const type & decision )
{
	_decision = decision;
	_ready = true;
}

template <typename type>
void UserBarrierStrategy<type>::Yield()
{
	_yield = true;
	_ready = true;
}
