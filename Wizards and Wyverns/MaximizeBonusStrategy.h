#pragma once

#include "Strategy.h"

class MaximizeBonusStrategy : public FreeStrategy<Item const *>, protected AbstractUser
{
public:
	MaximizeBonusStrategy();
	~MaximizeBonusStrategy();

	virtual void RequestDecision( Character const * const character, Map const * const map );

	void MaximizeBonusFor( Character const * character, SlotType const * type );

	bool CheckIfMaxBonus( Equipment const * equipment );

protected:
	virtual void Use( Item const * item );
	virtual void Use( Equipment const * equipment );

	unsigned int _max;
	SlotType const * _type;
};

