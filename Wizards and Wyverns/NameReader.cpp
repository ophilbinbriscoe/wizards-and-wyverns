#include "NameReader.h"

#pragma warning(disable:4996)

NameReader::NameReader( const char* filename, Dataset<Item *>& dataset )
{
	//open file
	FILE * file = fopen( filename, "r" );

	char name[1024];

	while ( fgets( name, 1024, file ) != NULL )
	{
		name[strcspn( name, "\r\n" )] = 0;

		dataset.Add( new Item( name ) );
	}

	fclose( file );
}

NameReader::~NameReader()
{}