#include "Game.h"

#include "Dice.h"

Game::Game( Campaign const * const campaign, Character * const hero, StrategyProvider const * const provider, MapAdjuster * const adjuster ) : _node( campaign->GetStart() ), _adjuster( adjuster ), _provider( provider ), _hero( hero )
{
	// pack the logic segments comprising a single turn of play into a vector
	// every time Update is called, these active segment will be executed
	// if during execution, a segment is completed, the active segment will be incremented
	{
		// turn begins
		_segments.push_back( &Game::BeginTurn );

		// movement
		_segments.push_back( &Game::BeginMovement );
		_segments.push_back( &Game::PollMovement );
		_segments.push_back( &Game::EndMovement );

		// interaction (combat)
		_segments.push_back( &Game::BeginInteraction );
		_segments.push_back( &Game::PollInteraction );
		_segments.push_back( &Game::EndInteraction );

		// turn ends
		_segments.push_back( &Game::EndTurn );

		// next player gets their turn
		_segments.push_back( &Game::IncrementActivePlayer );
	}

	// listener for Characters and Chests beign spawned
	_adjuster->OnSpawnCharacter.Subscribe( this, &Game::HandleCharacterSpawned );
	_adjuster->OnSpawnChest.Subscribe( this, &Game::HandleChestSpawned );

	// store a mapping from a const-qualified pointer to the hero Character to a non-const-qualified one
	_privileged.insert( pair<MapObject const *, MapObject *>( _hero, _hero ) );

	// listen for death event from hero
	_hero->OnDie.Subscribe( this, &Game::HandleHeroDeath );
	
	// add the hero to the list of players
	AddPlayer( _hero );
}

void Game::Start ()
{
	// check that the Game hasn't already started
	if ( !_started )
	{
		// flag the Game as having been started
		_started = true;

		// setup the first Map in the Campaign
		StartMap();
	}
}

Game::~Game()
{
	_adjuster->OnSpawnCharacter.Unsubscribe( this, &Game::HandleCharacterSpawned );
	_adjuster->OnSpawnChest.Unsubscribe( this, &Game::HandleChestSpawned );
	
	_hero->OnDie.Unsubscribe( this, &Game::HandleHeroDeath );
}

void Game::Advance()
{
	// execute the active segment of turn logic
	(this->*(_segments[_segment]))();
}

void Game::SetUserMovementStrategy( BarrierStrategy<Position>* strategy )
{
	_movementStrategies.insert_or_assign( _hero, strategy );
}

void Game::SetUserInteractionStrategy( BarrierStrategy<MapObject const*>* strategy )
{
	_interactionStrategies.insert_or_assign( _hero, strategy );
}

void Game::SetUserEquipStrategy( FreeStrategy<Item const*>* strategy )
{
	_equipStrategies.insert_or_assign( _hero, strategy );
}

void Game::SetUserUnequipStrategy( FreeStrategy<SlotType const*>* strategy )
{
	_unequipStrategies.insert_or_assign( _hero, strategy );
}

void Game::SetUserLevelStrategy( FreeStrategy<StatType const*>* strategy )
{
	_levelStrategies.insert_or_assign( _hero, strategy );
}

void Game::AddPlayer( Character * character )
{
	// add player
	_players.push_back( character );

	// add strategy mapping entries for the player
	UpdateStrategies( character );

	// listen for events from player's asynchronous strategies
	_equipStrategies.at( character )->OnDecide.Subscribe( this, &Game::ProcessEquipRequest );
	_unequipStrategies.at( character )->OnDecide.Subscribe( this, &Game::ProcessUnequipRequest );
	_levelStrategies.at( character )->OnDecide.Subscribe( this, &Game::ProcessIncreaseRequest );

	// listen for looted corpse events
	_players.at(_players.size()-1)->OnLootCorpse.Subscribe(this, &Game::HandleCorpseLooted);
}

void Game::CleanUp()
{
	// iterative over all objects
	for ( auto pair : _privileged )
	{
		// check that the object is not the hero (whose strategies will be retained across Maps)
		if ( pair.second != _hero )
		{
			auto character = dynamic_cast<Character *>(pair.second);

			if ( character )
			{
				// stop listening for events from player's asynchronous strategies

				_equipStrategies.at( character )->OnDecide.Unsubscribe( this, &Game::ProcessEquipRequest );
				_unequipStrategies.at( character )->OnDecide.Unsubscribe( this, &Game::ProcessUnequipRequest );

				// erase the player's entries from the strategy mappings

				_movementStrategies.erase( character );
				_interactionStrategies.erase( character );
				_levelStrategies.erase( character );
				_equipStrategies.erase( character );
				_unequipStrategies.erase( character );
			}

			// clean up object
			delete pair.second;
		}
	}

	// clear const removal lookup
	_privileged.clear();

	// clear the list of players
	_players.clear();

	// add the hero back into the list of players
	_players.push_back( _hero );

	// add the hero back into the const removal lookup
	_privileged.insert_or_assign( _hero, _hero );

	// clean up map
	delete _map;
}

void Game::UpdateStrategies( Character * character )
{
	_movementStrategies.insert_or_assign( character, _provider->GetMovementStrategy( character ) );
	_interactionStrategies.insert_or_assign( character, _provider->GetInteractionStrategy( character ) );
	_levelStrategies.insert_or_assign( character, _provider->GetLevelStrategy( character ) );
	_equipStrategies.insert_or_assign( character, _provider->GetEquipStrategy( character ) );
	_unequipStrategies.insert_or_assign( character, _provider->GetUnequipStrategy( character ) );
}

void Game::HandleHeroDeath( Character const * const character )
{
	Over();
}

void Game::HandleCharacterSpawned( Character * character )
{
	// store a mapping from a const-qualified pointer to the Character to a non-const-qualified one
	_privileged.insert( pair<MapObject const *, MapObject *>( character, character ) );

	// add the Character to the list of players
	AddPlayer( character );
	
}

void Game::HandleChestSpawned( Chest * chest )
{
	// store a mapping from a const-qualified pointer to the Chest to a non-const-qualified one
	_privileged.insert( pair<MapObject const *, MapObject *>( chest, chest ) );
}

void Game::HandleCorpseLooted(Character const * const character, Character const * const corpse)
{
	_map->RemoveObject( corpse );
}

void Game::ProcessEquipRequest( FreeStrategy<Item const *> const * strategy, Character const * character )
{
	// verify that the request is for the player whose turn it is
	if ( character == ActivePlayer() )
	{
		// strip const access modifier
		Character * user = static_cast<Character *>(_privileged.at( character ));

		// cache the decision
		Item const * decision = strategy->GetDecision();

		// validate the decision against the player's backpack contents
		if ( character->GetBackpack()->GetItemQuantity( decision ) > 0 )
		{
			// remove the equipment from the player's backpack
			user->AccessBackpack().RemoveItem( decision, 1 );

			// equip the equipment on the player
			decision->Accept( user );
		}
		else
		{
			// do nothing
		}
	}
}

void Game::ProcessUnequipRequest( FreeStrategy<SlotType const *> const * strategy, Character const * character )
{
	// verify that the request is for the player whose turn it is
	if ( character == ActivePlayer() )
	{
		// strip const access modifier
		Character * privileged = static_cast<Character *>(_privileged.at( character ));

		// cache the decision
		SlotType const * decision = strategy->GetDecision();

		// validate the decision against the player's slot
		if ( privileged->GetSlot( decision ).IsEmpty() )
		{
			// do nothing
		}
		{
			// cache the equiped item
			Item const * equipment = privileged->GetSlot( decision ).GetEquipped();

			// unequip the item from the slot
			privileged->Unequip( decision );

			// add the equipment to the player's backpack
			privileged->AccessBackpack().AddItem( equipment, 1 );
		}
	}
}

void Game::ProcessIncreaseRequest( FreeStrategy<StatType const*> const * strategy, Character const * character )
{
	static_cast<Character *>( _privileged.at( character ) )->IncreaseAbilityScore( strategy->GetDecision() );
}

void Game::Decouple( Character * character )
{
	
}

void Game::RollInitiative()
{
	// multimap sorts itself by key value, and allows repeated keys
	multimap<unsigned int, Character *> initiative;

	// iterate over all players
	for ( auto player : _players )
	{
		// roll player's initiative, store a mapping from initiative to player
		initiative.insert( pair<unsigned int, Character *>( RollInitiative( player ), player ) );
	}

	// clear the list of players
	_players.clear();

	// iterate over the sorted mapping from initiatives to players
	for ( auto pair : initiative )
	{
		// reconstruct the list of players, ordered by initiative
		_players.push_back( pair.second );
	}
}

unsigned int Game::RollInitiative( Character * character )
{
	// return the Character's rolled initiative, calculated as 1d20 + Dexterity modifier
	return Dice::Roll( "1d20" ) + character->GetAbilityModifier( Stats().Dexterity );
}

void Game::IncrementTurnSegment()
{
	// set the active segment to the next segment, wrapping to 0 if we have reached the end of the vector
	_segment = (_segment + 1) % _segments.size();
}

void Game::IncrementActivePlayer()
{
	do
	{
		// set the active player to the next player, wrapping to 0 if we have reached the end of the vector
		_player = (_player + 1) % _players.size();
	}
	while ( _players[_player]->GetStat( Stats().HitPoints ) <= 0 );

	// move to the next turn logic segment
	IncrementTurnSegment();
}

void Game::BeginTurn()
{
	// notify listeners that a player has begun their turn
	OnBeginTurn( ActivePlayer() );

	// move to the next turn logic segment
	IncrementTurnSegment();
}

void Game::EndTurn()
{
	// notify listeners that a player's turn has ended
	OnEndTurn( ActivePlayer() );

	// move to the next turn logic segment
	IncrementTurnSegment();
}

void Game::BeginMovement()
{
	// request a movement decision from the active player's strategy
	_movementStrategies.at( ActivePlayer() )->RequestDecision( ActivePlayer(), _map );

	// notify listeners that a player has begun their movement phase
	OnBeginMovement( ActivePlayer() );

	// move to the next turn logic segment
	IncrementTurnSegment();
}

void Game::BeginInteraction()
{
	// request an ineraction decision from the active player's strategy
	_interactionStrategies.at( ActivePlayer() )->RequestDecision( ActivePlayer(), _map );

	// notify listeners that a player has begun their interaction phase
	OnBeginInteraction( ActivePlayer() );

	// move to the next turn logic segment
	IncrementTurnSegment();
}

void Game::PollMovement()
{
	// check if the active player's strategy is ready
	if ( _movementStrategies.at( ActivePlayer() )->IsReady() )
	{
		// check if the active player's strategy is yielding the remainder of this phase
		if ( _movementStrategies.at( ActivePlayer() )->WillYield() )
		{
			// notify listeners that the remainder of the phase has been forfeit
			OnYieldPhase( ActivePlayer() );
		}
		else
		{
			// handle the move
			if ( ProcessMovement( _movementStrategies.at( ActivePlayer() )->GetDecision() ) )
			{
				return;
			}
		}

		// move to the next turn logic segment
		IncrementTurnSegment();
	}
}

void Game::PollInteraction()
{
	// check if the active player's strategy is ready
	if ( _interactionStrategies.at( ActivePlayer() )->IsReady() )
	{
		// check if the active player's strategy is yielding the remainder of this phase
		if ( _interactionStrategies.at( ActivePlayer() )->WillYield() )
		{
			// notify listeners that the remainder of the phase has been forfeit
			OnYieldPhase( ActivePlayer() );
		}
		else
		{
			// handle the interaction
			ProcessInteraction( _interactionStrategies.at( ActivePlayer() )->GetDecision() );
		}

		// move to the next turn logic segment
		IncrementTurnSegment();
	}
}

void Game::EndMovement()
{
	// notify listeners that a active player's movement phase has ended
	OnEndMovement( ActivePlayer() );

	// move to the next turn logic segment
	IncrementTurnSegment();
}

void Game::EndInteraction()
{
	// notify listeners that a active player's interaction phase has ended
	OnEndInteraction( ActivePlayer() );

	// move to the next turn logic segment
	IncrementTurnSegment();
}

bool Game::ProcessMovement( const Position & position )
{
	// check if the new position requested by the active player's strategy is already occupied 
	if ( _map->IsOccupied( position ) )
	{
		// notify listeners that a collision was detected and movement blocked
		OnBlockIllegalMovement( position );
	}
	else
	{
		// compute the displacement requested by the active player's strategy
		auto delta = position - _map->GetPosition( ActivePlayer() );

		// validate the displacement
		if ( abs( delta.x ) + abs( delta.y ) == 1 )
		{
			_map->SetPosition( ActivePlayer(), position );
		}

		// check if the hero Character has just moved onto an exit
		if ( ActivePlayer() == _hero && _map->GetTileAt( position ) == TileType::Exit )
		{
			// finish the current Map
			FinishMap();

			return true;
		}
	}

	return false;
}

void Game::ProcessInteraction( MapObject const * object )
{
	// validate the adjacency of the position of interaction with the active player
	if ( _map->IsAdjacent( ActivePlayer(), _map->GetPosition( object ) ) )
	{
		// lookup the non-const-qualified pointer, and use it to perform the interaction
		_privileged.at( object )->Accept( ActivePlayer() );
	}
}

void Game::StartMap()
{
	// initialize the Map pointed to by the current CampaignNode to match the hero's level
	_map = _adjuster->Adjust( _node->GetMap() );

	// add the hero to the Map at a randomly selected entrance
	_map->AddObjectAt( _hero, _map->GetEntrance( rand() % _map->GetEntranceCount() ) );

	// determine turn order
	RollInitiative();

	// reset player enumerator
	_player = 0;

	// reset segment enumerator
	_segment = 0;

	// notify listeners that a map has been started
	OnStartMap( _map );
}

void Game::FinishMap()
{
	// notify listeners that the map has been finished
	OnFinishMap( _map );

	// clean up map and its objects
	CleanUp();

	// level up the hero
	_hero->LevelUp();

	// get the next CampaignNode
	CampaignNode* next = _node->GetNext();

	// if the next CampaignNode actually exists
	if ( next )
	{
		// move to the next CampaignNode
		_node = next;

		// start the next map
		StartMap();
	}

	// if the Campaign's nodes have bee nexhausted
	else
	{
		// end the game
		End();
	}
}

void Game::End()
{
	// flag the Game as done
	_ended = true;

	// notify listeners that the Campaign has ended
	OnFinishCampaign();
}

void Game::Over()
{
	// flag the Game as over
	_over = true;

	// notify listeners that the hero has died
	OnGameOver();
}

Character * Game::ActivePlayer() const
{
	return _players[_player];
}
