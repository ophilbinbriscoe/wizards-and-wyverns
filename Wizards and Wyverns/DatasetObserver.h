#pragma once

#include "Database.h"
#include "Observer.h"
#include "View.h"

template <class type>
class DatasetObserver : public View, public Observer<Dataset<type>>
{
public:
	DatasetObserver( shared_ptr<sfg::Box> _container );
	~DatasetObserver();

protected:
	virtual void StartObserving();
	virtual void StopObserving();

	void HandleAdd( type member );
	void HandleRemove( type member );

	void Add( type& member );
	void Remove( type& member );

private:
	map<type, ResourceObserver*> _observers;
};

template<class type>
inline DatasetObserver<type>::DatasetObserver( shared_ptr<sfg::Box> container ) : View( container )
{}

template<class type>
inline DatasetObserver<type>::~DatasetObserver()
{}

template<class type>
inline void DatasetObserver<type>::StartObserving()
{
	_container->RemoveAll();

	for ( auto member : _target->GetContents() )
	{
		Add( member );
	}
}

template<class type>
inline void DatasetObserver<type>::StopObserving()
{
	for ( auto member : _target->GetContents() )
	{
		Remove( member );
	}

	_container->RemoveAll();
}

template<class type>
inline void DatasetObserver<type>::HandleAdd( type member )
{
	Add( member );
}

template<class type>
inline void DatasetObserver<type>::HandleRemove( type member )
{
	Remove( member );
}

template<class type>
inline void DatasetObserver<type>::Add( type& member )
{
	auto observer = new ResourceObserver( _container );

	observer->Observe( static_cast<Resource*>(member) );

	_observers.insert( pair<type, ResourceObserver*>( member, observer ) );
}

template<class type>
inline void DatasetObserver<type>::Remove( type& member )
{
	auto observer = _observers.at( member );

	observer->Observe( nullptr );

	delete observer;

	_observers.erase( member );
}
