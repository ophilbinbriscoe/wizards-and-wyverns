#include "CharacterReader.h"

#include "ItemContainerReader.h"

#pragma warning(disable:4996)

const int FIGHTER = 0;

CharacterReader::CharacterReader( const char* filename, const vector<Item *>& items ) : Reader( filename ), _items( items )
{}

CharacterReader::~CharacterReader()
{}

bool CharacterReader::Read()
{
	char header[32];
	char name[1024];

	// playable flag, class, level, and alignment
	int playable = 0, type = 0, level = 0, alignment = 0;

	// hit points
	int max = 0, dmg = 0;

	// ability scores
	int str = 0, dex = 0, con = 0, itl = 0, wis = 0, cha = 0;

	// worn equipment
	int worn = 0;

	int res = fscanf( _file, "%s %i %i %i %i %i %i %i %i %i %i %i %i %i", header, &playable, &type, &level, &alignment, &max, &dmg, &str, &dex, &con, &itl, &wis, &cha, &worn );

	if ( res == EOF )
	{
		// close file stream
		fclose( _file );

		// clean pointer
		_file = nullptr;

		// notify user of EOF
		return false;
	}

	CharacterBuilder builder( playable, level, DomainConcept::Get<Class>( type ), DomainConcept::Get<Alignment>( alignment ), "" );

	builder.SetHitPoints( max, dmg );

	builder.SetScore( Stats().Strength, str );
	builder.SetScore( Stats().Dexterity, dex );
	builder.SetScore( Stats().Constitution, con );
	builder.SetScore( Stats().Intelligence, itl );
	builder.SetScore( Stats().Wisdom, wis );
	builder.SetScore( Stats().Charisma, cha );

	for ( int i = 0; i < worn; i++ )
	{
		int index = 0;

		fscanf( _file, " %i", &index );

		auto gear = dynamic_cast<Equipment *>(_items[index]);

		builder.AddWorn( gear );
	}

	// backpack items and equipment
	int n = 0, equipmentCount = 0;

	fscanf( _file, " %i", &n );

	if ( n == 0 )
	{
		fscanf( _file, " " );
	}
	else
	{
		ReadItemContainer( _file, n, _items, builder.AccessBackpack() );
	}

	fgets( name, 1024, _file );

	name[strcspn( name, "\r\n" )] = 0;

	builder.SetName( name );

	_result = builder.Create();

	return true;
}