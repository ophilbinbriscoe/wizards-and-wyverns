#include "MapBuilderDriver.h"

#include <iostream>

#include "MapReader.h"
#include "MapWriter.h"
#include "MapAdjuster.h"

#include "Database.h"

#include "ItemContainerWriter.h"

using namespace std;

int MapBuilderDriver()
{
	//...
	int height = (rand() % 9) + 1;
	int width = (rand() % 9) + 1;

	printf("Generating Map with random values\n");
	printf("Width: %d, Height: %d \n", width, height);
	Map map = Map(width, height);

	for ( int y = 0; y < height; y++ )
	{
		for ( int x = 0; x < width; x++ )
		{
			int value = (rand() % 4) + TileType::Free->Enumerator();

			map.SetTileAt( Position( x, y ), DomainConcept::Get<TileType>( value ) );
		
			printf( "%d ", map.GetTileAt( Position( x, y ) ) );
		}
	
		printf( "\n" );
	}

	printf("\n");

	fstream file;

	MapWriter writer( file, "../templates/mapWrite.txt", Data().characters );

	writer.Write( file, &map, new ItemContainerWriter( Data().items ) );

	printf("\n");

	file.close();

	MapReader::MapReader( "../templates/mapWrite.txt", Data().characters.GetContents(), Data().items.GetContents() );

	cin.get();

	return 0;
}