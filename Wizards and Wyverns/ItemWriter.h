#pragma once

#include "Writer.h"
#include "Weapon.h"

class ItemWriter : public Writer, protected AbstractUser
{
public:
	ItemWriter( fstream& file, const char * filepath );
	virtual ~ItemWriter();

	void Write( Item const * item );

	void Use( Item const * item );
	void Use( Equipment const * equipment );
};

