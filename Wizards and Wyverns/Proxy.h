#pragma once

#include "MapObject.h"

template <typename type>
class Proxy
{
public:
	Proxy( type * real );
	~Proxy();

	void SetReal( type * real );
	type const * GetReal() const;

	operator type const *() const;
	type const * operator ->();

protected:
	type * _real;
};

template<typename type>
inline Proxy<type>::Proxy( type * real ) : _real( real )
{}

template<typename type>
inline Proxy<type>::~Proxy()
{}

template<typename type>
inline void Proxy<type>::SetReal( type * real )
{
	_real = real;
}

template<typename type>
inline type const * Proxy<type>::GetReal() const
{
	return _real;
}

template<typename type>
inline Proxy<type>::operator type const*() const
{
	return _real;
}

template<typename type>
inline type const * Proxy<type>::operator->()
{
	return _real;
}