#pragma once

#include "Reader.h"
#include "CharacterBuilder.h"

class CharacterReader : public Reader<Character *>
{
public:
	CharacterReader( const char* filename, const vector<Item *>& items );
	virtual ~CharacterReader();

	virtual bool Read();

private:
	const vector<Item *>& _items;
};

