#include "MapReader.h"

#include <vector>
#include <stdio.h>
#include <string>
#include <cstring>

#include <algorithm>

#include "ItemContainerReader.h"

using namespace std;

#pragma warning(disable:4996)

MapReader::MapReader(const char* filename, const vector<Character *>& characters, const vector<Item *>& items ) : Reader( filename ), _characters( characters ), _items( items )
{}

MapReader::~MapReader()
{}

bool MapReader::Read()
{
	char header[32];
	char name[1024];

	int width = 0, height = 0, n = 0;

	int res = fscanf( _file, "%s %i %i %i ", header, &width, &height, &n );

	if ( res == EOF )
	{
		// close file stream
		fclose( _file );

		// clean pointer
		_file = nullptr;

		// notify user of EOF
		return false;
	}

	fgets( name, 1024, _file );

	name[strcspn( name, "\r\n" )] = 0;

	_result = new Map( width, height, name );

	for ( int i = 0; i < n; i++ )
	{
		int x = 0, y = 0, index = 0;

		fscanf( _file, "%s %i %i", header, &x, &y );

		if ( strcmp( header, "char:" ) == 0 )
		{
			fscanf( _file, " %i\n", &index );

			_result->AddObjectAt( new CharacterProxy( _characters[index] ), { x, y } );
		}

		else if ( strcmp( header, "chest:" ) == 0 )
		{
			int itc = 0;

			fscanf( _file, " %i", &itc );

			auto chest = new Chest();

			ReadItemContainer( _file, itc, _items, *chest );

			_result->AddObjectAt( chest, { x, y } );
		}

		fscanf( _file, "\n" );
	}

	int type = 0;

	for ( int y = 0; y < height; y++ )
	{
		for ( int x = 0; x < width; x++ )
		{
			Position position{ x, y };

			fscanf( _file, "%d ", &type );

			_result->SetTileAt( position, DomainConcept::Get<TileType>( type ) );
		}

		fscanf( _file, "\n" );
	}

	return true;
}