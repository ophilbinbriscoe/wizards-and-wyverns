#pragma once

#include "Map.h"
#include "Logger.h"
#include "Observer.h"

class MapLogger : public Logger, public Observer<Map>
{
public:
	MapLogger();
	~MapLogger();

	virtual void StartObserving();
	virtual void StopObserving();

	void OnSetPositionEventHandler( MapObject const * );
};

