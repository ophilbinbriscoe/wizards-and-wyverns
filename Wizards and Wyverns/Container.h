#pragma once

#include "Event.h"

template<typename type> class Container
{
public:
	Container();
	~Container();

	bool Add( const type element );
	bool Remove( const type element );
	bool RemoveAt( const int& index );
	const vector<type>& GetContents() const;
	int GetCount() const { return _vector.size(); }

	Event<type> OnAdd;
	Event<type> OnRemove;

	operator const vector<type>& () const { return _vector; }

protected:
	virtual bool AllowDuplicates() const = 0;
	vector<type> _vector;
};

template <typename type>
Container<type>::Container()
{}

template <typename type>
Container<type>::~Container()
{}

template<typename type>
bool Container<type>::Add( const type element )
{
	if ( AllowDuplicates() || find( _vector.begin(), _vector.end(), element ) == _vector.end() )
	{
		_vector.push_back( element );

		OnAdd( element );

		return true;
	}

	return false;
}

template<typename type>
bool Container<type>::Remove( const type element )
{
	unsigned int size = _vector.size();

	_vector.erase( remove( _vector.begin(), _vector.end(), element ), _vector.end() );

	if ( _vector.size() == size )
	{
		return false;
	}

	OnRemove( element );

	return true;
}

template<typename type>
bool Container<type>::RemoveAt( const int & index )
{
	if ( index < _vector.size() )
	{
		_vector.erase( &_vector[index] );
		return true;
	}

	return false;
}

template<typename type>
const vector<type>& Container<type>::GetContents() const
{
	return _vector;
}