#include "DamageBonusObserver.h"

#include "StatFormatter.h"

view::DamageBonusObserver::DamageBonusObserver( shared_ptr<sfg::Box> container ) : View( container )
{
	_label = sfg::Label::Create( "" );
	
	_label->SetAlignment( { 0.0f, 0.5f } );
	
	_container->Pack( _label, false, false );
}

view::DamageBonusObserver::~DamageBonusObserver()
{}

void view::DamageBonusObserver::StartObserving()
{
	_target->GetStat( Stats().DamageBonus ).OnModified.Subscribe( this, &DamageBonusObserver::HandleDamageBonusModified );
	
	_label->Show( true );

	SetLabel();
}

void view::DamageBonusObserver::StopObserving()
{	
	_target->GetStat( Stats().DamageBonus ).OnModified.Unsubscribe( this, &DamageBonusObserver::HandleDamageBonusModified );
	
	_label->Show( false );
	
	_label->SetText( "" );
}

void view::DamageBonusObserver::HandleDamageBonusModified( DerivedStat const * const stat )
{
	SetLabel();
}

void view::DamageBonusObserver::SetLabel()
{	
	_label->SetText( Stats().DamageBonus->name + string( ": +" ) + to_string( (int) _target->GetStat( Stats().DamageBonus ) ) );
}