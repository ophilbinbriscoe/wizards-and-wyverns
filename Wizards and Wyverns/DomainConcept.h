#pragma once

#include <string>
#include <map>

using namespace std;

class DomainConcept
{
public:
	template <typename type> static type const * const Get( const unsigned int& enumerator );
	const string name;
	const string abbreviation;
	unsigned int Enumerator() const;
protected:
	DomainConcept( const string& name, const string& abbreviation = "", const unsigned int& enumerator = -1 );
	~DomainConcept();
private:
	unsigned int enumerator;
	static map<unsigned int, DomainConcept *>& Concepts();
	static unsigned int next;
	static unsigned int Next();
};

template<typename type>
inline type const * const DomainConcept::Get( const unsigned int & enumerator )
{
	return static_cast<type const * const>(Concepts().at( enumerator ));
}
