#pragma once

#include "Observer.h"
#include "Resource.h"
#include "View.h"

class ResourceObserver : public View, public Observer<Resource>
{
public:
	ResourceObserver( shared_ptr<sfg::Box> container );
	virtual ~ResourceObserver();

protected:
	void HandleRename( Resource const * resource );

	virtual void StartObserving();
	virtual void StopObserving();

	shared_ptr<sfg::Label> _label;
};

