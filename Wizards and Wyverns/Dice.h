#pragma once

#include <string>

#include "Event.h"

/// Static class providing a dice-rolling service.
class Dice
{
public:
	~Dice();

	/// Singleton allowing an Observer to listen for dice-roll events
	static Dice const * const Singleton;

	Event<const string&, const unsigned int&> OnRoll;

	/// Accepts strings of the form xd+y where x and y are non-negative integers.
	static unsigned int Roll( const string& dice );

private:
	Dice();
};