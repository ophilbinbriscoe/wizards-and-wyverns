#pragma once

// this is an defintion of the Visitor pattern to avoid having to use dynamic_cast when
// the full derrived type of a Usable needs to be known

class AbstractUser;

class Usable
{
public:
	Usable();
	virtual ~Usable();

	virtual void Accept( AbstractUser * user ) const = 0;
};

class Item;
class Equipment;

class AbstractUser
{
public:
	AbstractUser();
	virtual ~AbstractUser();

	virtual void Use( Item const * item );
	virtual void Use( Equipment const * equipment );
};
