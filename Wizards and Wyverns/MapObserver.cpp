#include "MapObserver.h"

#include "SharedCanvas.h"

#include <iostream>

using namespace std;

sf::Texture * LoadFromFile( const char * filepath )
{
	sf::Texture * texture = new sf::Texture();

	texture->loadFromFile( filepath );

	return texture;
}

const sf::Texture & MonsterTexture()
{
	static sf::Texture * mtex = LoadFromFile( "textures/monster.png" );

	return *mtex;
};
const sf::Texture & DeadMonsterTexture()
{
	static sf::Texture * mtex = LoadFromFile("textures/deadmonster.png");

	return *mtex;
};

const sf::Texture & PlayerTexture()
{
	static sf::Texture * ptex = LoadFromFile( "textures/player.png" );

	return *ptex;
};

const sf::Texture & FriendlyTexture()
{
	static sf::Texture * ftex = LoadFromFile( "textures/friendly.png" );

	return *ftex;
};

const sf::Texture & ChestTexture()
{
	static sf::Texture * ctex = LoadFromFile( "textures/chest.png" );

	return *ctex;
};

const sf::Texture & TextureFor( MapObject const * object )
{
	CharacterProxy const * proxy = dynamic_cast<CharacterProxy const *>(object);

	if ( proxy )
	{

	}

	Character const * character;

	Alignment const * alignment = nullptr;

	if ( proxy )
	{
		character = proxy->GetReal();

		alignment = proxy->GetOverride();
	}
	else
	{
		character = dynamic_cast<Character const *>(object);

		if ( character )
		{
			alignment = character->GetAlignment();
		}
	}
	
	if ( character )
	{
		if ( character->IsPlayable() )
		{
			return PlayerTexture();
		}

		if ( alignment == Alignment::Evil )
		{
			if (character->GetStat(Stats().HitPoints) == 0)
			{
				return DeadMonsterTexture();
			}
			else
				return MonsterTexture();

		}

		return FriendlyTexture();
	}

	return ChestTexture();
}

namespace view
{
	MapObserver::MapObserver( shared_ptr<sfg::Box> container ) : View( container )
	{}

	MapObserver::~MapObserver()
	{}

	void MapObserver::HandleTileModified( const Position& position )
	{
		// get the view-space coordinates of the modified tile
		Position viewPosition = MapToView( position );

		// only update the view if the modified tile is actually visible
		if ( viewPosition.x >= 0 && viewPosition.y >= 0 && viewPosition.x < _rects->GetWidth() && viewPosition.y < _rects->GetHeight() )
		{
			Update( _rects->Get( viewPosition ), _target->GetTileAt( position ) );
		}

		// update the output
		Draw();
	}

	void MapObserver::HandleObjectAdded( MapObject const * object )
	{
		SubscribeToRedrawEvents( object );

		// update the output
		Draw();
	}

	void MapObserver::HandleObjectRemoved( MapObject const * object )
	{
		UnsubscribeFromRedrawEvents( object );

		// update the output
		Draw();
	}

	void MapObserver::HandleObjectMoved( MapObject const * object )
	{
		// update the output
		Draw();
	}

	void MapObserver::HandleAlignmentModified( Character const * const character )
	{
		// update the output
		Draw();
	}

	void MapObserver::HandleAlignmentModified( CharacterProxy const * const proxy )
	{
		// update the output
		Draw();
	}

	void MapObserver::SubscribeToRedrawEvents( MapObject const * object )
	{
		auto character = dynamic_cast<Character const *>(object);

		if ( character )
		{
			character->OnAlignmentModified.Subscribe( this, &MapObserver::HandleAlignmentModified );
		}
		else
		{
			auto proxy = dynamic_cast<CharacterProxy const *>(object);

			if ( proxy )
			{
				proxy->OnAlignmentModified.Subscribe( this, &MapObserver::HandleAlignmentModified );
			}
		}
	}

	void MapObserver::UnsubscribeFromRedrawEvents( MapObject const * object )
	{
		auto character = dynamic_cast<Character const *>(object);

		if ( character )
		{
			character->OnAlignmentModified.Unsubscribe( this, &MapObserver::HandleAlignmentModified );
		}
		else
		{
			auto proxy = dynamic_cast<CharacterProxy const *>(object);

			if ( proxy )
			{
				proxy->OnAlignmentModified.Unsubscribe( this, &MapObserver::HandleAlignmentModified );
			}
		}
	}

	void MapObserver::HandleClick()
	{
		auto mouse = sf::Mouse::getPosition();

		auto window = _window->GetPosition();

		auto canvas = GetSharedCanvas()->GetAbsolutePosition();

		auto window_local = mouse - window;

		auto canvas_local = window_local - sf::Vector2i( (int) canvas.x + 8, (int) canvas.y );

		auto grid_local = canvas_local / MAP_TILE_SIZEi;

		Position position = ViewToMap( { grid_local.x, grid_local.y - 1 } );

		if ( _target->Contains( position ) )
		{
			OnTileClicked( position );
		}
	}

	void MapObserver::StartObserving()
	{
		auto alignment = sfg::Alignment::Create();
		
		alignment->SetScale( { 0.0f, 0.0f } );
		alignment->SetAlignment( { 0.5f, 0.5f } );

		// set the Frame's title to reflect the Map's name
		_frame = sfg::Frame::Create( _target->GetName() );

		_frame->SetAlignment( { 0.5f, 0.0f } );

		auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 0.0f );

		_rects = new Grid<sf::RectangleShape>( min( _target->GetWidth(), 10 ), min( _target->GetHeight(), 10 ) );

		//TODO: set the offset to center the view around the Player's position

		auto alignmentBox = sfg::Alignment::Create();

		alignmentBox->SetScale( { 0.0f, 0.0f } );
		alignmentBox->SetAlignment( { 0.5f, 0.5f } );

		for ( int y = 0; y < _rects->GetHeight(); y++ )
		{
			for ( int x = 0; x < _rects->GetWidth(); x++ )
			{
				// 
				sf::RectangleShape rect( { MAP_TILE_SIZEf - 2.0f, MAP_TILE_SIZEf - 2.0f } );

				// 
				rect.setPosition( { x * MAP_TILE_SIZEf + 1.0f, y * MAP_TILE_SIZEf + 1.0f } );

				rect.setOutlineThickness( 1.0f );

				// 
				_rects->Set( { x, y }, rect );
			}
		}

		_serial = GetSharedCanvas()->GetSignal( sfg::Canvas::OnLeftClick ).Connect( bind( &MapObserver::HandleClick, this ) );

		GetSharedCanvas()->SetRequisition( { _rects->GetWidth() * MAP_TILE_SIZEf, _rects->GetHeight() * MAP_TILE_SIZEf } );
		GetSharedCanvas()->SetRequisition( { _rects->GetWidth() * MAP_TILE_SIZEf, _rects->GetHeight() * MAP_TILE_SIZEf } );

		alignmentBox->Add( GetSharedCanvas() );

		_frame->Add( alignmentBox );

		alignment->Add( _frame );

		_container->Pack( alignment, true, true );

		// subscribe to be notified any time a tile changes
		_target->OnTileModified.Subscribe( this, &MapObserver::HandleTileModified );

		// subscribe to be notified any time an object is added to or removed from the Map
		_target->OnAdd.Subscribe( this, &MapObserver::HandleObjectAdded );
		_target->OnRemove.Subscribe( this, &MapObserver::HandleObjectRemoved );
		_target->OnSetPosition.Subscribe( this, &MapObserver::HandleObjectMoved );

		for ( auto pair : _target->GetObjects() )
		{
			SubscribeToRedrawEvents( pair.first );
		}

		// update the view to reflect the state of the map
		Update();

		// update the output
		Draw();
	}

	void MapObserver::StopObserving()
	{
		// decouple
		_target->OnTileModified.Unsubscribe( this, &MapObserver::HandleTileModified );
		_target->OnAdd.Unsubscribe( this, &MapObserver::HandleObjectAdded );
		_target->OnRemove.Unsubscribe( this, &MapObserver::HandleObjectRemoved );
		_target->OnSetPosition.Unsubscribe( this, &MapObserver::HandleObjectMoved );

		for ( auto pair : _target->GetObjects() )
		{
			UnsubscribeFromRedrawEvents( pair.first );
		}

		// clear the canvas
		GetSharedCanvas()->Clear( sf::Color::Black );

		// disconnect from the canvas
		GetSharedCanvas()->GetSignal( sfg::Canvas::OnLeftClick ).Disconnect( _serial );

		// hide frame
		_frame->Show( false );

		// remove the frame
		_container->Remove( _frame->GetParent() );

		// clean up
		delete _rects;
	}

	void MapObserver::Update()
	{
		// iterate over visible cells
		for ( int y = 0; y < _rects->GetHeight(); y++ )
		{
			for ( int x = 0; x < _rects->GetWidth(); x++ )
			{
				// cache positions of cell in view and map space
				Position guiPosition( x, y );
				Position mapPosition = ViewToMap( guiPosition );

				// if the cell's map position falls withing the map's bounds, update the cell based on the relevant map tile
				if ( mapPosition.x >= 0 && mapPosition.y >= 0 && mapPosition.x < _target->GetWidth() && mapPosition.y < _target->GetHeight() )
				{
					Update( _rects->Get( guiPosition ), _target->GetTileAt( mapPosition ) );
				}

				// otherwise, set the cell's fill / border to the void color
				else
				{
					_rects->Get( guiPosition ).setFillColor( sf::Color::Black );
					_rects->Get( guiPosition ).setOutlineColor( sf::Color::Black );
				}
			}
		}
	}

	void MapObserver::Update( sf::RectangleShape & rectangle, TileType const * type )
	{
		// update RectangleShape's fill color
		rectangle.setFillColor( type->fill );

		// update RectangleShape's outline color
		rectangle.setOutlineColor( type->outline );
	}

	void MapObserver::Draw()
	{
		// bind the sf::Canvas as the current render target
		GetSharedCanvas()->Bind();

		// clear the buffer
		GetSharedCanvas()->Clear( sf::Color::Black );

		// get the view-space coordinates of the Map's extremities

		Position topLeft = MapToView( { 0, 0 } );
		Position bottomRight = MapToView( { _target->GetWidth(), _target->GetHeight() } );

		// calculate intersection of the Map region and the viewport, to get the visible Map region

		int xmin = max( 0, topLeft.x );
		int xmax = min( _rects->GetWidth(), bottomRight.x );

		int ymin = max( 0, topLeft.y );
		int ymax = min( _rects->GetHeight(), bottomRight.y );

		sf::Sprite sprite;

		float scale = (MAP_TILE_SIZEf - 2.0f) / 8.0f;

		sprite.setScale( { scale, scale } );

		// iterate over the visible Map region
		for ( int x = xmin; x < xmax; x++ )
		{
			for ( int y = ymin; y < ymax; y++ )
			{
				// draw the tile at the position
				GetSharedCanvas()->Draw( _rects->Get( { x, y } ) );

				auto object = _target->GetObjectAt( ViewToMap( { x, y } ) );

				if ( object )
				{
					sprite.setPosition( { x * MAP_TILE_SIZEf + 1.0f, y * MAP_TILE_SIZEf + 1.0f } );

					sprite.setTexture( TextureFor( object ) );

					GetSharedCanvas()->Draw( sprite );
				}
			}
		}

		// display the results
		GetSharedCanvas()->Display();

		// unbind the sf::Canvas
		GetSharedCanvas()->Unbind();
	}

	void MapObserver::SetOffset( const Position& offset )
	{
		// only apply the change if the new offset is actually different from the existing offset
		// updating everything and redrawing should only be done if necessary
		if ( offset.x != _offset.x || offset.y != _offset.y )
		{
			// store the new offset
			_offset = offset;

			if ( _target )
			{
				// update everything to reflect the new offset
				Update();

				// update the output
				Draw();
			}
		}
	}

	Position MapObserver::MapToView( const Position& position ) const
	{
		return position - _offset;
	}

	Position MapObserver::ViewToMap( const Position& position ) const
	{
		return position + _offset;
	}

	void MapObserver::SetWindow( Window const * window )
	{
		_window = window;
	}
}