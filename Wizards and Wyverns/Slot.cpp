#include "Slot.h"

Slot::Slot( SlotType const * const type ) : type( type ), _equipped( nullptr ), OnEquip()
{}

Slot::~Slot()
{}

bool Slot::IsEmpty() const
{
	return _equipped == nullptr;
}

bool Slot::Equip( Equipment const * equipment )
{
	if ( equipment->slot == type )
	{
		Unequip();

		_equipped = equipment;

		OnEquip( equipment );

		return true;
	}

	return false;
}

void Slot::Unequip()
{
	if ( _equipped )
	{
		Equipment const * equipment = _equipped;

		_equipped = nullptr;

		OnUnequip( equipment );
	}
}

Equipment const * Slot::GetEquipped() const
{
	return _equipped;
}

unsigned int Slot::IncreaseTo( StatType const * const type ) const
{
	if ( _equipped )
	{
		return _equipped->IncreaseTo( type );
	}

	return 0;
}
