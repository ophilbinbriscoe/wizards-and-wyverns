#pragma once

#include "MenuController.h"
#include "Character.h"

class SaveDiscardDialogController : public MenuController
{
public:
	SaveDiscardDialogController( Character const * character, Character * original );
	virtual ~SaveDiscardDialogController();

protected:
	Character const * _character;
	Character * _existing;

	void HandleClickDiscard();
	void HandleClickOverwrite();
};

