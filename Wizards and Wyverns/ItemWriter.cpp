#include "ItemWriter.h"

#pragma warning(disable:4996)

ItemWriter::ItemWriter( fstream& file, const char * filepath ) : Writer( file, filepath )
{}

ItemWriter::~ItemWriter()
{}

void ItemWriter::Write( Item const * item )
{
	item->Accept( this );
}

void ItemWriter::Use( Item const * item )
{
	_file << "item: " << item->GetName() << '\n';
}

void ItemWriter::Use( Equipment const * equipment )
{
	auto weapon = dynamic_cast<Weapon const *>(equipment);

	if ( weapon )
	{
		_file << "weap: " << weapon->type->Enumerator() << ' ';
	}
	else
	{
		_file << "gear: " << equipment->slot->Enumerator() << ' ';
	}

	_file << equipment->GetEnchantments().size() << ' ';

	for ( auto enchantment : equipment->GetEnchantments() )
	{
		_file << enchantment.type->Enumerator() << ' ' << enchantment.bonus << ' ';
	}

	_file << equipment->GetName().data() << '\n';
}
