#pragma once

#include "Character.h"
#include "ItemObserver.h"
#include "View.h"

namespace view
{
	class AttacksObserver : public View, public Observer<Character>
	{
	public:
		AttacksObserver( shared_ptr<sfg::Box> container );
		virtual ~AttacksObserver();

	private:
		virtual void StartObserving();
		virtual void StopObserving();

		void HandleAttacksModified( const vector<int>& bonuses );
		void SetAttacksLabel();

		shared_ptr<sfg::Box> _box;
		shared_ptr<sfg::Label> _labelAttacks;
	};
}